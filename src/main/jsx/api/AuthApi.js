import {createClient, HalResource, createResource, resetCache} from '@youdowell/ydw-hal-rest-client'
import AuthActions from 'actions/AuthActions'
import OperationStatusActions from 'actions/OperationStatusActions'

const halClient = createClient(window.ydw.config._links.authApi.href)

const handleError = (err, handler) => {
  if (err.response) {
    const {data, status} = err.response
    switch (status) {
      case 401:
        return AuthActions.reauthorize().then(handler)
      default:
        OperationStatusActions.notify({
          text: data && data.error ? `${data.error}. ${data.message}` :
            data.message || err.message,
        })
    }
  } else {
    return handler()
  }
  return Promise.reject(err)
}

class AuthApi {
  setAccessToken(accessToken) {
    halClient.addHeader('Authorization', `Bearer ${accessToken}`)
  }

  setLocale(locale) {
    halClient
      .addHeader('X-Locale', locale)
      .addHeader('Accept-Language', locale)
  }

  getCurrentUser() {
    const handler = () => halClient.fetchResource('/users/me')

    return handler().catch(err => handleError(err, handler))
  }

  findUserByUid(uid) {
    const handler = () => halClient.fetchResource('/users/search')
      .then(res => res.link('findByUid').fetch({uid, projection: 'view'}))
    // .then(res => halClient.fetchResource(res.uri.fill({ uid, projection: 'view' })))

    return handler().catch(err => handleError(err, handler))
  }

  findUserByUsername(username) {
    const handler = () => halClient.fetchResource('/users/search')
      .then(res => res.link('findByUsername').fetch({username, projection: 'view'}))

    return handler().catch(err => handleError(err, handler))
  }

  findUserByEmail(email) {
    const handler = () => halClient.fetchResource('/users/search')
      .then(res => res.link('findByEmail').fetch({email, projection: 'view'}))

    return handler().catch(err => handleError(err, handler))
  }

  listUsers(params) {
    const handler = () => halClient.fetchResource('users')

    return handler().catch(err => handleError(err, handler))
  }

  searchUsers(params) {
    const handler = () => halClient.fetchResource('users')
      .then(res => halClient.fetchResource(res.uri.fill(params)))

    return handler().catch(err => handleError(err, handler))
  }

  createUser(data) {
    const handler = () => halClient.create('users', data)

    return handler().catch(err => handleError(err, handler))
  }

  createUserResource() {
    resetCache()
    return createResource(halClient, HalResource, 'users')
  }

  findGroupByUid(uid) {
    const handler = () => halClient.fetchResource('/groups/search')
      .then(res => res.link('findByUid').fetch({uid}))

    return handler().catch(err => handleError(err, handler))
  }

  listGroups(params) {
    const handler = () => halClient.fetchResource('groups')

    return handler().catch(err => handleError(err, handler))
  }

  searchGroups(params) {
    const handler = () => halClient.fetchResource('groups')
      .then(res => halClient.fetchResource(res.uri.fill(params)))

    return handler().catch(err => handleError(err, handler))
  }

  createGroup(data) {
    const handler = () => halClient.create('groups', data)

    return handler().catch(err => handleError(err, handler))
  }

  createGroupResource() {
    resetCache()
    return createResource(halClient, HalResource, 'groups')
  }
}

export default new AuthApi()