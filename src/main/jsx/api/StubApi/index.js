import {createClient, HalResource, createResource, resetCache} from '@youdowell/ydw-hal-rest-client'
import AuthActions from 'actions/AuthActions'
import OperationStatusActions from 'actions/OperationStatusActions'

const halClient = createClient(window.ydw.config._links.coachApi.href)

const handleError = (err, handler) => {
  if (err.response) {
    const {data, status} = err.response
    switch (status) {
      case 401:
        return AuthActions.reauthorize().then(handler)
      default:
        OperationStatusActions.notify({
          text: data && data.error ? `${data.error}. ${data.message}` :
            data.message || err.message,
        })
    }
  } else {
    return handler()
  }
  return Promise.reject(err)
}

class Api {

  async listClientProfiles(params = {}) {
    let json = {}
    try {
      json = await require('./clients.json')
    } catch (err){
      handleError(err)
    }
    return json
  }


  async listForms(params = {}) {
    let json = {}
    try {
      json = await require('./forms.json')
    } catch (err){
      handleError(err)
    }
    return json
  }

  async listResponsesByUserId(userId) {
    let json = {}
    try {
      json = await require('./responses.json')
    } catch (err){
      handleError(err)
    }
    return json
  }

  async getResponseById(id) {
    let json = {}
    try {
      json = await require('./response.json')
    } catch (err){
      handleError(err)
    }
    return json
  }

  async fetchLinkToResponse(id) {
    let json = {}
    try {
      json = await require('./process.json')
    } catch (err){
      handleError(err)
    }
    return json
  }

}

export default new Api()
