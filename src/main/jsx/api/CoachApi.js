import {createClient, HalResource, createResource, resetCache} from '@youdowell/ydw-hal-rest-client'
import AuthActions from 'actions/AuthActions'
import OperationStatusActions from 'actions/OperationStatusActions'

const halClient = createClient(window.ydw.config._links.coachApi.href)

const handleError = (err, handler) => {
  if (err.response) {
    const {data, status} = err.response
    switch (status) {
      case 401:
        return AuthActions.reauthorize().then(handler)
      default:
        OperationStatusActions.notify({
          text: data && data.error ? `${data.error}. ${data.message}` :
            data.message || err.message,
        })
    }
  } else {
    return handler()
  }
  return Promise.reject(err)
}

class CoachApi {
  setAccessToken(accessToken) {
    halClient.addHeader('Authorization', `Bearer ${accessToken}`)
  }

  setLocale(locale) {
    halClient
      .addHeader('X-Locale', locale)
      .addHeader('Accept-Language', locale)
  }

  getClientProfileByUserId(userId) {
    const handler = () => halClient.fetchResource('clientProfiles/search')
      .then(res => res.link('findByAccountId').fetch({accountId: userId, projection: 'view'}))
    //.then(res => res.link('findByUid').fetch({ uid, projection: 'view' }))
    resetCache()
    return handler()
      .catch(err => handleError(err, handler))
      .catch(() => null)
  }

  getBoardByUserId(userId) {
    const handler = () => this.getClientProfileByUserId(userId)
      .then(res => res.link('board').fetch({projection: 'view'}))

    return handler().catch(err => handleError(err, handler))
  }

  listClientProfiles(params = {}) {
    const handler = () => halClient.fetchResource('/')
      .then(res => res.link('clientProfiles').fetch({projection: 'view', ...params}))

    return handler().catch(err => handleError(err, handler))
  }

  findNotes(params = {}) {
    const handler = () => halClient.fetchResource('notes/search')
      .then(res => res.link('findByBoard').fetch({projection: 'list', ...params}))

    return handler().catch(err => handleError(err, handler))
  }

  findNotesByLabel(params = {}) {
    const handler = () => halClient.fetchResource('notes/search')
      .then(res => res.link('findByBoardAndLabel').fetch({projection: 'list', ...params}))

    return handler().catch(err => handleError(err, handler))
  }
}

export default new CoachApi()