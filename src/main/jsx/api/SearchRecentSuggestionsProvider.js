const {localStorage: storage} = window

class SearchRecentSuggestionsProvider {
  insert(uri, value) {
    const values = JSON.parse(storage.getItem(uri)) || []
    storage.setItem(uri, JSON.stringify([value].concat(values)))
  }

  delete(uri, selection) {
    const values = JSON.parse(storage.getItem(uri)) || []
    let result = []
    if (selection) {
      result = values.filter(val => val.displayName.indexOf(selection) === -1)
    }
    storage.setItem(uri, JSON.stringify(result))
    return values.length - result.length
  }

  query(uri, selection) {
    const values = JSON.parse(storage.getItem(uri)) || []

    if (selection) {
      return values.filter(val => val.displayName.indexOf(selection) > -1)
    }

    return values
  }

  getType() {
    return 'Historical'
  }
}

export default SearchRecentSuggestionsProvider