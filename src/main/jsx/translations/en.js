export default {
  'locale': 'en',
  'phrases': {
    'ui.titles.clients': 'Clients',
    'ui.titles.myActiveClients': 'My active clients',
    'ui.titles.clientDetails': 'Client details',
    'ui.titles.experts': 'Experts',
    'ui.titles.joined': 'Joined',
    'ui.titles.inactive': 'Inactive',
    'ui.titles.noClients': 'No clients',

    'ui.titles.comments': 'Comments',
    'ui.titles.physicalActivities': 'Physical activities',
    'ui.titles.foodAndNutrition': 'Food & nutrition',
    'ui.titles.surveys': 'Surveys',
    'ui.titles.organizations': 'Organizations',
    'ui.titles.teams': 'Teams',

    "ui.titles.settings": "Settings",
    "ui.titles.untitled": "Untitled",
    "ui.titles.anonymous": "Anonymous",

    'ui.controls.ViewPlan.title': 'Diary',
    "ui.controls.NoItems.title": "No entries",

    'ui.pages.Clients.title': 'My Clients',
    'ui.pages.Client.title': 'Client details',

    'ui.pages.SurveyResponses.title': 'Responses',
    'ui.pages.SurveyResponse.title': 'Response details',
    'ui.pages.SurveyResponse.phase': 'Phase: %{phase}',
    'ui.pages.SurveyResponse.page': 'Page %{page}. %{title}',
    'ui.pages.SurveyForms.title': 'Forms',
    'ui.pages.SurveyProcess.title': 'Respond %{title}',
    'ui.pages.SurveyProcess.description': 'Survey will be opened in the new tab. Please respond to survey and click "Finish" button',

    "ui.controls.Drawer.sections.my.title": "Favorites",

    "ui.controls.Drawer.links.summary.title": "Summary",
    "ui.controls.Drawer.links.plan.title": "Plan",
    "ui.controls.Drawer.links.notes.title": "Notes",
    "ui.controls.Drawer.links.trackers.title": "Biometrics",

    "ui.controls.Drawer.links.exercises.title": "Activities",
    "ui.controls.Drawer.links.foods.title": "Foods",
    "ui.controls.Drawer.links.experts.title": "Experts",

    "ui.controls.Drawer.links.settings.title": "Settings",
    "ui.controls.Drawer.links.contacts.title": "Help & Contacts",

    "ui.controls.Drawer.links.profile.title": "Profile",
    "ui.controls.Drawer.links.logout.title": "Log out",
  },
}
