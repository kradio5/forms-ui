export default {
  'locale': 'de',
  'phrases': {
    'ui.titles.clients': 'Kunden',
    'ui.titles.myActiveClients': 'My active clients',
    'ui.titles.experts': 'Experts',
    'ui.titles.joined': 'Beigetreten',
    'ui.titles.inactive': 'Inaktiv',
    'ui.titles.noClients': 'Keine Kunden',

    'ui.titles.comments': 'Comments',
    'ui.titles.physicalActivities': 'Physical activities',
    'ui.titles.foodAndNutrition': 'Food & nutrition',
    'ui.titles.surveys': 'Surveys',
    'ui.titles.organizations': 'Organisationen',
    "ui.titles.teams": "Teams",
    
    "ui.titles.settings": "Einstellungen",
    "ui.titles.untitled": "Ohne Titel",
    "ui.titles.anonymous": "Anonym",

    'ui.controls.ViewPlan.title': 'Tagebuch',
    "ui.controls.NoItems.title": "Keine Einträge",

    'ui.pages.Clients.title': 'Meine Kunden',
    'ui.pages.Client.title': 'Kundendaten',

    'ui.pages.SurveyResponses.title': 'Antworten',
    'ui.pages.SurveyResponse.title': 'Antwortdetails',
    'ui.pages.SurveyResponse.phase': 'Phase: %{phase}',
    'ui.pages.SurveyResponse.page': 'Seite %{page}. %{title}',
    'ui.pages.SurveyForms.title': 'Formen',
    'ui.pages.SurveyProcess.title': 'Antworten %{title}',
    'ui.pages.SurveyProcess.description': 'Survey will be opened in the new tab. Please respond to survey and click "Finish" button',

    "ui.controls.Drawer.sections.my.title": "Favoriten",

    "ui.controls.Drawer.links.summary.title": "Übersicht",
    "ui.controls.Drawer.links.plan.title": "Plan",
    "ui.controls.Drawer.links.notes.title": "Notizen",
    "ui.controls.Drawer.links.trackers.title": "Vitaldaten",

    "ui.controls.Drawer.links.exercises.title": "Aktivitäten",
    "ui.controls.Drawer.links.foods.title": "Essen",
    "ui.controls.Drawer.links.experts.title": "Experten",

    "ui.controls.Drawer.links.settings.title": "Einstellungen",
    "ui.controls.Drawer.links.contacts.title": "Hilfe und Kontakt",

    "ui.controls.Drawer.links.profile.title": "Profil",
    "ui.controls.Drawer.links.logout.title": "Log out",
  },
}
