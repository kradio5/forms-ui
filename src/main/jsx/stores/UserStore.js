import AppDispatcher from 'dispatcher/AppDispatcher'
import {ReduceStore} from 'flux/utils'
import {ActionTypes} from 'constants/AppConstants'

class UserStore extends ReduceStore {
  getInitialState() {
    return null
  }

  hasProfile() {
    return Boolean(this.getState().links.clientProfile)
  }

  getCurrentUser() {
    return this.getState()
  }

  getUserId() {
    const user = this.getCurrentUser()
    return user && user.props.uid
  }

  getUsername() {
    const user = this.getCurrentUser()
    return user && user.props.username
  }

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.CURRENT_USER_DATA_LOAD:
        return action.data
      default:
        return state
    }
  }
}

export default new UserStore(AppDispatcher)