import AppDispatcher from 'dispatcher/AppDispatcher'
import {ReduceStore} from 'flux/utils'
import UserStore from './UserStore'
import {ActionTypes} from 'constants/AppConstants'

class ClientStore extends ReduceStore {
  getInitialState() {
    return null
  }

  getClient() {
    return this.getState()
  }

  getUserId() {
    const client = this.getClient()
    return client && client.props.uid
  }

  getUrlFragment() {
    return this.getUserId() === UserStore.getUserId() ?
      'me' : this.getUsername()
  }

  getUsername() {
    const client = this.getClient()
    return client && client.props.username
  }

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.CLIENT_DATA_LOAD:
        return action.data
      default:
        return state
    }
  }
}

export default new ClientStore(AppDispatcher)