import AppDispatcher from 'dispatcher/AppDispatcher'
import {ReduceStore} from 'flux/utils'
import {ActionTypes} from 'constants/AppConstants'

class BoardStore extends ReduceStore {
  getInitialState() {
    return null
  }

  getBoard() {
    return this.getState()
  }

  getBoardId() {
    const board = this.getBoard()
    return board && board.prop('boardId')
  }

  getBoardUrl() {
    const board = this.getBoard()
    return board && board.uri.fill({})
  }

  getBoardLabels() {
    const board = this.getBoard()
    return board && board.prop('labels') || []
  }

  /*
  getBoardLabelsHref(labelIds) {
    return this.getState().get('labels')
      .filter(it => labelIds ? labelIds.indexOf(it.get('labelId')) > -1 : true)
      .map(v => urlTemplate.parse(v.getIn(['_links', 'self', 'href'])).expand({}))
      .toArray()
  }
  */

  getBoardLabelById(labelId) {
    return this.getBoardLabels()
      .find(v => v.prop('labelId') === labelId)
  }

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.BOARD_LOAD:
        return action.data
      default:
        return state
    }
  }
}

export default new BoardStore(AppDispatcher)