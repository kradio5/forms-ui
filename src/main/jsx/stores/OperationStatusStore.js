import {ReduceStore} from 'flux/utils'
import AppDispatcher from 'dispatcher/AppDispatcher'
import {ActionTypes} from 'constants/AppConstants'

class OperationStatusStore extends ReduceStore {
  getInitialState() {
    return []
  }

  peek() {
    return this.getState()[0]
  }

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.OPERATION_STATUS_PUSH:
        return state.concat(action.data)
      case ActionTypes.OPERATION_STATUS_POP:
        return state.slice(0, -1)
      case ActionTypes.OPERATION_STATUS_CLEAR:
        return this.getInitialState()
      default:
        return state
    }
  }
}

export default new OperationStatusStore(AppDispatcher)