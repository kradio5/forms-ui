import AppDispatcher from 'dispatcher/AppDispatcher'
import {ReduceStore} from 'flux/utils'
import {ActionTypes} from 'constants/AppConstants'

class ClientProfileStore extends ReduceStore {
  getInitialState() {
    return null
  }

  getClientProfile() {
    return this.getState()
  }

  getClientProfileUrl() {
    const profile = this.getClientProfile()
    return profile && profile.uri.fill({})
  }

  getCalendarId() {
    const profile = this.getClientProfile()
    return profile && profile.props.calendarId
  }

  isActivated() {
    const profile = this.getClientProfile()
    return Boolean(profile && profile.props.activated)
  }

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.CLIENT_PROFILE_LOAD:
        return action.data
      default:
        return state
    }
  }
}

export default new ClientProfileStore(AppDispatcher)