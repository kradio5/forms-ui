import AppDispatcher from 'dispatcher/AppDispatcher'
import {ReduceStore} from 'flux/utils'
import {ActionTypes} from 'constants/AppConstants'
import jwtDecode from 'jwt-decode'

const {sessionStorage: storage} = window
// Mark token Expired if it will expire less than in 2 minutes
const TOKEN_EXPIRY_THRESHOLD = 120

class AuthStore extends ReduceStore {
  getInitialState() {
    return {
      accessToken: storage.getItem('accessToken'),
      accessTokenExpiry: storage.getItem('accessTokenExpiry'),
    }
  }

  hasToken() {
    return Boolean(this.getState().accessToken)
  }

  isAuthorized() {
    return this.hasToken() && !this.isTokenExpired()
  }

  getToken() {
    return this.getState().accessToken
  }

  getTokenExpiry() {
    return this.getState().accessTokenExpiry
  }

  estimateTokenExpiry(expiresInSeconds) {
    return Date.now() + Number(expiresInSeconds) * 1000
  }

  isTokenExpired() {
    return Date.now() > Number(this.getTokenExpiry())
  }

  isTokenResetRequired() {
    const tokenExpiry = this.getTokenExpiry()
    return !tokenExpiry || Date.now() > tokenExpiry - TOKEN_EXPIRY_THRESHOLD * 1000
  }

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.AUTH_STORE_CREDENTIALS: {
        const {
          accessToken,
          expiresIn,
        } = action.data
        let payload = jwtDecode(accessToken)
        let accessTokenExpiry = this.estimateTokenExpiry(expiresIn)

        storage.setItem('accessToken', accessToken)
        storage.setItem('accessTokenExpiry', accessTokenExpiry)

        return {
          ...payload,
          ...action.data,
          accessTokenExpiry,
        }
      }
      case ActionTypes.AUTH_DROP_CREDENTIALS:
        storage.removeItem('accessToken')
        storage.removeItem('accessTokenExpiry')
        return this.getInitialState()
      default:
        return state
    }
  }
}

export default new AuthStore(AppDispatcher)
