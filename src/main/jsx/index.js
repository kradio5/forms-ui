import React from 'react'
import ReactDOM from 'react-dom'
import AppRoutes from 'routes'
import LocationUtil from 'util/location'

/*eslint-disable no-unused-vars, no-undef */
__webpack_public_path__ = window.ydw.config.sourceUri.replace(/\/?$/, '/')
/*eslint-enable no-unused-vars, no-undef */

if (!LocationUtil.isRenderedInFrame()) {
  ReactDOM.render(<AppRoutes />, document.getElementById('app'))
}