import keyMirror from 'keymirror'

const CONSTANTS = {
  ActionTypes: keyMirror({
    AUTH_STORE_CREDENTIALS: null,
    AUTH_DROP_CREDENTIALS: null,
    CLIENTS_LOAD: null,
    CLIENTS_LOAD_NEXT: null,
    CLIENTS_CLEAR: null,

    SURVEY_RESPONSES_LOAD: null,
    SURVEY_RESPONSES_CLEAR: null,

    SURVEY_RESPONSE_LOAD: null,
    SURVEY_RESPONSE_CLEAR: null,

    SURVEY_FORMS_LOAD: null,
    SURVEY_FORMS_LOAD_NEXT: null,
    SURVEY_FORMS_CLEAR: null,

    SURVEY_PROCESS_LOAD: null,
    SURVEY_PROCESS_CLEAR: null,

    OPERATION_STATUS_PUSH: null,
    OPERATION_STATUS_POP: null,
    OPERATION_STATUS_CLEAR: null,
  }),
}

export default CONSTANTS
export const ActionTypes = CONSTANTS.ActionTypes
