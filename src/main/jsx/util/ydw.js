import moment from 'util/moment'
import i18n from 'util/i18n'

class YDWUtil {
  constructor(options) {
    this._options = options
  }

  getWeightLossRateValue(rate) {
    switch (rate) {
      case 'SLOW':
        return -0.25
      case 'NORMAL':
        return -0.5
      case 'FAST':
        return -1.0
    }
  }

  getWeightGainRateValue(rate) {
    switch (rate) {
      case 'SLOW':
        return 0.125
      case 'NORMAL':
        return 0.25
      case 'FAST':
        return 0.5
    }
  }

  estimateEnergy(met, duration) {
    const d = moment.duration(
      typeof duration === 'number' ?
        duration * 60 * 1000 : duration
    )
    const hours = d.asHours()
    const weight = 70
    const kcal = met * weight * hours

    return Math.ceil(kcal)
  }

  estimateTargetTime(startWeight, targetWeight, rate) {
    const weeks = Math.abs(startWeight - targetWeight) / rate

    return moment.duration(weeks, 'week').humanize()
  }

  estimateBmi(weight, height) {
    return weight / Math.pow(height / 100, 2)
  }

  estimateExtremeBmiValues(height) {
    return [18.5, 25].map(bmi => Math.floor(bmi * Math.pow(height / 100, 2)))
  }

  getBmiRanges() {
    return [16, 18.5, 25, 40]
  }

  estimateBmiName(value) {
    let valueLevelMed = 'NORMAL'
    switch (true) {
      case value < 17:
        valueLevelMed = 'SEVERELY_UNDERWEIGHT'
        break
      case value < 18.5:
        valueLevelMed = 'UNDERWEIGHT'
        break
      case value >= 40:
        valueLevelMed = 'OBESE_3'
        break
      case value >= 35:
        valueLevelMed = 'OBESE_2'
        break
      case value >= 30:
        valueLevelMed = 'OBESE_1'
        break
      case value >= 25:
        valueLevelMed = 'OVERWEIGHT'
        break
      default:
        valueLevelMed = 'NORMAL'
        break
    }
    return valueLevelMed
  }

  estimateBmiNameSimple(value) {
    return value < 18.5 ? 'UNDERWEIGHT' : value >= 25 ? 'OVERWEIGHT' : 'NORMAL'
  }

  estimateWhr(waist, hip) {
    return waist / hip
  }

  estimateExtremeWhrValues(sex = 'MALE') {
    return sex === 'MALE' ? [0.8, 0.85] : [0.9, 1]
  }

  estimateWhrRanges(sex = 'MALE') {
    return sex === 'MALE' ? [0.5, 0.9, 1, 1.2] : [0.5, 0.8, 0.85, 1.2]
  }

  estimateWhrName(value, sex = 'MALE') {
    const ranges = this.estimateWhrRanges(sex)
    return value < ranges[1] ? 'LOW_RISK' : value >= ranges[2] ? 'HIGH_RISK' : 'MODERATE_RISK'
  }

  formatStartDay(start) {
    const m = moment.isMoment(start) ? start : moment(start)
    /**
     * @see https://github.com/moment/moment/issues/4552
     */
    const key = moment.calendarFormat(
      m.clone().startOf('day'),
      moment().startOf('day')
    )
    switch (key) {
      case 'sameDay':
      case 'lastDay':
      case 'nextDay':
        return i18n.t(`ui.titles.${key}`)
      default:
        return m.format('ll')
    }
  }

  formatSampleAmount(amount, scale = 0) {
    return Number(amount.toFixed(scale))
  }

  getDocumentSize() {
    return [
      window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
      window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
    ]
  }

  isMobile() {
    return (function (a) { return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)) })(navigator.userAgent || navigator.vendor || window.opera)
  }

  /**
   * TODO: Add implementation
   */
  formatDistance(distance) {
    return `${distance} m`
  }

  formatDuration(duration) {
    let d = moment.isDuration(duration) ?
      duration : (
        typeof duration === 'number' ?
          moment.duration(duration, 'sec') :
          moment.duration(duration)
      )
    let hours = d.hours()
    let minutes = d.minutes()
    let seconds = d.seconds()

    return `${hours < 10 ? '0' + hours : hours}:${minutes < 10 ? '0' + minutes : minutes}:${seconds < 10 ? '0' + seconds : seconds}`
  }
}

export default new YDWUtil()