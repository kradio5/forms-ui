import moment from 'moment'
// import i18n from 'util/i18n'

// moment.locale(i18n.locale())

moment.relativeTimeThreshold('s', 60)
moment.relativeTimeThreshold('m', 60)
moment.relativeTimeThreshold('h', 24)
moment.relativeTimeThreshold('d', 31)
moment.relativeTimeThreshold('M', 12)

/*
moment.relativeTimeRounding(function(v) {
  return Number(v.toPrecision(3))
})
*/

export default moment
