import createBrowserHistory from 'history/createBrowserHistory'
import url from 'url'

export default createBrowserHistory({
  basename: url.parse(window.ydw.config._links.expertUi.href).pathname,
})
