import math from 'mathjs'

math.createUnit({
  kgm: {
    definition: '9.80665 J',
  },
  kcal: {
    definition: '4186.798188 J',
  },
  met: {},
  percent: {
    aliases: ['pct'],
  },
  step: {},
  rep: {},
})

export default math