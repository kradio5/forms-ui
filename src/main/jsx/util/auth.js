import {UserManager, Log} from 'oidc-client'

Log.logger=console

const {config} = window.ydw
const options = config.oidc
export default new UserManager({
  ...options,
})
