import querystring from 'querystring'

class LocationUtil {
  parseQuery(loc) {
    return loc.search ?
      querystring.parse(loc.search.substring(1)) : {}
  }

  resolveUrl(baseUrl, url) {
    return [
      baseUrl.replace(/\/*$/, ''),
      url.replace(/^\/*/, ''),
    ].join('/')
  }

  getLocationParam(loc, param) {
    const query = this.parseQuery(loc)
    return query[param] || (loc.state && loc.state[param])
  }

  isRenderedInFrame() {
    try {
      return window.self !== window.top
    } catch (e) {
      return true
    }
  }
}

export default new LocationUtil()
