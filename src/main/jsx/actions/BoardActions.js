import AppDispatcher from 'dispatcher/AppDispatcher'
import {ActionTypes} from 'constants/AppConstants'
import ClientStore from 'stores/ClientStore'
import {CoachApi} from 'api'

class BoardActions {
  getClientBoard() {
    return CoachApi.getBoardByUserId(ClientStore.getUserId())
      .then(data => {
        AppDispatcher.dispatch({
          actionType: ActionTypes.BOARD_LOAD,
          data: data,
        })

        return data
      })
  }
}

export default new BoardActions()
