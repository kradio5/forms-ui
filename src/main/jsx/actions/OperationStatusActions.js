import AppDispatcher from 'dispatcher/AppDispatcher'
import {ActionTypes} from 'constants/AppConstants'

class OperationStatusActions {
  notify(data) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.OPERATION_STATUS_PUSH,
      data,
    })
  }
  relieve() {
    AppDispatcher.dispatch({
      actionType: ActionTypes.OPERATION_STATUS_POP,
    })
  }
}

export default new OperationStatusActions()