import AppDispatcher from 'dispatcher/AppDispatcher'
import {ActionTypes} from 'constants/AppConstants'
import {AuthApi} from 'api'

class ClientActions {
  findByUid(uid) {
    return AuthApi.findUserByUid(uid)
      .then(data => {
        AppDispatcher.dispatch({
          actionType: ActionTypes.CLIENT_DATA_LOAD,
          data: data,
        })

        return data
      })
  }

  findByUsername(username) {
    return AuthApi.findUserByUsername(username)
      .then(data => {
        AppDispatcher.dispatch({
          actionType: ActionTypes.CLIENT_DATA_LOAD,
          data: data,
        })

        return data
      })
  }
}

export default new ClientActions()