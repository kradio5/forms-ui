import AppDispatcher from 'dispatcher/AppDispatcher'
import {ActionTypes} from 'constants/AppConstants'
import ClientStore from 'stores/ClientStore'
import {CoachApi} from 'api'

class ClientProfileActions {
  getByUserId(userId) {
    return CoachApi.getClientProfileByUserId(userId)
      .then(data => {
        AppDispatcher.dispatch({
          actionType: ActionTypes.CLIENT_PROFILE_LOAD,
          data: data,
        })

        return data
      })
  }
  
  createResource() {
    return CoachApi.createClientProfileResource()
      .prop('accountId', ClientStore.getUserId())
  }

  create(clientProfile) {
    return clientProfile.create()
      .then(data => {
        AppDispatcher.dispatch({
          actionType: ActionTypes.CLIENT_PROFILE_LOAD,
          data: data,
        })

        return data
      })
  }

  update(clientProfile) {
    return clientProfile.update()
      .then(data => {
        AppDispatcher.dispatch({
          actionType: ActionTypes.CLIENT_PROFILE_LOAD,
          data: data,
        })

        return data
      })
  }
}

export default new ClientProfileActions()