import AppDispatcher from 'dispatcher/AppDispatcher'
import {ActionTypes} from 'constants/AppConstants'
import {AuthApi} from 'api'

class UserActions {
  getCurrentUser() {
    return AuthApi.getCurrentUser()
      .then(data => { 
        AppDispatcher.dispatch({
          actionType: ActionTypes.CURRENT_USER_DATA_LOAD,
          data: data,
        })

        return data
      })
  }
}

export default new UserActions()