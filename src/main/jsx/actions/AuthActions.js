import AppDispatcher from 'dispatcher/AppDispatcher'
import {ActionTypes} from 'constants/AppConstants'
import {AuthApi, CoachApi} from 'api'
import authUtil from 'util/auth'
import LocationUtil from 'util/location'

class AuthActions {
  authorize() {
    authUtil.authorize({
      redirectUri: location.href.split('#')[0],
    })
  }

  storeCredentials(credentials) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.AUTH_STORE_CREDENTIALS,
      data: credentials,
    })
  }

  authorizeApi(accessToken) {
    AuthApi.setAccessToken(accessToken)
    CoachApi.setAccessToken(accessToken)
  }

  unauthorize() {
    AppDispatcher.dispatch({
      actionType: ActionTypes.AUTH_DROP_CREDENTIALS,
    })
  }

  reauthorize() {
    return new Promise((resolve, reject) => {
      const {href} = window.ydw.config._links.expertUi
      authUtil.renewAuth({
        redirectUri: LocationUtil.resolveUrl(href, 'login'),
      }, (err, credentials) => {
        if (err || !credentials) {
          this.unauthorize()
          this.authorize()
          reject(err)
        } else {
          this.authorizeApi(credentials.accessToken)
          this.storeCredentials(credentials)
          resolve(credentials)
        }
      })
    })
  }
}

export default new AuthActions()