import AppDispatcher from 'dispatcher/AppDispatcher'
import {ActionTypes} from 'constants/AppConstants'
import url from 'url'
import history from 'util/history'

class NavigationActions {
  constructor() {
    // this.clearListener = this.setupListener()
  }
  getCurrentLocation() {
    return history.location
  }
  getCurrentAction() {
    return history.location
  }
  getLength() {
    return history.length
  }
  go(n) {
    return history.go(n)
  }
  goBack() {
    history.goBack()
  }
  goForward() {
    history.goForward()
  }
  push(path, state) {
    history.push(path, state)
  }
  replace(path, state) {
    history.replace(path, state)
  }
  pushLocation(location) {
    history.push(url.format(location), location.state)
  }
  replaceLocation(location) {
    history.replace(url.format(location), location.state)
  }
  setupListener() {
    return history.listen(this.handleChange)
  }
  handleChange = (location, action) => {
    AppDispatcher.dispatch({
      actionType: ActionTypes.NAVIGATE,
      data: {
        location: location,
        action: action,
      },
    })
  }
}

export default new NavigationActions()