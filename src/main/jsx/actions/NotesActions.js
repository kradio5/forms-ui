import AppDispatcher from 'dispatcher/AppDispatcher'
import {ActionTypes} from 'constants/AppConstants'
import BoardStore from 'stores/BoardStore'
import {CoachApi} from 'api'

class NotesActions {
  countUnread() {
    return CoachApi.findNotesByLabel({
      board: BoardStore.getBoardId(),
      label: 'unread',
      sort: 'created,desc',
      size: 0,
    })
      .then(res => res.props.page.totalElements)
      .catch(() => 0)
  }
}

export default new NotesActions()
