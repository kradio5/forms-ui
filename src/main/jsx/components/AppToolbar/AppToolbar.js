import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import classNames from 'classnames'

const drawerWidth = 0
const styles = theme => ({
  appBar: {
    position: 'absolute',
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  'appBar-left': {
    marginLeft: drawerWidth,
  },
  'appBar-right': {
    marginRight: drawerWidth,
  },
  toolbar: {
    // flexDirection: theme.direction === 'rtl' ? 'row-reverse' : 'row',
    position: 'relative',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    width: '100%',
  },
})

class AppToolbar extends React.Component {
  static displayName = 'AppToolbar'

  static propTypes = {
    ToolbarProps: PropTypes.shape({}),
    children: PropTypes.node,
  }

  static defaultProps = {
    ToolbarProps: {},
  }

  render() {
    const {children, classes, className, theme, ToolbarProps, ...otherProps} = this.props
    const anchor = theme.direction === 'rtl' ? 'right' : 'left'
    return (
      <AppBar
        className={classNames(className, classes.appBar, classes[`appBar-${anchor}`])}
        {...otherProps}
      >
        <Toolbar
          className={classNames(ToolbarProps.className, classes.toolbar)}
          disableGutters
          {...ToolbarProps}
        >
          {children}
        </Toolbar>
      </AppBar>
    )
  }
}

export default withStyles(styles, {withTheme: true})(AppToolbar)
