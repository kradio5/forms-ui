import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import classNames from 'classnames'

const styles = theme => ({
  root: {
    position: 'relative',
    display: 'inline-flex',
    flexFlow: 'row nowrap',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    minWidth: 0,
  },
  variantStart: {
    flex: theme.direction === 'rtl' ? 'initial' : 1,
    justifyContent: theme.direction === 'rtl' ? 'flex-end' : 'flex-start',
  },
  variantEnd: {
    flex: theme.direction === 'rtl' ? 1 : 'initial',
    justifyContent: theme.direction === 'rtl' ? 'flex-start' : 'flex-end',
  },
})

class ToolbarSection extends React.Component {
  static displayName = 'ToolbarSection'

  static propTypes = {
    children: PropTypes.node,
    variant: PropTypes.oneOf(['start', 'end']).isRequired,
  }

  render() {
    const {children, classes, variant} = this.props
    const className = classNames(classes.root, {
      [classes.variantEnd]: variant === 'end',
      [classes.variantStart]: variant === 'start',
    })
    return (
      <div className={className}>
        {children}
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(ToolbarSection)
