import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import classNames from 'classnames'

const styles = theme => ({
  root: {
    ...theme.mixins.toolbarMenuButton,
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
})

class MenuButton extends React.Component {
  static displayName = 'MenuButton'

  static propTypes = {
    children: PropTypes.node,
    color: PropTypes.string,
  }

  static defaultProps = {
    color: 'secondary',
  }

  render() {
    const {classes, className, theme, ...otherProps} = this.props
    return (
      <IconButton
        aria-label="open drawer"
        className={classNames(className, classes.root)}
        {...otherProps}
      >
        <Icon>{'menu'}</Icon>
      </IconButton>
    )
  }
}

export default withStyles(styles, {withTheme: true})(MenuButton)
