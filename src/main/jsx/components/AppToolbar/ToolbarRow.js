import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import classNames from 'classnames'

const styles = theme => ({
  root: {
    ...theme.mixins.toolbar,
    display: 'flex',
    flexDirection: theme.direction === 'rtl' ? 'row-reverse' : 'row',
    flexWrap: 'nowrap',
    position: 'relative',
    alignItems: 'center',
    width: '100%',
    height: 'auto',
  },
  gutters: theme.mixins.gutters(),
})

class ToolbarRow extends React.Component {
  static displayName = 'ToolbarRow'

  static propTypes = {
    children: PropTypes.node,
    disableGutters: PropTypes.bool,
  }

  static defaultProps = {
    disableGutters: false,
  }

  render() {
    const {children, classes, className: classNameProp, disableGutters, ...other} = this.props
    const className = classNames(
      classes.root,
      {[classes.gutters]: !disableGutters},
      classNameProp
    )
    return (
      <div className={className}>
        {children}
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(ToolbarRow)
