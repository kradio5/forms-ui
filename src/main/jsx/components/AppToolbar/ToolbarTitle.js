import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

const styles = theme => ({
  root: {
    display: 'inline-block',
  },
})

class ToolbarTitle extends React.Component {
  static displayName = 'ToolbarTitle'

  static propTypes = {
    children: PropTypes.node,
  }

  render() {
    const {children, classes} = this.props
    return (
      <Typography
        className={classes.root}
        color="secondary"
        noWrap
        variant="h6"
      >
        {children}
      </Typography>
    )
  }
}

export default withStyles(styles, {withTheme: true})(ToolbarTitle)
