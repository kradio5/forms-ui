import React from 'react'
import PropTypes from 'prop-types'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import Menu from '@material-ui/core/Menu'
import {withStyles} from '@material-ui/core/styles'
import classNames from 'classnames'

const styles = theme => ({
  root: {
    marginLeft: theme.spacing.unit * (theme.direction === 'rtl' ? -1.5 : 0),
    marginRight: theme.spacing.unit * (theme.direction === 'rtl' ? 0 : -1.5),
  },
})

class ToolbarMenu extends React.Component {
  static displayName = 'ToolbarMenu'

  static propTypes = {
    children: PropTypes.node,
  }

  state = {
    anchorEl: null,
  }

  handleMenu = e => {
    this.setState({anchorEl: e.currentTarget})
  }

  handleClose = () => {
    this.setState({anchorEl: null})
  }

  render() {
    const {children, classes} = this.props
    const {anchorEl} = this.state
    const open = Boolean(anchorEl)
    const originPosition = {
      vertical: 'top',
      horizontal: 'right',
    }
    return (
      <div className={classes.root}>
        <IconButton
          aria-haspopup="true"
          aria-owns={open ? 'toolbar-menu' : null}
          color="secondary"
          onClick={this.handleMenu}
        >
          <Icon>{'more_vert'}</Icon>
        </IconButton>
        <Menu
          anchorEl={anchorEl}
          anchorOrigin={originPosition}
          id="toolbar-menu"
          MenuListProps={{disablePadding: true}}
          onClose={this.handleClose}
          open={open}
          transformOrigin={originPosition}
        >
          {React.Children.map(children, child => {
            if (!child) return child
            const handler = child.props.onClick
            return React.cloneElement(child, {
              onClick: (...args) => {
                this.handleClose()
                if (typeof handler === 'function') {
                  handler(...args)
                }
              },
            })
          })}
        </Menu>
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(ToolbarMenu)
