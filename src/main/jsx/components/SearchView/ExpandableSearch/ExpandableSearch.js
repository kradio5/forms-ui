import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import {ToolbarSection} from 'components/AppToolbar'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Dialog from '@material-ui/core/Dialog'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import Input from '@material-ui/core/Input'
import Slide from '@material-ui/core/Slide'
import classNames from 'classnames'

function Transition(props) {
  return (
    <Slide
      direction="up"
      {...props}
    />
  )
}

const styles = theme => ({
  root: {
  },
  appBar: {
    position: 'absolute',
  },
  toolbar: {
    flexDirection: theme.direction === 'rtl' ? 'row-reverse' : 'row',
    flexWrap: 'nowrap',
  },
  content: {
    position: 'relative',
    width: '100%',
    height: 'calc(100% - 56px)',
    padding: [[theme.spacing.unit, 0]],
    marginTop: 56,
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 64px)',
      padding: [[theme.spacing.unit * 2, 0]],
      marginTop: 64,
    },
    overflowX: 'hidden',
    overflowY: 'scroll',
  },
  contentSubmit: {
    padding: theme.spacing.unit,
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing.unit * 2,
    },
  },
  open: {
    visibility: 'visible',
  },
  form: {
    width: '100%',
  },
  input: {
    ...theme.typography.title,
    fontWeight: 400,
  },
})

class ExpandableSearch extends React.Component {
  static displayName = 'ExpandableSearch'

  static propTypes = {
    children: PropTypes.node,
    onCancel: PropTypes.func,
    onChange: PropTypes.func,
    onClose: PropTypes.func,
    onOpen: PropTypes.func,
    onSubmit: PropTypes.func,
    open: PropTypes.bool,
    placeholder: PropTypes.string,
  }

  static defaultProps = {
    onCancel: () => { },
    onChange: () => { },
    onClose: () => { },
    onOpen: () => { },
    onSubmit: () => { },
    open: false,
    placeholder: 'Search',
  }

  static contextTypes = {
    toolbar: PropTypes.object,
  }

  constructor(props) {
    super(props)

    this.state = {
      value: '',
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      value: nextProps.value,
    }, () => {
      if (this.input_) {
        if (this.props.open) {
          this.input_.focus()
        } else {
          this.input_.blur()
        }
      }
    })
  }

  handleInputRef = el => {
    if (el) {
      this.input_ = el
      if (this.props.open) {
        el.focus()
      }
    }
  }

  handleClear = e => {
    e.preventDefault()
    this.props.onChange('')
  }

  handleChange = e => {
    this.props.onChange(e.target.value)
  }

  handleSubmit = e => {
    e.preventDefault()
    this.props.onSubmit(this.state.value)
  }

  handleFocus = () => {
    this.props.onOpen()
  }

  handleCancel = e => {
    e.preventDefault()
    this.props.onCancel()
  }

  handleClose = () => {
    this.props.onClose()
  }

  render() {
    const {
      children,
      classes,
      open,
      placeholder,
      submit,
      query,
    } = this.props

    const {value} = this.state
    return (
      <Dialog
        className={classes.root}
        fullScreen
        onClose={this.handleClose}
        open={open || submit}
        TransitionComponent={Transition}
      >
        <AppBar
          className={classes.appBar}
          color="default"
          position="fixed"
        >
          <Toolbar className={classes.toolbar}>
            <ToolbarSection variant="start">
              <IconButton
                color="inherit"
                onClick={this.handleCancel}
              >
                <Icon>{'arrow_back'}</Icon>
              </IconButton>
              <form
                autoComplete="off"
                className={classes.form}
                noValidate
                onSubmit={this.handleSubmit}
              >
                <Input
                  autoComplete="off"
                  className={classes.input}
                  disableUnderline
                  fullWidth
                  id="search"
                  inputRef={this.handleInputRef}
                  margin="none"
                  onChange={this.handleChange}
                  onFocus={this.handleFocus}
                  placeholder={placeholder}
                  value={value}
                />
              </form>
            </ToolbarSection>
            <ToolbarSection variant="end">
              {value.length > 0 && (
                <IconButton
                  color="inherit"
                  onClick={this.handleClear}
                >
                  <Icon>{'close'}</Icon>
                </IconButton>
              )}
            </ToolbarSection>
          </Toolbar>
        </AppBar>
        <div className={classNames(classes.content, {
          [classes.contentSubmit]: submit,
        })}
        >
          {children}
        </div>
      </Dialog>
    )
  }
}

export default withStyles(styles, {withTheme: true})(ExpandableSearch)
