import keyMirror from 'keymirror'

const CONSTANTS = {
  ActionTypes: keyMirror({
    SEARCH_RESULTS_LOAD: null,
    SEARCH_RESULTS_NEXT: null,
    SEARCH_RESULT_REMOVE: null,
    SEARCH_RESULTS_CLEAR: null,
    SEARCH_RESULT_ITEMS_LOAD: null,
    SEARCH_RESULT_ITEMS_CLEAR: null,
    SEARCH_QUERY_ITEMS_LOAD: null,
    SEARCH_QUERY_ITEMS_CLEAR: null,
    HISTORICAL_SUGGESTIONS_LOAD: null,
    HISTORICAL_SUGGESTIONS_CLEAR: null,
    AUTOCOMPLETED_SUGGESTIONS_LOAD: null,
    AUTOCOMPLETED_SUGGESTIONS_CLEAR: null,
    PAGER_LOAD: null,
  }),
}

export default CONSTANTS
export const ActionTypes = CONSTANTS.ActionTypes
