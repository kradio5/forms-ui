const {localStorage: storage} = window

class SearchRecentSuggestionsProvider {
  update(uri, value) {
    let values = JSON.parse(storage.getItem(uri)) || []
    values = values.reduce((reduction, it) => {
      if (it.displayName === value.displayName) {
        return reduction
      }
      return reduction.concat(it)
    }, [])
    storage.setItem(uri, JSON.stringify([{...value, type: this.getType()}].concat(values)))
  }

  delete(uri, selection) {
    const values = JSON.parse(storage.getItem(uri)) || []
    let result = []
    if (selection) {
      let re = new RegExp(selection, 'i')
      result = values.filter(val => !re.test(val.displayName))
    }
    storage.setItem(uri, JSON.stringify(result))
    return values.length - result.length
  }

  query(uri, selection) {
    const values = JSON.parse(storage.getItem(uri)) || []

    if (selection) {
      let re = new RegExp(selection, 'i')
      return values.filter(val => re.test(val.displayName))
    }

    return values
  }

  getType() {
    return 'Historical'
  }
}

export default SearchRecentSuggestionsProvider
