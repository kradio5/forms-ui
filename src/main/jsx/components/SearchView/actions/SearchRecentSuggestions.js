import AppDispatcher from 'dispatcher/AppDispatcher'
import {ActionTypes} from '../constants/SearchConstants'
import SearchRecentSuggestionsProvider from './SearchRecentSuggestionsProvider'

class SearchRecentSuggestions {
  constructor(context) {
    this.context_ = context
    this.provider_ = new SearchRecentSuggestionsProvider()
  }

  clearHistory() {
    this.provider_.delete(this.context_)
    AppDispatcher.dispatch({
      actionType: ActionTypes.HISTORICAL_SUGGESTIONS_CLEAR,
      data: null,
    })
  }

  saveRecentQuery(query) {
    this.provider_.update(this.context_, query)
  }

  loadRecentQueries(selection=null) {
    let queries = this.provider_.query(this.context_, selection)
    if (selection) {
      queries = queries.map(it => {
        let re = new RegExp(selection, 'gi')
        let hl = []
        let match = null
        while (match = re.exec(it.displayName)) {
          hl.push([match.index, re.lastIndex])
        }
        return {...it, hl}
      })
    }
    AppDispatcher.dispatch({
      actionType: ActionTypes.HISTORICAL_SUGGESTIONS_LOAD,
      data: queries,
    })
  }
}

export default SearchRecentSuggestions
