import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import SearchResultLinePickerItem from './SearchResultLinePickerItem'

const styles = theme => ({
  root: {
    marginBottom: theme.spacing.unit,
  },
  cardContent: {
    '&:last-child': {
      paddingBottom: theme.spacing.unit * 2,
    },
  },
  itemsContaner: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'flex-start',
    width: '100%',
    overflowX: 'auto',
  },
  itemContainer: {
    display: 'inline-flex',
    marginRight: theme.spacing.unit * 2,
    '&:last-of-type': {
      marginRight: 0,
    },
  },
})

class SearchResultLinePicker extends React.PureComponent {
  static displayName = 'SearchResultLinePicker'

  static propTypes = {
    children: PropTypes.node,
    items: PropTypes.arrayOf(PropTypes.shape({
      props: PropTypes.object,
    })),
    onSubmit: PropTypes.func,
  }

  static defaultProps = {
    onSubmit: () => { },
  }

  _renderItem(it, index) {
    const {classes} = this.props
    return (
      <span
        className={classes.itemContainer}
        key={index}
      >
        <SearchResultLinePickerItem
          item={it}
          onSubmit={this.props.onSubmit}
        />
      </span>
    )
  }

  render() {
    const {children, classes, items} = this.props
    return (
      <Card className={classes.root}>
        <CardContent className={classes.cardContent}>
          <div className={classes.itemsContaner}>
            {items.map(this._renderItem, this)}
            {children}
          </div>
        </CardContent>
      </Card>
    )
  }
}

export default withStyles(styles, {withTheme: true})(SearchResultLinePicker)
