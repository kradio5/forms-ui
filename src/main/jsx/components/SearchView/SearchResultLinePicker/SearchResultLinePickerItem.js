import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'

const styles = theme => ({
  root: {
    flex: 'none',
  },
  fab: {
    boxShadow: 'none',
    '&:active': {
      boxShadow: 'none',
    },
  },
})

class SearchResultLinePickerItem extends React.PureComponent {
  static displayName = 'SearchResultItem'

  static propTypes = {
    color: PropTypes.string,
    disabled: PropTypes.bool,
    icon: PropTypes.string,
    item: PropTypes.shape({
      props: PropTypes.shape({
        icon: PropTypes.string,
      }),
    }),
    onSubmit: PropTypes.func,
  }

  static defaultProps = {
    color: 'secondary',
    disabled: false,
    icon: 'accessibility',
    onSubmit: () => { },
  }

  handleSubmit = e => {
    e.preventDefault()
    this.props.onSubmit(this.props.item)
  }

  render() {
    const {classes, color, disabled, icon, item} = this.props
    return (
      <Button
        className={classes.root}
        classes={{fab: classes.fab}}
        color={color}
        disabled={disabled}
        onClick={this.handleSubmit}
        variant="fab"
      >
        <Icon>
          {item.props.icon || icon}
        </Icon>
      </Button>
    )
  }
}

export default withStyles(styles, {withTheme: true})(SearchResultLinePickerItem)
