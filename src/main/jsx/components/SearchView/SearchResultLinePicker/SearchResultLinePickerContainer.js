import React from 'react'
import {Container} from 'flux/utils'
import SearchResultItemStore from '../stores/SearchResultItemStore'
import SearchResultLinePicker from './SearchResultLinePicker'

class SearchResultLinePickerContainer extends React.Component {
  static displayName = 'SearchResultLinePickerContainer'

  static getStores() {
    return [SearchResultItemStore]
  }

  static calculateState() {
    return {
      items: SearchResultItemStore.getAll(),
    }
  }

  render() {
    const {children, ...otherProps} = this.props
    return this.state.items.length ? (
      <SearchResultLinePicker
        {...otherProps}
        {...this.state}
      >
        {children}
      </SearchResultLinePicker>
    ) : null
  }
}

export default Container.create(SearchResultLinePickerContainer)
