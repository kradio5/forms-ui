import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import Card from '@material-ui/core/Card'
import SearchQueryListPickerItem from './SearchQueryListPickerItem'

const styles = theme => ({
  root: {
    marginBottom: theme.spacing.unit,
  },
})

class SearchQueryListPicker extends React.PureComponent {
  static displayName = 'SearchQueryListPicker'

  static propTypes = {
    children: PropTypes.node,
    items: PropTypes.arrayOf(PropTypes.shape({
      props: PropTypes.object,
    })),
    onSubmit: PropTypes.func,
  }

  static defaultProps = {
    onSubmit: () => {},
  }

  _renderItem(it, index) {
    return (
      <SearchQueryListPickerItem
        item={it}
        key={index}
        onSubmit={this.props.onSubmit}
      />
    )
  }

  render() {
    const {classes} = this.props
    return (
      <Card className={classes.root}>
        <List disablePadding>
          {this.props.items.map(this._renderItem, this)}
          {this.props.children}
        </List>
      </Card>
    )
  }
}

export default withStyles(styles, {withTheme: true})(SearchQueryListPicker)
