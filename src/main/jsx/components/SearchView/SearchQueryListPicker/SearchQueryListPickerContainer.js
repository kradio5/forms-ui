import React from 'react'
import {Container} from 'flux/utils'
import SearchQueryItemStore from '../stores/SearchQueryItemStore'
import SearchQueryListPicker from './SearchQueryListPicker'

class SearchQueryListPickerContainer extends React.Component {
  static displayName = 'SearchQueryListPickerContainer'

  static getStores() {
    return [SearchQueryItemStore]
  }

  static calculateState() {
    return {
      items: SearchQueryItemStore.getAll(),
    }
  }

  render() {
    return this.state.items.size > 0 ? (
      <SearchQueryListPicker
        {...this.props}
        {...this.state}
      />
    ) : null
  }
}

export default Container.create(SearchQueryListPickerContainer)
