import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Icon from '@material-ui/core/Icon'

const styles = theme => ({
  root: {},
})

class SearchQueryListPickerItem extends React.PureComponent {
  static displayName = 'SearchQueryListPickerItem'

  static propTypes = {
    icon: PropTypes.string,
    item: PropTypes.shape({
      displayName: PropTypes.string,
      icon: PropTypes.string,
    }),
    onSubmit: PropTypes.func,
  }

  static defaultProps = {
    icon: 'folder',
  }

  handleSubmit = e => {
    e.preventDefault()
    this.props.onSubmit(this.props.item)
  }

  render() {
    const {
      classes,
      icon,
      item,
    } = this.props
    return (
      <ListItem
        button
        className={classes.root}
        onClick={this.handleSubmit}
      >
        <ListItemIcon>
          <Icon>{(item.icon || icon).replace(/-/g, '_')}</Icon>
        </ListItemIcon>
        <ListItemText primary={item.displayName} />
      </ListItem>
    )
  }
}

export default withStyles(styles, {withTheme: true})(SearchQueryListPickerItem)