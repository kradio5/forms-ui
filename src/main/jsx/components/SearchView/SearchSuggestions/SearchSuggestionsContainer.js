import React from 'react'
import PropTypes from 'prop-types'
import {Container} from 'flux/utils'
import AutocompletedSuggestionsStore from '../stores/AutocompletedSuggestionsStore'
import HistoricalSuggestionsStore from '../stores/HistoricalSuggestionsStore'
import SearchSuggestions from './SearchSuggestions'

class SearchSuggestionsContainer extends React.Component {
  static displayName = 'SearchSuggestionsContainer'

  static propTypes = {
    limit: PropTypes.number,
  }

  static defaultProps = {
    limit: Number.POSITIVE_INFINITY,
  }

  static getStores() {
    return [AutocompletedSuggestionsStore, HistoricalSuggestionsStore]
  }

  static calculateState() {
    const searchRecentQueries = HistoricalSuggestionsStore.getAll()
    const searchRecentQueriesKeys = searchRecentQueries.map(it => it.displayName)
    const searchQueries = AutocompletedSuggestionsStore.getAll()

    return {
      searchRecentQueries: searchRecentQueries,
      searchQueries: searchQueries.filter(it => !searchRecentQueriesKeys.includes(it.displayName)),
    }
  }

  render() {
    const {
      searchRecentQueries,
      searchQueries,
    } = this.state
    const {
      limit,
      ...otherProps
    } = this.props
    return (
      <div>
        {searchRecentQueries.length > 0 && (
          <SearchSuggestions
            {...otherProps}
            icon="history"
            suggestions={searchRecentQueries.slice(0, limit)}
          />
        )}
        {searchQueries.length > 0 && searchRecentQueries.length < limit && (
          <SearchSuggestions
            {...otherProps}
            suggestions={searchQueries.slice(0, limit - searchRecentQueries.length)}
          />
        )}
      </div>
    )
  }
}

export default Container.create(SearchSuggestionsContainer)
