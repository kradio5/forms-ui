import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import SearchSuggestion from './SearchSuggestion'

const styles = theme => ({
})

class SearchSuggestions extends React.PureComponent {
  static displayName = 'SearchSuggestions'

  static propTypes = {
    icon: PropTypes.string,
    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    suggestions: PropTypes.arrayOf(PropTypes.shape({
      displayName: PropTypes.string,
      value: PropTypes.shape({}),
    })),
  }

  static defaultProps = {
    icon: 'search',
    onChange: () => {},
    onSubmit: () => {},
  }

  _renderSuggestion(suggestion, index) {
    return (
      <SearchSuggestion
        icon={this.props.icon}
        key={index}
        onChange={this.props.onChange}
        onSubmit={this.props.onSubmit}
        suggestion={suggestion}
      />
    )
  }

  render() {
    const {classes, suggestions} = this.props
    return (
      <List disablePadding>
        {suggestions.map(this._renderSuggestion, this)}
      </List>
    )
  }
}

export default withStyles(styles, {withTheme: true})(SearchSuggestions)