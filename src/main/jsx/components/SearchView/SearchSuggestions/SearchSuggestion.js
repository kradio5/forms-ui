import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import IconButton from '@material-ui/core/IconButton'
import Icon from '@material-ui/core/Icon'

const styles = theme => ({
  root: {
    paddingLeft: theme.spacing.unit * 4,
    [theme.breakpoints.up('sm')]: {
      paddingLeft: theme.spacing.unit * 5,
    },
  },
  action: {
    right: theme.spacing.unit * 2,
    [theme.breakpoints.up('sm')]: {
      right: theme.spacing.unit * 3,
    },
  },
  actionIcon: {
    transform: 'rotate(45deg)',
  },
  listIcon: {
    marginRight: theme.spacing.unit,
  },
  listItemText: {
    paddingLeft: 0,
  },
})

class SearchSuggestion extends React.PureComponent {
  static displayName = 'SearchSuggestion'

  static propTypes = {
    icon: PropTypes.string,
    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    suggestion: PropTypes.shape({
      displayName: PropTypes.string,
      value: PropTypes.shape({}),
    }),
  }

  static defaultProps = {
    onChange: () => { },
    onSubmit: () => { },
  }

  handleChange = e => {
    e.preventDefault()
    e.stopPropagation()
    const {suggestion} = this.props
    this.props.onChange(suggestion.displayName)
  }

  handleSubmit = e => {
    e.preventDefault()
    const {hl, ...rest} = this.props.suggestion
    this.props.onSubmit(rest)
  }

  highlighter(result, range) {
    const [s = '', ...other] = result.props.children
    return React.cloneElement(
      result,
      null,
      s.substring(0, range[0]) || null,
      <strong>{s.slice(...range)}</strong>,
      s.substring(range[1]) || null,
      ...other
    )
  }

  render() {
    const {classes, icon, suggestion} = this.props
    const {hl, displayName: text} = suggestion
    return (
      <ListItem
        button
        className={classes.root}
        onClick={this.handleSubmit}
      >
        <ListItemIcon className={classes.listIcon}>
          <Icon>{icon}</Icon>
        </ListItemIcon>
        <ListItemText
          className={classes.listItemText}
          primary={hl ? hl.reduceRight(this.highlighter, <span>{[text]}</span>) : text}
        />
        <ListItemSecondaryAction className={classes.action}>
          <IconButton
            color="default"
            onClick={this.handleChange}
          >
            <Icon className={classes.actionIcon}>arrow_back</Icon>
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
    )
  }
}

export default withStyles(styles, {withTheme: true})(SearchSuggestion)
