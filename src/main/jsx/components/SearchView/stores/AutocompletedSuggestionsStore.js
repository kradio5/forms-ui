import AppDispatcher from 'dispatcher/AppDispatcher'
import {ReduceStore} from 'flux/utils'
import {ActionTypes} from '../constants/SearchConstants'

class AutocompletedSuggestionsStore extends ReduceStore {
  getInitialState() {
    return []
  }

  getAll() {
    return this.getState()
  }

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.AUTOCOMPLETED_SUGGESTIONS_LOAD:
        return action.data
      case ActionTypes.AUTOCOMPLETED_SUGGESTIONS_CLEAR:
        return this.getInitialState()
      default:
        return state
    }
  }
}

export default new AutocompletedSuggestionsStore(AppDispatcher)
