import AppDispatcher from 'dispatcher/AppDispatcher'
import {ReduceStore} from 'flux/utils'
import {ActionTypes} from '../constants/SearchConstants'

class HistoricalSuggestionsStore extends ReduceStore {
  getInitialState() {
    return []
  }

  getAll() {
    return this.getState()
  }

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.HISTORICAL_SUGGESTIONS_LOAD:
        return action.data
      case ActionTypes.HISTORICAL_SUGGESTIONS_CLEAR:
        return this.getInitialState()
      default:
        return state
    }
  }
}

export default new HistoricalSuggestionsStore(AppDispatcher)
