import AppDispatcher from 'dispatcher/AppDispatcher'
import {ReduceStore} from 'flux/utils'
import {ActionTypes} from '../constants/SearchConstants'

class SearchResultItemStore extends ReduceStore {
  getInitialState() {
    return {
      // resource: {},
      results: [],
    }
  }

  getAll() {
    return this.getState().results
  }

  /*   getResource() {
    return this.getState().resource
  } */

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.SEARCH_RESULT_ITEMS_CLEAR:
        return this.getInitialState()
      case ActionTypes.SEARCH_RESULT_ITEMS_LOAD:
        return action.data
      default:
        return state
    }
  }
}

export default new SearchResultItemStore(AppDispatcher)