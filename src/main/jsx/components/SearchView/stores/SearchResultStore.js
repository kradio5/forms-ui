import AppDispatcher from 'dispatcher/AppDispatcher'
import {ReduceStore} from 'flux/utils'
import {ActionTypes} from '../constants/SearchConstants'

class SearchResultStore extends ReduceStore {
  getInitialState() {
    return {
      resource: {},
      results: [],
    }
  }

  getAll() {
    return this.getState().results
  }

  getResource() {
    return this.getState().resource
  }

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.SEARCH_RESULTS_LOAD:
        return action.data
      case ActionTypes.SEARCH_RESULTS_NEXT:
        return {
          ...action.data,
          results: state.results.concat(action.data.results),
        }
      case ActionTypes.SEARCH_RESULTS_CLEAR:
        return this.getInitialState()
      default:
        return state
    }
  }
}

export default new SearchResultStore(AppDispatcher)
