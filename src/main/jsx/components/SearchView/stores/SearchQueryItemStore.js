import AppDispatcher from 'dispatcher/AppDispatcher'
import {ReduceStore} from 'flux/utils'
import {ActionTypes} from '../constants/SearchConstants'

class SearchQueryItemStore extends ReduceStore {
  getInitialState() {
    return []
  }

  getAll() {
    return this.getState()
  }

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.SEARCH_QUERY_ITEMS_CLEAR:
        return this.getInitialState()
      case ActionTypes.SEARCH_QUERY_ITEMS_LOAD:
        return action.data
      default:
        return state
    }
  }
}

export default new SearchQueryItemStore(AppDispatcher)