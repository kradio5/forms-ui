import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import OperationStatus from 'components/OperationStatus'

const styles = theme => ({
  root: {
    position: 'relative',
    // height: '100vh',
    height: '100%',
  },
})

class AppLayout extends React.Component {
  static displayName = 'AppLayout'

  static propTypes = {
    children: PropTypes.node,
  }

  render() {
    const {classes} = this.props
    return (
      <div className={classes.root}>
        {this.props.children}
        <OperationStatus />
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(AppLayout)
