import React from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import Paper from '@material-ui/core/Paper'
import {withStyles} from '@material-ui/core/styles'

const styles = {
  root: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    display: 'flex',
    flexWrap: 'nowrap',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    width: 68,
    height: 68,
  },
}

class Preloader extends React.Component {
  static displayName = 'Preloader'

  static propTypes = {
    color: PropTypes.string,
    paper: PropTypes.bool,
  }

  static defaultProps = {
    color: 'secondary',
    paper: false,
  }

  render() {
    const {classes, color, error, isLoading, paper} = this.props
    if (!isLoading || error) {
      return null
    }
    return (
      <div className={classes.root}>
        {paper ? (
          <Paper className={classes.paper}>
            <CircularProgress color={color} />
          </Paper>
        ) : (
          <CircularProgress color={color} />
        )}
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(Preloader)
