import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import {lighten} from '@material-ui/core/styles/colorManipulator'
import Avatar from '@material-ui/core/Avatar'
import Icon from '@material-ui/core/Icon'
import Typography from '@material-ui/core/Typography'
import classNames from 'classnames'
import i18n from 'util/i18n'

const styles = theme => ({
  root: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexFlow: 'column nowrap',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    textAlign: 'center',
    userSelect: 'none',
    display: 'flex',
    flexFlow: 'column',
    alignItems: 'center',
  },
  avatar: {
    width: 112,
    height: 112,
    backgroundColor: lighten(theme.palette.primary.main, .85),
    marginBottom: theme.spacing.unit * 2,
  },
  icon: {
    fontSize: 56,
    color: theme.palette.background.disabled,
  },
})

class NoItems extends React.Component {
  static displayName = 'NoItems'

  static propTypes = {
    icon: PropTypes.string,
    title: PropTypes.string,
  }

  static defaultProps = {
    icon: 'accessibility',
    title: i18n.t('ui.controls.NoItems.title'),
  }

  render() {
    const {classes, className, icon, title} = this.props
    return (
      <div className={classNames(classes.root, className)}>
        <div className={classes.content}>
          <Avatar className={classes.avatar}>
            <Icon className={classes.icon}>{icon}</Icon>
          </Avatar>
          <Typography variant="subtitle1">
            {title}
          </Typography>
        </div>
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(NoItems)
