import React from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'
import classNames from 'classnames'

const styles = theme => ({
  root: {
    // marginRight: '3px',
    borderRadius: '12px',
    padding: '5px 12px',
    textTransform: 'uppercase',
    fontSize: '10px',
    fontWeight: '500',
    lineHeight: '1',
    letterSpacing: 0,
    color: theme.palette.common.white,
    textAlign: 'center',
    whiteSpace: 'nowrap',
    verticalAlign: 'baseline',
    display: 'inline-block',
  },
  primary: {
    backgroundColor: theme.palette.materialKit.primary,
  },
  warning: {
    backgroundColor: theme.palette.materialKit.warning,
  },
  danger: {
    backgroundColor: theme.palette.materialKit.danger,
  },
  success: {
    backgroundColor: theme.palette.materialKit.success,
  },
  info: {
    backgroundColor: theme.palette.materialKit.info,
  },
  rose: {
    backgroundColor: theme.palette.materialKit.rose,
  },
  gray: {
    backgroundColor: theme.palette.materialKit.gray,
  },
  default: {
    backgroundColor: theme.palette.materialKit.default,
  },
})

function Badge({...props}) {
  const {children, className, classes, color, component} = props
  return React.createElement(
    component,
    {className: classNames(classes.root, classes[color], className)},
    children
  )
}

Badge.defaultProps = {
  color: 'default',
  component: 'span',
}

Badge.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  classes: PropTypes.shape({
    root: PropTypes.string,
  }),
  color: PropTypes.oneOf([
    'primary',
    'warning',
    'danger',
    'success',
    'info',
    'rose',
    'gray',
    'inverse',
    'default',
  ]),
  component: PropTypes.string,
}

export default withStyles(styles, {withTheme: true})(Badge)
