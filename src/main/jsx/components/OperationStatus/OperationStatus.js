import React from 'react'
import PropTypes from 'prop-types'
import OperationStatusActions from 'actions/OperationStatusActions'
import Button from '@material-ui/core/Button'
import Snackbar from '@material-ui/core/Snackbar'
import Typography from '@material-ui/core/Typography'

class OperationStatus extends React.Component {
  static displayName = 'OperationStatus'

  static propTypes = {
    closeTimeout: PropTypes.number,
    status: PropTypes.shape({
      action: PropTypes.string,
      actionHandler: PropTypes.func,
      text: PropTypes.string,
    }),
  }

  static defaultProps = {
    closeTimeout: 3000,
    status: {text: ''},
  }

  handleClose = () => {
    OperationStatusActions.relieve()
  }

  handleAction = () => {
    this.props.status.actionHandler()
    OperationStatusActions.relieve()
  }

  render() {
    const {
      closeTimeout,
      status,
    } = this.props
    const message = status.text && (
      <Typography
        color="inherit"
        id="operation-status-message-id"
        noWrap
      >
        {status.text}
      </Typography>
    )
    const action = status.action ? (
      <Button
        color="secondary"
        dense
        key={action}
        onClick={this.handleAction}
      >
        {action}
      </Button>
    ) : null
    return (
      <Snackbar
        action={action}
        autoHideDuration={3000}
        key={status}
        message={message}
        onClose={this.handleClose}
        open={Boolean(status.text)}
        SnackbarContentProps={{'aria-describedby': 'operation-status-message-id'}}
      />
    )
  }
}

export default OperationStatus
