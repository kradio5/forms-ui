import React from 'react'
import {Container} from 'flux/utils'
import OperationStatusStore from 'stores/OperationStatusStore'
import OperationStatus from './OperationStatus'

class OperationStatusContainer extends React.Component {
  static displayName = 'OperationStatusContainer'

  static getStores() {
    return [OperationStatusStore]
  }

  static calculateState() {
    return {
      status: OperationStatusStore.peek(),
    }
  }

  render() {
    return (
      <OperationStatus
        {...this.props}
        {...this.state}
      />
    )
  }
}

export default Container.create(OperationStatusContainer)