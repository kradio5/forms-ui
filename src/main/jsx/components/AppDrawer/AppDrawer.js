import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import DrawerLinks from 'routes/links/Drawer'
import Drawer from '@material-ui/core/Drawer'
import Divider from '@material-ui/core/Divider'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListSubheader from '@material-ui/core/ListSubheader'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'
import NewNotesNotificator from 'components/NewNotesNotificator'
import Collapse from '@material-ui/core/Collapse'
import Icon from '@material-ui/core/Icon'
import AuthActions from 'actions/AuthActions'
import DrawerHeader from './DrawerHeader'
import i18n from 'util/i18n'

const drawerWidth = 250
const styles = theme => ({
  docked: {
    height: '100%',
    // height: '100vh',
  },
  paper: {
    height: '100%',
    overflowY: 'auto',
    display: 'flex',
    flexFlow: 'column nowrap',
    justifyContent: 'space-between',
    width: drawerWidth + 10,
    [theme.breakpoints.up('md')]: {
      width: drawerWidth,
      position: 'relative',
      height: '100%',
    },
  },
  content: {
  },
  badge: {
    marginRight: theme.spacing.unit * 2,
    [theme.breakpoints.up('sm')]: {
      marginRight: theme.spacing.unit,
    },
  },
  footer: {
    flex: '0 1 auto',
  },
})

class AppDrawer extends React.Component {
  static displayName = 'AppDrawer'

  static propTypes = {
    onClose: PropTypes.func,
    open: PropTypes.bool,
  }

  static defaultProps = {
    onClose: () => { },
  }

  state = {
    open: false,
  }

  handleToggleOpen = () => {
    this.setState(({open}) => ({
      open: !open,
    }))
  }

  handleClickLink = name => {
    switch (name) {
      case 'logout':
        AuthActions.unauthorize()
        break
    }
  }

  _renderLink(link) {
    const {classes} = this.props
    return (
      <ListItem
        button
        component="a"
        href={DrawerLinks.linkTo(link.href)}
        key={link.name}
        onClick={() => this.handleClickLink(link.name)}
      >
        <ListItemIcon>
          <Icon>{link.icon}</Icon>
        </ListItemIcon>
        <ListItemText primary={i18n.t(`ui.controls.Drawer.links.${link.name}.title`)} />
        {link.name === 'notes' && (
          <ListItemSecondaryAction>
            <NewNotesNotificator className={classes.badge}>
              <span />
            </NewNotesNotificator>
          </ListItemSecondaryAction>
        )}
      </ListItem>
    )
  }

  render() {
    const {classes, theme, ...restProps} = this.props
    const {open} = this.state
    const {sections = {user: [], main: [], my: [], contacts: []}} = window.ydw.config.navigation
    return (
      <Drawer
        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
        classes={{paper: classes.paper, docked: classes.docked}}
        {...restProps}
      >
        <div className={classes.content}>
          <DrawerHeader
            onToggleOpen={this.handleToggleOpen}
            open={open}
          />
          <Collapse
            in={open}
            timeout={0}
            unmountOnExit
          >
            <List component="nav">
              {(sections.user || []).map(this._renderLink, this)}
            </List>
          </Collapse>
          <Collapse
            in={!open}
            timeout={0}
            unmountOnExit
          >
            <List component="nav">
              {(sections.main || []).map(this._renderLink, this)}
            </List>
            <Divider />
            <List
              component="nav"
              subheader={<ListSubheader>{i18n.t('ui.controls.Drawer.sections.my.title')}</ListSubheader>}
            >
              {(sections.my || []).map(this._renderLink, this)}
            </List>
            <Divider />
            <List component="nav">
              {(sections.contacts || []).map(this._renderLink, this)}
            </List>
          </Collapse>
        </div>
        {'footer' in sections && (
          <Collapse
            in={!open}
            timeout={0}
            unmountOnExit
          >
            <List
              className={classes.footer}
              component="nav"
            >
              {sections.footer.map(this._renderLink, this)}
            </List>
          </Collapse>
        )}
      </Drawer>
    )
  }
}

export default withStyles(styles, {withTheme: true})(AppDrawer)
