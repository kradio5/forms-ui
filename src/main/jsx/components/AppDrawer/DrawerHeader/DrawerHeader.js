import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import {lighten} from '@material-ui/core/styles/colorManipulator'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'
import UserStore from 'stores/UserStore'
import AppLogo from 'components/AppLogo'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import Avatar from '@material-ui/core/Avatar'

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    padding: [[0, theme.spacing.unit * 2, 0]],
    minHeight: 212,
    backgroundColor: lighten(theme.palette.primary.main, .85),
    userSelect: 'none',
    overflow: 'hidden',
    position: 'relative',
  },
  controls: {
    ...theme.mixins.toolbar,
    width: '100%',
  },
  avatar: {
    width: 80,
    height: 80,
    backgroundColor: theme.palette.common.white,
  },
  avatarIcon: {
    fontSize: '3em',
    color: lighten(theme.palette.primary.main, .85),
  },
  content: {
    width: '100%',
    cursor: 'pointer',
  },
  secondaryAction: {
    right: -12,
  },
  list: {
    padding: [[10, 0]],
  },
  disableHover: {
    /*
    '&:focus': {
      backgroundColor: 'transparent',
    },
    */
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  backgroundIcon: {
    position: 'absolute',
    left: 30,
    top: -50,
    fontSize: 300,
    color: lighten(theme.palette.primary.main, .90),
    transform: 'scaleX(-1)',
  },
  logo: {
    width: 350,
    top: 60,
    left: -50,
    position: 'absolute',
    backgroundColor: 'transparent',
    opacity: .5,
  },
})

class DrawerHeader extends React.Component {
  static displayName = 'DrawerHeader'

  static propTypes = {
    controls: PropTypes.node,
    onToggleOpen: PropTypes.func,
    open: PropTypes.bool,
  }

  handleClick = () => {
    this.props.onToggleOpen()
  }

  render() {
    const {classes, open} = this.props
    const user = UserStore.getCurrentUser()
    return (
      <div className={classes.root}>
        <Icon className={classes.backgroundIcon}>{'directions_bike'}</Icon>
        <AppLogo
          className={classes.logo}
        />
        <div className={classes.controls}>
          {this.props.controls}
        </div>
        <div
          className={classes.content}
          onClick={this.handleClick}
        >
          {user && user.props.imageUrl ? (
            <Avatar
              alt={user.props.displayName}
              className={classes.avatar}
              src={user.props.imageUrl}
            />
          ) : (
            <Avatar className={classes.avatar}>
              <Icon className={classes.avatarIcon}>{'person'}</Icon>
            </Avatar>
          )}
          <List className={classes.list}>
            <ListItem
              dense
              disableGutters
            >
              <ListItemText
                primary={user ? user.props.displayName : 'Anonymous'}
              />
              <ListItemSecondaryAction className={classes.secondaryAction}>
                <IconButton
                  className={classes.disableHover}
                  color="primary"
                  disableRipple
                  tabIndex={-1}
                >
                  <Icon>{`arrow_drop_${open ? 'up' : 'down'}`}</Icon>
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          </List>
        </div>
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(DrawerHeader)