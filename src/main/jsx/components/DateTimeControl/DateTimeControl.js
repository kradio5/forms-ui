import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import DatePicker from 'components/DatePicker'
import TimePicker from 'components/TimePicker'

const styles = theme => ({
  root: {
    width: '100%',
    display: 'flex',
    flexDirection: theme.direction === 'rtl' ? 'row-reverse' : 'row',
    flexWrap: 'nowrap',
  },
})

class DateTimeControl extends React.Component {
  static displayName = 'DateTimeControl'

  static propTypes = {
    onChange: PropTypes.func,
    value: PropTypes.instanceOf(Date),
  }

  static defaultProps = {
    onChange: () => { },
    value: new Date(),
  }

  constructor(props) {
    super(props)

    this.state = {
      value: props.value,
    }
  }

  componentWillReceiveProps({value}) {
    this.setState({value})
  }

  handleDateChange = val => {
    const {value} = this.state
    val.setHours(value.getHours(), value.getMinutes())
    this.setState({value: val}, () => {
      this.props.onChange(this.state.value)
    })
  }

  handleTimeChange = val => {
    const value = new Date()
    value.setTime(this.state.value.getTime())
    value.setHours(val.getHours(), val.getMinutes())
    this.setState({value}, ()  => {
      this.props.onChange(this.state.value)
    })
  }

  render() {
    const {classes} = this.props
    const {value} = this.state
    return (
      <div className={classes.root}>
        <DatePicker
          onChange={this.handleDateChange}
          value={value}
        />
        <TimePicker
          onChange={this.handleTimeChange}
          value={value}
        />
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(DateTimeControl)
