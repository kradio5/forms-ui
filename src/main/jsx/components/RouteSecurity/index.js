import {matchPath} from 'react-router'

class RouteSecurity {
  constructor(rules) {
    this._rules = rules
  }

  addRule(rule) {
    this._rules.push(rule)
  }

  isSecure(url) {
    for (let rule of this._rules) {
      if (matchPath(url, {path: rule[0]})) {
        return rule[1]()
      }
    }
    return false
  }
}

export default RouteSecurity
