import React from 'react'
import PropTypes from 'prop-types'
import {Container} from 'flux/utils'
import AuthStore from 'stores/AuthStore'
import AuthActions from 'actions/AuthActions'
import NavigationActions from 'actions/NavigationActions'
import authUtil from 'util/auth'
import RouteSecurity from 'routes/security'

export default ComposedComponent => Container.create(
  class Authenticator extends React.Component {
    static displayName = 'Authenticator'

    static propTypes = {
      children: PropTypes.node,
      location: PropTypes.shape({
        hash: PropTypes.string,
        pathname: PropTypes.string,
        search: PropTypes.string,
      }),
    }

    static calculateState(prevState, props) {
      const {pathname} = props.location
      const isAuthorized = AuthStore.isAuthorized()
      const isTokenResetRequired = AuthStore.isTokenResetRequired()
      const isLoading = RouteSecurity.isSecure(pathname) ?
        (!isAuthorized || isTokenResetRequired) :
        (AuthStore.hasToken() && isTokenResetRequired)
      return {
        isAuthorized,
        isLoading,
      }
    }

    static getStores() {
      return [AuthStore]
    }

    componentWillMount() {
      console.log('will mount')
      const {hash, pathname, search} = this.props.location

      //authUtil.parseHash({hash}, (err, credentials) => {
      //  if (hash && credentials) {
      //    AuthActions.storeCredentials(credentials)
      //    AuthActions.authorizeApi(credentials.accessToken)
      //    NavigationActions.replace(`${pathname}${search}`)
      //  } else {
      if (AuthStore.hasToken()) {
        if (AuthStore.isTokenResetRequired()) {
          AuthActions.reauthorize()
        } else {
          AuthActions.authorizeApi(AuthStore.getToken())
        }
      } else if (RouteSecurity.isSecure(pathname)) {
        //AuthActions.authorize()
        authUtil.signinRedirect()
        console.log(hash)
      }

        authUtil.signinRedirectCallback().then((user)=> {
          console.log("signed in", user);
        })
      //}
      //})
    }

    componentWillReceiveProps(nextProps) {
      console.log('will receive props')
      if (this.state.isLoading) {
        return
      }
      const {pathname} = nextProps.location
      if (AuthStore.hasToken()) {
        if (AuthStore.isTokenResetRequired()) {
          AuthActions.reauthorize()
        }
      } else if (RouteSecurity.isSecure(pathname)) {
        //AuthActions.authorize()
      }
    }

    render() {
      if (this.state.isLoading) {
        return null
      }

      const {children, ...otherProps} = this.props
      return (
        <ComposedComponent {...otherProps}>
          {children}
        </ComposedComponent>
      )
    }
  }, {withProps: true}
)
