import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import Input from '@material-ui/core/Input'
import InputAdornment from '@material-ui/core/InputAdornment'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import FormGroup from '@material-ui/core/FormGroup'
import FormHelperText from '@material-ui/core/FormHelperText'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import classNames from 'classnames'
import math from 'util/math'
import i18n from 'util/i18n'

const RE_FIXED_2 = /(?:0(?!0|[1-9])|[1-9])\d*(?:\.\d{0,2})?/
const RE_FIXED_3 = /(?:0(?!0|[1-9])|[1-9])\d*(?:\.\d{0,3})?/
const RE_INT = /(?:0(?!0|[1-9])|[1-9])\d{0,8}/

const styles = theme => ({
  container: {
    alignItems: 'baseline',
    display: 'flex',
    flexFlow: 'row nowrap',
    overflow: 'hidden',
    width: '100%',
  },
  valueInputs: {
    minWidth: 190,
  },
  valuesInput: {
    position: 'relative',
    width: '100%',
    marginBottom: 0,
    '& input': {
      textAlign: 'center',
    },
  },
  valuesInputDelim: {
    marginLeft: theme.spacing.unit,
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 1.5,
    },
    [theme.breakpoints.up('md')]: {
      marginLeft: theme.spacing.unit * 2,
    },
    /*
    '&:before': {
      content: '":"',
      position: 'absolute',
      left: -theme.spacing.unit,
      top: theme.spacing.unit,
      opacity: 0.6
    },
    */
  },
  /*
  valueInput: {
    marginRight: theme.spacing.unit,
  },
  unitSelect: {
    marginLeft: theme.spacing.unit / 2,
  },
  */
  formControl: {
    display: 'flex',
    flexFlow: 'row nowrap',
  },
  formGroup: {
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'space-between',
    marginRight: theme.spacing.unit,
    [theme.breakpoints.up('sm')]: {
      marginRight: theme.spacing.unit * 1.5,
    },
    [theme.breakpoints.up('md')]: {
      marginRight: theme.spacing.unit * 2,
    },
  },
})

class DurationControl extends React.Component {
  static displayName = 'DurationControl'

  static propTypes = {
    disabled: PropTypes.bool,
    errorText: PropTypes.string,
    id: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func,
    onRef: PropTypes.func,
    onUnitChange: PropTypes.func,
    unit: PropTypes.oneOf(['h', 'min', 's']),
    validator: PropTypes.func,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
  }

  static defaultProps = {
    disabled: false,
    errorText: i18n.t('ui.controls.ActivityParametersControl.errorText'),
    label: i18n.t('model.Duration.title'),
    onChange: () => { },
    onRef: () => { },
    unit: 'h',
    validator: function (value) {
      return value.toNumber('s') > 0
    },
    value: '',
  }

  constructor(props) {
    super(props)

    this.state = {
      ...this._createState(props),
      isDirty: false,
    }
  }

  componentDidMount() {
    this.props.onRef(this)
  }

  componentWillReceiveProps(nextProps) {
    const {isDirty, value} = this.state
    if (!isDirty && value.toNumber() !== Number(nextProps.value)) {
      this.setState(this._createState(nextProps))
    }
  }

  _createState(props) {
    const value = math.unit(props.value, props.unit)
    return {
      invalid: false,
      value: value,
      unit: props.unit,
      rawValue: props.value && this._normalizeOut(value).toString(),
    }
  }

  isValid() {
    return this.props.validator(this.state.value)
  }

  validate() {
    const isValid = this.isValid()

    this.setState({
      invalid: !isValid,
    })

    return isValid
  }

  handleBlur = () => {
    this.setState({activeUnit: null})
  }

  handleFocus = e => {
    if (this.state.unit === 'h') {
      this.setState({activeUnit: e.target.name})
    }
  }

  handleChange = e => {
    let rawValue = this._normalizeIn(e.target.value)
    let val = !rawValue ? '0' : rawValue

    if (Number.isNaN(val)) {
      return
    }

    if (this.state.unit === 'h') {
      let fieldUnit = e.target.name
      val = this.state.value
        .splitUnit(['h', 'min', 's'])
        .reduce((v, u) => {
          const name = u.units[0].unit.name
          return math.add(v, name === fieldUnit ? math.unit(val, name) : u)
        }, math.unit(0, 'h'))
    } else {
      val = math.unit(val, this.state.unit)
    }

    this.setState({
      isDirty: true,
      invalid: false,
      value: val,
      rawValue: rawValue,
    }, () => {
      this.props.onChange({
        name: this.props.name,
        value: this._normalizeOut(this.state.value),
        unit: this.state.unit,
      })
    })
  }

  handleUnitChange = e => {
    const {value: val} = e.target
    const newValue = this.state.value.to(val)
    const normalized = this._normalizeOut(newValue)
    this.setState({
      unit: val,
      value: newValue,
      rawValue: normalized ? normalized.toString() : '',
    }, () => {
      this.props.onChange({
        name: this.props.name,
        value: this._normalizeOut(this.state.value),
        unit: this.state.unit,
      })
    })
  }

  _normalizeIn(val) {
    let s = (val || '').toString()
      .replace(/,+/g, '.')

    switch (this.state.unit) {
      case 'min':
        return (s.match(RE_FIXED_2) || [''])[0]
      case 's':
        return (s.match(RE_FIXED_3) || [''])[0]
      default:
        return (s.match(RE_INT) || [''])[0]
    }
  }

  _normalizeOut(val) {
    const unit = val.units[0].unit.name
    let precision
    switch (unit) {
      case 'min':
        precision = 2
        break
      case 's':
        precision = 3
        break
      case 'h':
        return val.toNumber()
    }
    return val.toNumber() && Number(math.format(val.toNumber(), {
      notation: 'fixed',
      precision: precision,
    }))
  }

  _renderValueInput() {
    const {
      unit,
      rawValue,
    } = this.state
    let precision = 0
    switch (unit) {
      case 'min':
        precision = 2
        break
      case 's':
        precision = 3
        break
    }
    const {classes, disabled, name} = this.props
    return (
      <Input
        className={classes.valueInput}
        disabled={disabled}
        fullWidth
        id={`${name}-${unit}`}
        name={name}
        onBlur={this.handleBlur}
        onChange={this.handleChange}
        onFocus={this.handleFocus}
        placeholder={`90.${'9'.repeat(precision)}`}
        value={rawValue}
      />
    )
  }

  _renderValuesInput(unit, index) {
    const name = unit.units[0].unit.name
    const value = Number.parseInt(unit.toNumber())
    const {classes, disabled} = this.props
    const className = classNames(classes.valuesInput, {
      [classes.valuesInputDelim]: index > 0,
    })
    const names = ['hr', 'min', 'sec']
    const endAdornment = (
      <InputAdornment position="end">{names[index]}</InputAdornment>
    )
    return (
      <FormControl
        className={className}
        fullWidth
        key={index}
        margin="normal"
      >
        <Input
          disabled={disabled}
          endAdornment={endAdornment}
          id={`time-${name}`}
          maxLength="2"
          name={name}
          onBlur={this.handleBlur}
          onChange={this.handleChange}
          onFocus={this.handleFocus}
          placeholder="00"
          value={value > 0 ? value : ' '}
        />
      </FormControl>
    )
  }

  render() {
    const {
      classes,
      disabled,
      errorText,
      label,
      name,
    } = this.props
    const {
      activeUnit,
      invalid,
      value,
      unit,
    } = this.state
    /*
    const unitKey = unit.replace('/', '_')
    const unitName = activeUnit ?
      i18n.t(`uom.Unit.${activeUnit.toUpperCase()}.title`) :
      (unit === 'h' ? '' : i18n.t(`uom.Unit.${unitKey.toUpperCase()}.title`))
    */
    return (
      <div className={classes.container}>
        <FormControl
          aria-describedby="duration-control-text"
          className={classNames({[classes.valueInputs]: unit === 'h'})}
          disabled={disabled}
          error={invalid}
          fullWidth
          margin="normal"
        >
          <InputLabel
            htmlFor={unit === 'h' ? `time-${unit}` : `${name}-${unit}`}
            shrink
          >
            {/*unitName ? `${label}, ${unitName.toLowerCase()}` : */label}
          </InputLabel>
          {unit === 'h' ? (
            <FormGroup
              className={classes.formGroup}
              row
            >
              {value.splitUnit(['h', 'min', 's']).map(this._renderValuesInput, this)}
            </FormGroup>
          ) : this._renderValueInput()}
          {invalid && (
            <FormHelperText id="duration-control-text">
              {value > 0 ? errorText : i18n.t('ui.controls.TextFieldRequired.errorText')}
            </FormHelperText>
          )}
        </FormControl>
        {disabled || (
          <TextField
            className={classes.unitSelect}
            fullWidth
            margin="normal"
            onChange={this.handleUnitChange}
            select
            value={unit}
          >
            <MenuItem value={'h'}>
              {i18n.t('ui.titles.time')}
            </MenuItem>
            <MenuItem value={'min'}>
              {i18n.t('ui.titles.minutes')}
            </MenuItem>
            <MenuItem value={'s'}>
              {i18n.t('ui.titles.seconds')}
            </MenuItem>
          </TextField>
        )}
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(DurationControl)
