import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import NavigationActions from 'actions/NavigationActions'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import i18n from 'util/i18n'

const styles = theme => ({
  root: theme.mixins.toolbarMenuButton,
})

class BackButton extends React.Component {
  static displayName = 'BackButton'

  static propTypes = {
    color: PropTypes.string,
    icon: PropTypes.string,
    label: PropTypes.string,
    onClick: PropTypes.func,
  }

  static defaultProps = {
    color: 'secondary',
    icon: 'arrow_back',
    label: i18n.t('ui.controls.BackButton.label', {_: 'Back'}),
    onClick: () => NavigationActions.goBack(),
  }

  render() {
    const {classes, color, icon, label, onClick} = this.props
    return (
      <IconButton
        aria-label={label}
        className={classes.root}
        color={color}
        onClick={onClick}
      >
        <Icon>{icon}</Icon>
      </IconButton>
    )
  }
}

export default withStyles(styles, {withTheme: true})(BackButton)
