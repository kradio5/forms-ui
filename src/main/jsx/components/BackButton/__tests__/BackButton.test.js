jest.mock('actions/NavigationActions')

import React from 'react'
// import { createShallow, getClasses } from '@material-ui/core/test-utils'
import {shallow} from 'enzyme'
import BackButton from 'components/BackButton'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import NavigationActions from 'actions/NavigationActions'
import muiTheme from 'components/App/MuiTheme'

const shallowWithContext = (node) => shallow(node, {context: {muiTheme}})

describe('Back Button', () => {
/*
  let shallow
  let classes

  beforeAll(() => {
    shallow = createShallow({ dive: true })
    classes = getClasses(<BackButton />)
  })
*/
  it('should render', () => {
    expect(true).toBeTruthy()
  })
/*
  it('should render Icon', () => {
    const wrapper = shallowWithContext(<BackButton />)
    const icon = wrapper.find(Icon)

    expect(icon).toHaveLength(1)
    expect(icon.props()).toHaveProperty('children', 'arrow_back')
  })

  it('should call NavigationActions.goBack', () => {
    const wrapper = shallowWithContext(<BackButton />)

    wrapper.simulate('click')
    expect(NavigationActions.goBack).toBeCalled()
  })
*/
})
