import React from 'react'
import PropTypes from 'prop-types'
import {AuthApi, CoachApi} from 'api'
import moment from 'util/moment'
import i18n from 'util/i18n'
import _ from 'lodash'

const AVAILABLE_LOCALES = ['en', 'de']
  .map(x => x.substr(0, 2)) // Shorten strings to use two chars (en-US -> en)

export default ComposedComponent =>
  class Localizator extends React.Component {
    static displayName = 'Localizator'

    static propTypes = {
      children: PropTypes.node,
    }

    state = {
      isLoading: true,
    }

    componentWillMount() {
      const locale = this._getLocale()
      import('translations/' + locale)
        .then(m => m.default || m)
        .then(translation => {
          CoachApi.setLocale(locale)
          AuthApi.setLocale(locale)
          moment.locale(locale)
          i18n.locale(locale)
          const {translations = {}} = window.ydw
          i18n.extend({
            ...translation.phrases,
            ...(translations[locale] || {}),
          })
          this.setState({isLoading: false})
        })
    }

    _getLocale() {
      return this._getQueryLocale() ||
        this._getBrowserLocale() ||
        AVAILABLE_LOCALES[0]
    }

    _getQueryLocale() {
      const url = new URL(window.location.href)
      const locale = (url.searchParams.get('locale') || '').substr(0, 2)
      return AVAILABLE_LOCALES.includes(locale) ? locale : null
    }

    _getBrowserLocale() {
      const browserLanguagePropertyKeys = ['languages', 'language', 'browserLanguage', 'userLanguage', 'systemLanguage']
      return _.chain(window.navigator)
        .pick(browserLanguagePropertyKeys) // Get only language properties
        .values() // Get values of the properties
        .flatten() // flatten all arrays
        .compact() // Remove undefined values
        .map(x => x.substr(0, 2)) // Shorten strings to use two chars (en-US -> en)
        .find(x => AVAILABLE_LOCALES.includes(x)) // Returns first language matched in available languages
        .value()
    }

    render() {
      if (this.state.isLoading) {
        return null
      }

      return (
        <ComposedComponent>
          {this.props.children}
        </ComposedComponent>
      )
    }
  }
