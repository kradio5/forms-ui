import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import i18n from 'util/i18n'

const styles = theme => ({
  root: {},
})

const VALUES = ['EVENT', 'EVENT_AND_FOLLOWING_EVENTS']

class RecurringEventDialog extends React.Component {
  static displayName = 'RecurringEventDialog'

  static propTypes = {
    action: PropTypes.oneOf(['Delete', 'Save']),
    onClose: PropTypes.func,
    onSubmit: PropTypes.func,
    open: PropTypes.bool,
    value: PropTypes.oneOf(VALUES),
  }

  static defaultProps = {
    open: false,
    value: VALUES[0],
  }

  constructor(props) {
    super(props)

    this.state = {
      value: props.value,
    }
  }

  handleClose = () => {
    this.props.onClose()
  }

  handleChange = (e, value) => {
    this.setState({value})
  }

  handleEntering = () => {
    this.radioGroup_.focus()
  }

  handleSubmit = () => {
    this.props.onSubmit(this.state.value === VALUES[1])
  }

  render() {
    const {action, classes, open} = this.props
    return (
      <Dialog
        aria-labelledby="save-recurring-event-dialog-title"
        onClose={this.handleClose}
        onEntering={this.handleEntering}
        open={open}
      >
        <DialogTitle id="recurring-event-dialog-title">
          {i18n.t(`ui.controls.${action}RecurringEventDialog.title`)}
        </DialogTitle>
        <DialogContent>
          <RadioGroup
            aria-label="Recurring event dialog"
            name="recurring-event-dialog"
            onChange={this.handleChange}
            ref={ref => this.radioGroup_ = ref}
            value={this.state.value}
          >
            {VALUES.map(value => (
              <FormControlLabel
                control={<Radio />}
                key={value}
                label={i18n.t(`ui.controls.${action}RecurringEventDialog.option.${value}.label`)}
                value={value}
              />
            ))}
          </RadioGroup>
        </DialogContent>
        <DialogActions>
          <Button
            color="secondary"
            onClick={this.handleClose}
          >
            {i18n.t('ui.controls.Cancel.title')}
          </Button>
          <Button
            autoFocus
            color="secondary"
            onClick={this.handleSubmit}
          >
            {i18n.t(`ui.controls.${action}.title`)}
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

export default withStyles(styles, {withTheme: true})(RecurringEventDialog)
