import React from 'react'
import PropTypes from 'prop-types'
import {ActivityParameterControl} from 'components/ActivityParametersControl'
import math from 'util/math'
import i18n from 'util/i18n'

class BodyMassControl extends React.Component {
  static displayName = 'BodyMassControl'

  static propTypes = {
    label: PropTypes.string,
    onChange: PropTypes.func,
    unit: PropTypes.string,
    validator: PropTypes.func,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
  }

  static defaultProps = {
    label: i18n.t('model.Weight.title'),
    onChange: () => { },
    value: '',
    unit: 'kg',
    validator: function (value) {
      const v = value.toNumber('kg')
      return v >= 30 && v <= 250
    },
  }

  constructor(props) {
    super(props)

    this.state = {
      value: math.unit(props.value, props.unit),
    }
  }

  handleChange = parameter => {
    this.setState({
      value: math.unit(parameter.value, parameter.unit),
    }, () => {
      this.props.onChange(this.state.value.toNumber('kg'))
    })
  }

  validate() {
    return this.controlRef_.validate()
  }

  render() {
    let {
      label,
      validator,
    } = this.props
    const {value} = this.state

    return (
      <ActivityParameterControl
        allowedUnits={['kg', 'lbs']}
        innerRef={ref => this.controlRef_ = ref}
        label={label}
        onChange={this.handleChange}
        precision={2}
        unit={value.formatUnits()}
        validator={validator}
        value={value.toNumber() === 0 ? '' : value.toNumber()}
      />
    )
  }
}

export default BodyMassControl