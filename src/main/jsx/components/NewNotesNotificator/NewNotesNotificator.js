import React from 'react'
import PropTypes from 'prop-types'
import Badge from '@material-ui/core/Badge'
import NotesActions from 'actions/NotesActions'
import BoardActions from 'actions/BoardActions'

class NewNotesNotificator extends React.Component {
  static displayName = 'NewNotesNotificator'

  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
  }

  state = {
    notes: 0,
  }

  componentWillMount() {
    BoardActions.getClientBoard()
      .then(NotesActions.countUnread)
      .then(notes => {
        if (this.isMounted_) {
          this.setState({notes})
        }
      })
  }

  componentDidMount() {
    this.isMounted_ = true
  }

  componentWillUnmount() {
    this.isMounted_ = false
  }

  render() {
    const {className, children} = this.props
    const {notes} = this.state
    return notes ? (
      <Badge
        badgeContent={notes}
        className={className}
        color="error"
      >
        {children}
      </Badge>
    ) : (
      <span className={className}>{children}</span>
    )
  }
}

export default NewNotesNotificator
