import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'
import i18n from 'util/i18n'

const styles = theme => ({
})

class ExpandButton extends React.Component {
  static displayName = 'ExpandButton'

  static propTypes = {
    icon: PropTypes.string,
    label: PropTypes.string,
    onClick: PropTypes.func,
  }

  static defaultProps = {
    icon: 'expand_more',
    label: i18n.t('ui.controls.ExpandButton.label'),
    onClick: () => { },
  }

  handleClick = () => {
    this.props.onClick()
  }

  render() {
    const {
      classes,
      icon,
      label,
    } = this.props
    return (
      <Button
        className={classes.button}
        color="secondary"
        onClick={this.handleClick}
        size="small"
      >
        {this.props.label}
        <Icon className={classes.icon}>
          {icon}
        </Icon>
      </Button>
    )
  }
}

export default withStyles(styles, {withTheme: true})(ExpandButton)
