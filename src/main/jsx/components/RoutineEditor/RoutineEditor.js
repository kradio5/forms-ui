import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'

import Divider from '@material-ui/core/Divider'
import Typography from '@material-ui/core/Typography'
import CalculatorManager from 'activityCalculators'
import ActivityMedia from 'components/ActivityMedia/ActivityMedia'
import ActivityParametersControl from 'components/ActivityParametersControl'
import IconButton from '@material-ui/core/IconButton'
import Icon from '@material-ui/core/Icon'
import Chip from '@material-ui/core/Chip'
import Collapse from '@material-ui/core/Collapse'
import url from 'url'
import classNames from 'classnames'
import i18n from 'util/i18n'

const styles = theme => ({
  rootContainer: {
    position: 'relative',
    minHeight: '100vh',
  },
  root: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    display: 'flex',
    flexDirection: theme.direction === 'rtl' ? 'row-reverse' : 'row',
    zIndex: 1,
    overflow: 'hidden',
  },
  content: {
    position: 'relative',
    width: '100%',
    height: 'calc(100% - 56px)',
    padding: theme.spacing.unit,
    marginTop: 56,
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 64px)',
      padding: theme.spacing.unit * 2,
      marginTop: 64,
    },
    overflowX: 'hidden',
    overflowY: 'scroll',
  },
  card: {
    marginBottom: theme.spacing.unit,
    '&:last-child': {
      marginBottom: 0,
    },
  },
  cardHeader: {
    paddingBottom: 0,
    // paddingTop: 0,
  },
  tab: {
    textTransform: 'initial',
  },
  actions: {
    display: 'flex',
    flexDirection: theme.direction === 'rtl' ? 'row' : 'row-reverse',
    paddingBottom: 0,
    paddingTop: 0,
  },
  chips: {
    display: 'flex',
    flexFlow: 'row wrap',
    padding: [[theme.spacing.unit * 1.5, theme.spacing.unit * 2]],
    [theme.breakpoints.up('sm')]: {
      padding: [[theme.spacing.unit * 1.5, theme.spacing.unit * 3]],
    },
  },
  chip: {
    margin: [[
      0,
      theme.spacing.unit / 2,
      theme.spacing.unit / 2,
      0,
    ]],
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  preformatted: {
    whiteSpace: 'pre-wrap',
  },
  tooltipPlacementLeft: {
    transformOrigin: 'right center',
    margin: `0 ${theme.spacing.unit / 2}px`,
    [theme.breakpoints.up('sm')]: {
      margin: `0 ${theme.spacing.unit / 2}px`,
    },
  },
})

class RoutineEditor extends React.Component {
  static displayName = 'RoutineEditor'

  static propTypes = {
    disabled: PropTypes.bool,
    exercise: PropTypes.shape({
      props: PropTypes.object,
    }),
    onChange: PropTypes.func,
    onClose: PropTypes.func,
    onCloseSearch: PropTypes.func,
    onEdit: PropTypes.func,
    onRemove: PropTypes.func,
    onSubmit: PropTypes.func,
    open: PropTypes.bool,
    routine: PropTypes.shape({
      props: PropTypes.object,
    }),
    searchOpen: PropTypes.bool,
  }

  static defaultProps = {
    open: false,
  }

  constructor(props) {
    super(props)

    const routine = props.routine// || WorkoutActions.createRoutineResource()

    this.state = {
      calculator: routine && this._createCalculator(routine),
      expanded: false,
      routine,
    }
  }

  componentWillReceiveProps({routine}) {
    if (routine && routine !== this.state.routine) {
      this.setState({
        routine,
        calculator: this._createCalculator(routine),
      })
    }
  }

  handleCancel = () => {
    this.props.onClose()
  }

  handleDone = () => {
    const isValid = this.validate()

    if (!isValid) {
      return
    }

    this.props.onSubmit(this.props.routine)
  }

  handleExpandClick = () => {
    this.setState(({expanded}) => ({expanded: !expanded}))
  }

  handleParametersChange = parameters => {
    this.setState(({calculator, routine}) => ({
      routine: routine
        .prop('parameters', parameters)
        .prop('duration', calculator.calculateDuration().toNumber('s'))
        .prop('calories', calculator.calculateEnergy().toNumber()),
    }), () => {
      this.props.onChange(this.state.routine)
    })
  }

  handleSelectExercise = exercise => {
    const {routine} = this.state
    routine
      .prop('motion', exercise)
      .prop('duration', exercise.props.duration)
      .prop('met', exercise.props.met)
      .prop('calories', exercise.props.calories)
    this.props.onChange(routine)
    this.props.onCloseSearch()
  }

  handleSelectCategory = category => {
    const {exerciseUi} = window.ydw.config._links
    window.location.href = url.format({
      pathname: `${exerciseUi.href}/exercises/by-category/${category.props.slug}`,
    })
  }

  handleSelectTag = tag => {
    const {exerciseUi} = window.ydw.config._links
    window.location.href = url.format({
      pathname: `${exerciseUi.href}/exercises`,
      query: {text: tag},
    })
  }

  handleSearchClose = () => {
    this.props.onCloseSearch()
  }

  validate() {
    const {calculator} = this.state

    return calculator.getRequiredParameters().length === 0 || this.refs.parameters.validate()
  }

  _createCalculator(routine) {
    const defaultCalculator = 'Exercise#Standard'
    const {motion: exercise} = routine.props
    const {calculator: calculator = defaultCalculator} = exercise ? exercise.props.extendedProperties || {} : {}
    const Calculator = CalculatorManager.has(calculator) ?
      CalculatorManager.get(calculator) : CalculatorManager.get(defaultCalculator)

    return new Calculator(routine)
  }

  _getRootParameters() {
    const {calculator, routine} = this.state
    return [{
      name: 'duration',
      value: calculator.getDuration().toNumber('h'),
      unit: 'h',
    }, {
      name: 'met',
      value: calculator.getMet().toNumber(),
      unit: 'met',
    }, {
      name: 'energy',
      value: calculator.getEnergy().toNumber(),
      unit: 'kcal',
    }]
  }

  _getMixedParameters() {
    const {routine} = this.state
    return (routine.props.parameters || []).concat(
      this._getRootParameters()
    )
  }

  _getIntensityName(met) {
    const MET_NAMES = ['VERY_LIGHT', 'LIGHT', 'LIGHT_MODERATE', 'MODERATE', 'MODERATE_HARD', 'HARD', 'VERY_HARD']
    const MET_VALUES = [1.5, 2.5, 3.5, 4.5, 6, 7.5, 10]
    const metValue = MET_VALUES
      .reduce((v, c) => {
        if (Math.abs(v - met) > Math.abs(c - met)) {
          return c
        }
        return v
      }, MET_VALUES[0])

    return MET_NAMES[MET_VALUES.indexOf(metValue)]
  }

  _renderCategory(category, index) {
    const {classes} = this.props
    return (
      <Chip
        className={classes.chip}
        key={`category-${index}`}
        label={category.props.title}
        onClick={() => this.handleSelectCategory(category)}
      />
    )
  }

  _renderTag(tag, index) {
    const {classes} = this.props
    return (
      <Chip
        className={classes.chip}
        key={`tag-${index}`}
        label={tag}
        onClick={() => this.handleSelectTag(tag)}
      />
    )
  }

  _renderRoutineExercise() {
    const {classes, disabled} = this.props
    const {calculator, expanded, routine} = this.state
    const {motion: exercise} = routine.props
    const {categories = [], description, tags = [], title, image, video} = exercise.props
    const action = (
      <IconButton
        aria-expanded={expanded}
        aria-label="Show more"
        className={classNames(classes.expand, {[classes.expandOpen]: expanded})}
        onClick={this.handleExpandClick}
      >
        <Icon>{'expand_more'}</Icon>
      </IconButton>
    )
    const intensity = this._getIntensityName(routine.props.met)
    const intensityTitle = i18n.t(`model.Intensity.${intensity}.shortTitle`) + ' ' +
      i18n.t('model.Intensity.title').toLowerCase()
    return (
      <Card className={classes.card}>
        <ActivityMedia
          icon={calculator.getIcon()}
          image={image && image.uri}
          video={video && video.uri}
        />
        <CardHeader
          action={description ? action : null}
          className={classes.cardHeader}
          subheader={`${Math.ceil(routine.props.calories)} ${i18n.t('uom.Unit.KCAL')} - ${intensityTitle}`}
          title={title}
        />
        <Collapse in={[categories, tags].some(({length}) => length > 0)}
          timeout="auto"
          unmountOnExit
        >
          <CardActions className={classes.chips}>
            {categories.map(this._renderCategory, this)}
            {tags.map(this._renderTag, this)}
          </CardActions>
        </Collapse>
        <Collapse in={expanded}
          timeout="auto"
          unmountOnExit
        >
          <Divider light />
          <CardContent>
            <Typography className={classes.preformatted}>
              {description}
            </Typography>
          </CardContent>
        </Collapse>
      </Card>
    )
  }

  _renderRoutineParameters() {
    const {classes, disabled} = this.props
    const {calculator, routine} = this.state
    return (
      <Card className={classes.card}>
        <CardContent>
          <ActivityParametersControl
            allowedParameters={calculator.getAllowedParameters()}
            disallowedParameters={disabled ? [] : ['met']}
            onChange={this.handleParametersChange}
            open
            parameters={disabled ? this._getMixedParameters() : routine.props.parameters || []}
            readOnly={disabled}
            ref="parameters"
            requiredParameters={['reps'].concat(calculator.getRequiredParameters())}
            rootParameters={this._getRootParameters()}
          />
        </CardContent>
      </Card>
    )
  }

  render() {
    const {classes, disabled, routine, searchOpen} = this.props
    const {motion: exercise} = routine && routine.props || {}
    return (
      <main className={classes.content}>
        {routine && exercise && disabled && this._renderRoutineExercise()}
        {routine && exercise && this._renderRoutineParameters()}
      </main>
    )
  }
}

export default withStyles(styles, {withTheme: true})(RoutineEditor)
