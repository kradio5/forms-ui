import React from 'react'
import Preloader from 'components/Preloader'
import Loadable from 'react-loadable'

export default loader => Loadable({
  loader,
  loading: Preloader,
})