import React from 'react'
import PropTypes from 'prop-types'
import {Container} from 'flux/utils'
import UserStore from 'stores/UserStore'
import ClientActions from 'actions/ClientActions'
import ClientStore from 'stores/ClientStore'
import ClientProfileActions from 'actions/ClientProfileActions'
import ClientProfileStore from 'stores/ClientProfileStore'
import Preloader from 'components/Preloader'

export default ComposedComponent => Container.create(
  class ClientContainer extends React.Component {
    static displayName = 'ClientContainer'

    static propTypes = {
      children: PropTypes.node,
    }

    static calculateState(prevState) {
      return {
        ...prevState,
        client: ClientStore.getClient(),
        clientProfile: ClientProfileStore.getClientProfile(),
        isActivated: ClientProfileStore.isActivated(),
      }
    }

    static getStores() {
      return [ClientStore, ClientProfileStore]
    }

    componentWillMount() {
      const uid = UserStore.getUserId()
      if (uid && !this.state.client) {
        this.setState({isLoading: true})
        Promise.all([
          ClientActions.findByUid(uid),
          ClientProfileActions.getByUserId(uid),
        ])
          .finally(() => {
            this.setState({isLoading: false})
          })
      }
    }

    render() {
      if (this.state.isLoading) {
        return (<Preloader />)
      }

      const {children, ...restProps} = this.props
      return (
        <ComposedComponent
          {...restProps}
        >
          {children}
        </ComposedComponent>
      )
    }
  }
)