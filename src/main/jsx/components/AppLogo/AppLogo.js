import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import classNames from 'classnames'

const styles = theme => ({
  root: {
    height: '100%',
    margin: [[0, 'auto']],
    maxWidth: 800,
  },
})

class AppLogo extends React.Component {
  static displayName = 'AppLogo'

  static propTypes = {
    className: PropTypes.string,
  }

  render() {
    const {classes, className} = this.props
    return (
      <div className={classNames(classes.root, 'app-logo__image', className)} />
    )
  }
}

export default withStyles(styles, {withTheme: true})(AppLogo)
