import React from 'react'
import PropTypes from 'prop-types'
import {Container} from 'flux/utils'
import UserActions from 'actions/UserActions'
import UserStore from 'stores/UserStore'
import AuthStore from 'stores/AuthStore'

export default ComposedComponent => Container.create(
  class UserContainer extends React.Component {
    static getStores() {
      return [/*AuthStore, */UserStore]
    }

    static calculateState(prevState) {
      return {
        ...prevState,
        user: UserStore.getCurrentUser(),
      }
    }

    static displayName = 'UserContainer'

    static propTypes = {
      children: PropTypes.node,
    }

    componentWillMount() {
      if (AuthStore.isAuthorized() && !this.state.user) {
        this.setState({isLoading: true})
        UserActions.getCurrentUser()
          .finally(() => {
            this.setState({isLoading: false})
          })
      }
    }

    render() {
      if (this.state.isLoading) {
        return null
      }

      const {children, ...restProps} = this.props
      return (
        <ComposedComponent
          {...restProps}
        >
          {children}
        </ComposedComponent>
      )
    }
  }
)
