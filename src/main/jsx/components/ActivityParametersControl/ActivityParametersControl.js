import React from 'react'
import PropTypes from 'prop-types'
import List from '@material-ui/core/List'
import ActivityParameterControl from './ActivityParameterControl'
import ActivityParameterDetail from './ActivityParameterDetail'
import DurationControl from 'components/DurationControl'
import MetSlider from 'components/MetSlider'
import i18n from 'util/i18n'

const PARAMETERS_DATA = {
  duration: {
    icon: 'timer',
    label: i18n.t('model.Duration.title'),
    type: 'Time',
    units: ['h', 'min', 's'],
    unit: 'h',
    component: DurationControl,
  },
  distance: {
    icon: 'swap_horiz',
    label: i18n.t('model.Distance.title'),
    type: 'Length',
    units: ['m', 'km', 'ft', 'yd', 'mi'],
    unit: 'm',
    precision: 2,
  },
  reps: {
    icon: 'repeat',
    label: i18n.t('model.Repetition.title'),
    type: 'Repetition',
    units: ['rep'],
    unit: 'rep',
    precision: 0,
    validator: function (value, rawValue) {
      return !rawValue || value.toNumber() >= 1
    },
  },
  steps: {
    icon: 'directions_walk',
    label: i18n.t('model.Steps.title'),
    type: 'Step',
    units: ['step'],
    unit: 'step',
    precision: 0,
  },
  energy: {
    icon: 'whatshot',
    label: i18n.t('model.Energy.title'),
    type: 'Energy',
    units: ['kcal', 'kJ'],
    unit: 'kcal',
    precision: 2,
  },
  met: {
    icon: 'battery_charging_full',
    label: i18n.t('model.Intensity.description'),
    type: 'Effort',
    units: ['met'],
    unit: 'met',
    component: MetSlider,
  },
  grade: {
    icon: 'landscape',
    label: i18n.t('model.Grade.description'),
    type: 'Angles',
    units: ['percent'],
    unit: 'percent',
    precision: 2,
    validator: function (value) {
      return value.toNumber() <= 100 && value.toNumber() >= -100
    },
  },
  speed: {
    icon: 'av_timer',
    label: i18n.t('model.Speed.description'),
    type: 'Speed',
    units: ['m/min', 'm/s', 'km/h', 'mi/h'],
    unit: 'm/min',
    precision: 2,
    validator: function (value) {
      return value.toNumber() > 0
    },
  },
  /*
  power: {
    icon: 'flash_on',
    label: i18n.t('model.Power.description'),
    type: 'Power',
    units: ['kgmmin', 'watt'],
    unit: 'kgmmin',
    precision: 2,
  },
  */
}

const controls = {}

class ActivityParametersControl extends React.Component {
  static displayName = 'ActivityParametersControl'

  static propTypes = {
    allowedParameters: PropTypes.arrayOf(PropTypes.string),
    calculator: PropTypes.string,
    disallowedParameters: PropTypes.arrayOf(PropTypes.string),
    onChange: PropTypes.func,
    open: PropTypes.bool,
    parameters: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      unit: PropTypes.string,
      value: PropTypes.any,
    })),
    readOnly: PropTypes.bool,
    requiredParameters: PropTypes.arrayOf(PropTypes.string),
    rootParameters: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      unit: PropTypes.string,
      value: PropTypes.any,
    })),
  }

  static defaultProps = {
    allowedParameters: Object.keys(PARAMETERS_DATA),
    disallowedParameters: [],
    readOnly: false,
    requiredParameters: [],
    onChange: () => { },
    open: false,
    parameters: [],
    rootParameters: [],
  }

  constructor(props) {
    super(props)

    this.state = {
      parameters: this._createParameters(props),
      value: '',
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      parameters: this._createParameters(nextProps),
    })
  }

  validate() {
    const required = new Set(this.props.requiredParameters)
    return [...this.state.parameters]
      .every(name => required.has(name) ?
        controls[name].validate() : true, this)
  }

  _createParameters(props) {
    const {disallowedParameters, requiredParameters, parameters} = props
    const disallowed = new Set(disallowedParameters)
    return new Set(
      requiredParameters
        .concat(parameters.map(param => param.name))
        .filter(param => !disallowed.has(param))
    )
  }

  handleParameterChange = parameter => {
    const {parameters} = this.props
    const parameterIndex = parameters.findIndex(p => p.name === parameter.name)
    if (parameterIndex > -1) {
      if (parameter.name === 'energy') {
        if (parameter.value) {
          Object.assign(parameters[parameterIndex], parameter)
        } else {
          parameters.splice(parameterIndex, 1)
        }
      } else {
        Object.assign(parameters[parameterIndex], parameter)
      }
    } else {
      if (parameter.name === 'energy') {
        if (parameter.value) {
          parameters.push(parameter)
        }
      } else {
        parameters.push(parameter)
      }
    }
    /*
    if (parameterIndex > -1) {
      if (parameter.value) {
        Object.assign(parameters[parameterIndex], parameter)
      } else {
        parameters.splice(parameterIndex, 1)
      }
    } else if (parameter.value) {
      parameters.push(parameter)
    }
    */
    this.props.onChange(parameters)
  }

  handleRef(ref) {
    controls[ref.props.name] = ref
  }

  _renderParameter(name) {
    const {parameters, rootParameters} = this.props
    const {
      component = ActivityParameterControl,
      units: allowedUnits,
      unit: defaultUnit,
      ...data
    } = PARAMETERS_DATA[name]
    const {
      value = '',
      unit = defaultUnit,
    } = parameters.find(it => it.name === name) ||
    rootParameters.find(it => it.name === name) || {}
    return React.createElement(component, {
      allowedUnits,
      key: name,
      name,
      onChange: this.handleParameterChange,
      onRef: this.handleRef,
      value,
      unit,
      ...data,
    })
  }

  _renderParameterDetail(name) {
    const {parameters} = this.props
    const {
      icon,
      label,
      unit: defaultUnit,
      //...data
    } = PARAMETERS_DATA[name]
    const {
      value = 0,
      unit = defaultUnit,
    } = parameters.find(it => it.name === name) || {}
    return (
      <ActivityParameterDetail
        icon={icon}
        key={name}
        label={label}
        unit={unit}
        value={value}
      />
    )
  }

  render() {
    const {allowedParameters, disallowedParameters, open, readOnly, requiredParameters} = this.props
    const {parameters} = this.state
    if (readOnly) {
      return (
        <List disablePadding>
          {[...parameters].map(this._renderParameterDetail, this)}
        </List>
      )
    }
    const visibleParameters = new Set(
      requiredParameters.filter(param => parameters.has(param))
    )
    const optionalParameters = new Set(
      [...parameters].filter(param => !visibleParameters.has(param))
        .concat(allowedParameters)
        .filter(param => !disallowedParameters.includes(param))
    )
    return (
      <div>
        {[...visibleParameters].map(this._renderParameter, this)}
        {optionalParameters.size && open && [...optionalParameters].map(this._renderParameter, this)}
      </div>
    )
  }
}

export default ActivityParametersControl
