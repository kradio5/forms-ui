import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import classNames from 'classnames'
import math from 'util/math'
import i18n from 'util/i18n'

/*
const RE_FIXED_3 = /-?(?:0(?!0|[1-9])|[1-9])\d*(?:\.\d{0,3})?/
const RE_FIXED_2 = /-?(?:0(?!0|[1-9])|[1-9])\d*(?:\.\d{0,2})?/
const RE_FIXED_1 = /-?(?:0(?!0|[1-9])|[1-9])\d*(?:\.\d{0,1})?/
const RE_INT = /-?(?:0(?!0|[1-9])|[1-9])\d{0,8}/
*/

const styles = theme => ({
  root: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'baseline',
  },
  /*
  valueField: {
    marginRight: theme.spacing.unit / 2,
  },
  unitSelect: {
    marginLeft: theme.spacing.unit / 2,
  },
  */
})

class ActivityParameterControl extends React.Component {
  static displayName = 'ActivityParameterControl'

  static propTypes = {
    allowedUnits: PropTypes.arrayOf(PropTypes.string),
    disabled: PropTypes.bool,
    errorText: PropTypes.string,
    helperText: PropTypes.string,
    id: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    label: PropTypes.string,
    name: PropTypes.string,
    onChange: PropTypes.func,
    onRef: PropTypes.func,
    precision: PropTypes.number,
    required: PropTypes.bool,
    unit: PropTypes.string,
    unitTitle: PropTypes.string,
    validator: PropTypes.func,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).isRequired,
  }

  static defaultProps = {
    allowedUnits: [],
    errorText: i18n.t('ui.controls.ActivityParametersControl.errorText'),
    disabled: false,
    label: '',
    onChange: () => { },
    onRef: () => { },
    onUnitChange: () => { },
    precision: 2,
    required: false,
    unitTitle: '',
    init: null,
    value: '',
    validator: function (value, rawValue) {
      return value.toNumber() >= 0 && rawValue.length > 0
    },
  }

  constructor(props) {
    super(props)

    const state = this._createState(props)
    this.state = {
      ...state,
      isDirty: false,
      rawValue: Number(state.rawValue) ? state.rawValue : '',
    }
  }

  componentDidMount() {
    this.props.onRef(this)
  }

  componentWillReceiveProps(nextProps) {
    const {isDirty, value} = this.state
    if (!isDirty && value.toNumber() !== Number(nextProps.value)) {
      this.setState(this._createState(nextProps))
    }
  }

  _createState(props) {
    const value = this._createValue(props.value, props.unit)
    return {
      invalid: false,
      value: value,
      unit: props.unit,
      rawValue: props.value && this._normalizeOut(value).toString(),
    }
  }

  _createValue(value, unit) {
    try {
      return math.unit(value, unit)
    } catch (e) {}
    return math.bignumber(value || math.number(value))
  }

  isValid() {
    return this.props.validator(this.state.value, this.state.rawValue)
  }

  validate() {
    const isValid = this.isValid()
    this.setState({invalid: !isValid})
    return isValid
  }

  _normalizeIn(val) {
    let s = (val || '').toString()
      .replace(/,+/g, '.')
      .replace(/-+/g, '-')

    if (s === '-') {
      return s
    }

    const {precision} = this.props
    const RE_FIXED = new RegExp(`-?(?:0(?!0|[1-9])|[1-9])\\d*(?:\\.\\d{0,${precision}})?`)
    const RE_INT = new RegExp('-?(?:0(?!0|[1-9])|[1-9])\\d{0,8}')

    return (s.match(precision > 0 ? RE_FIXED : RE_INT) || [''])[0]
  }

  _normalizeOut(val) {
    return Number(math.format(val.toNumber(), {
      notation: 'fixed',
      precision: this.props.precision,
    }))
  }

  handleChange = e => {
    const rawValue = this._normalizeIn(e.target.value)
    const val = !rawValue || rawValue === '-' ? '0' : rawValue

    if (Number.isNaN(val)) {
      return
    }

    const value = this._createValue(val, this.state.unit)
    this.setState({
      isDirty: true,
      invalid: false,
      value,
      rawValue: rawValue,
    }, () => {
      this.props.onChange({
        name: this.props.name,
        value: this._normalizeOut(this.state.value),
        unit: this.state.unit,
      })
    })
  }

  handleUnitChange = e => {
    const {value: val} = e.target
    const {rawValue} = this.state
    const newValue = this.state.value.to(val)
    const normalized = this._normalizeOut(newValue)
    this.setState({
      unit: val,
      value: newValue,
      rawValue: rawValue ? normalized.toString() : rawValue,
    }, () => {
      this.props.onChange({
        name: this.props.name,
        value: normalized,
        unit: this.state.unit,
      })
    })
  }

  _renderUnit(unit, index) {
    const unitKey = (unit || '')
      .replace('/', '_')
      .toUpperCase()
    return (
      <MenuItem
        key={index}
        value={unit}
      >
        {i18n.t(`uom.Unit.${unitKey}.title`)}
      </MenuItem>
    )
  }

  render() {
    const {allowedUnits, classes, disabled, errorText, helperText, label, name, required} = this.props
    const {invalid, rawValue, unit} = this.state
    const unitKey = (unit || '').replace('/', '_')
    const unitTitle = i18n.t(`uom.Unit.${unitKey.toUpperCase()}`, {_: ''}) || this.props.unitTitle
    const showUnits = !disabled && allowedUnits.length > 1
    return (
      <div className={classes.root}>
        <TextField
          InputLabelProps={{shrink: true}}
          className={classNames({[classes.valueField]: showUnits})}
          disabled={disabled}
          error={invalid}
          fullWidth
          helperText={invalid ? (rawValue ? errorText : i18n.t('ui.controls.TextFieldRequired.errorText')) : helperText}
          id={name}
          label={showUnits || !unitTitle ? label : `${label}, ${unitTitle}`}
          margin="normal"
          name={name}
          onChange={this.handleChange}
          required={required}
          value={rawValue}
        />
        {showUnits ? (
          <TextField
            className={classes.unitSelect}
            fullWidth
            margin="normal"
            name={name + '-units'}
            onChange={this.handleUnitChange}
            select
            value={this.state.unit}
          >
            {allowedUnits.map(this._renderUnit, this)}
          </TextField>
        ) : null}
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(ActivityParameterControl)
