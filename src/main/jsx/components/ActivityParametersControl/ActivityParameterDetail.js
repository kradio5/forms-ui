import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'

import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'

import Icon from '@material-ui/core/Icon'
import math from 'util/math'
import i18n from 'util/i18n'

const styles = theme => ({
})

class ActivityParameterDetail extends React.Component {
  static displayName = 'ActivityParameterDetail'

  static propTypes = {
    icon: PropTypes.string,
    label: PropTypes.string,
    precision: PropTypes.number,
    unit: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
  }

  static defaultProps = {
    icon: 'home',
    label: ' ',
    precision: 2,
    value: 0,
  }

  render() {
    let {
      icon,
      label,
      value,
      unit,
      precision,
    } = this.props
    //value = math.unit(value, unit)

    switch (unit) {
      case 'h':
        value = math.unit(value, unit).toNumber('min')
        unit = 'min'
        break
      case 'rep':
        if (value <= 1) {
          return null
        }
    }
    const unitName = i18n.t(`uom.Unit.${unit.replace('/', '_').toUpperCase()}`)
    return (
      <ListItem>
        <ListItemIcon>
          <Icon>{icon}</Icon>
        </ListItemIcon>
        <ListItemText
          primary={label}
          secondary={`${Number.isFinite(value) ? Number(value.toFixed(precision)) : value} ${unitName}`}
        />
      </ListItem>
    )
  }
}

export default withStyles(styles, {withTheme: true})(ActivityParameterDetail)
