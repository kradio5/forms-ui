const defaultFont = {
  fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  fontWeight: '300',
  lineHeight: '1.5em',
}
const title = {
  color: '#3C4858',
  margin: '1.75rem 0 0.875rem',
  textDecoration: 'none',
  fontWeight: '700',
  fontFamily: '"Roboto Slab", "Times New Roman", serif',
}
const containerFluid = {
  paddingRight: '15px',
  paddingLeft: '15px',
  marginRight: 'auto',
  marginLeft: 'auto',
  width: '100%',
}
const container = {
  ...containerFluid,
  '@media (min-width: 576px)': {
    maxWidth: '540px',
  },
  '@media (min-width: 768px)': {
    maxWidth: '720px',
  },
  '@media (min-width: 992px)': {
    maxWidth: '960px',
  },
  '@media (min-width: 1200px)': {
    maxWidth: '1140px',
  },
}
const palette = {
  primary: '#9c27b0',
  warning: '#ff9800',
  danger: '#f44336',
  success: '#4caf50',
  info: '#00acc1',
  rose: '#e91e63',
  default: '#999999',
  inverse: '#333333',
  gray: '#6c757d',
}
const boxShadow = {
  primary: {
    boxShadow: '0 12px 20px -10px rgba(156, 39, 176, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(156, 39, 176, 0.2)',
  },
  info: {
    boxShadow: '0 12px 20px -10px rgba(0, 188, 212, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(0, 188, 212, 0.2)',
  },
  success: {
    boxShadow: '0 12px 20px -10px rgba(76, 175, 80, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(76, 175, 80, 0.2)',
  },
  warning: {
    boxShadow: '0 12px 20px -10px rgba(255, 152, 0, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(255, 152, 0, 0.2)',
  },
  danger: {
    boxShadow: '0 12px 20px -10px rgba(244, 67, 54, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(244, 67, 54, 0.2)',
  },
  rose: {
    boxShadow: '0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(233, 30, 99, 0.4)',
  },
  default: {
    boxShadow: '0 10px 30px -12px rgba(0, 0, 0, 0.42), 0 4px 25px 0px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)',
  },
}
const mixins = {
  boxShadow,
  card: {
    display: 'inline-block',
    position: 'relative',
    width: '100%',
    margin: '25px 0',
    boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.14)',
    borderRadius: '3px',
    color: 'rgba(0, 0, 0, 0.87)',
    background: '#fff',
  },
  cardActions: {
    margin: '0 20px 10px',
    paddingTop: '10px',
    borderTop: '1px solid #eeeeee',
    height: 'auto',
    ...defaultFont,
  },
  cardHeader: {
    margin: '-30px 15px 0',
    borderRadius: '3px',
    padding: '15px',
  },
  cardHeaderOrange: {
    color: '#fff',
    background: 'linear-gradient(60deg, #ffa726, #fb8c00)',
    ...boxShadow.warning,
  },
  cardHeaderGreen: {
    color: '#fff',
    background: 'linear-gradient(60deg, #66bb6a, #43a047)',
    ...boxShadow.success,
  },
  cardHeaderRed: {
    color: '#fff',
    background: 'linear-gradient(60deg, #ef5350, #e53935)',
    ...boxShadow.danger,
  },
  cardHeaderBlue: {
    color: '#fff',
    background: 'linear-gradient(60deg, #26c6da, #00acc1)',
    ...boxShadow.info,
  },
  cardHeaderPurple: {
    color: '#fff',
    background: 'linear-gradient(60deg, #ab47bc, #8e24aa)',
    ...boxShadow.primary,
  },
  cardHeaderRose: {
    color: '#fff',
    background: 'linear-gradient(60deg, #ec407a, #d81b60)',
    ...boxShadow.rose,
  },
  cardSubtitle: {
    marginBottom: '0',
    marginTop: '-.375rem',
  },
  cardTitle: {
    ...title,
    marginTop: '.625rem',
  },
  container,
  containerFluid,
  font: defaultFont,
  title,
  transition: {
    transition: 'all 0.33s cubic-bezier(0.685, 0.0473, 0.346, 1)',
  },
}

const MaterialKitTheme = {
  palette,
  mixins,
}

export default MaterialKitTheme