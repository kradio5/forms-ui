import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import spacing from '@material-ui/core/styles/spacing'
import createBreakpoints from '@material-ui/core/styles/createBreakpoints'
import MaterialKitTheme from './MaterialKitTheme'

const breakpoints = createBreakpoints({})
const direction = 'ltr'
const {palette = {
  primary: {main: '#37474f'}, // blueGrey[800]
  secondary: {main: '#cddc39'}, // lime[500]
}} = window.ydw.config.theme || {}
const theme = createMuiTheme({
  palette: {
    ...palette,
    materialKit: MaterialKitTheme.palette,
  },
  mixins: {
    materialKit: MaterialKitTheme.mixins,
    toolbarMenuButton: {
      marginLeft: spacing.unit * (direction === 'rtl' ? 2 : -1.5),
      marginRight: spacing.unit * (direction === 'rtl' ? -1.5 : 2),
      [breakpoints.up('sm')]: {
        marginLeft: spacing.unit * (direction === 'rtl' ? 2.5 : -2),
        marginRight: spacing.unit * (direction === 'rtl' ? -2 : 2.5),
      },
      /*
      marginLeft: spacing.unit * (direction === 'rtl' ? 2 : 0),
      marginRight: spacing.unit * (direction === 'rtl' ? 0 : 2),
      [breakpoints.up('sm')]: {
        marginLeft: spacing.unit * (direction === 'rtl' ? 2 : -1),
        marginRight: spacing.unit * (direction === 'rtl' ? -1 : 2),
      },
      */
      /*
      marginLeft: spacing.unit * (direction === 'rtl' ? 2 : -1.5),
      marginRight: spacing.unit * (direction === 'rtl' ? -1.5 : 2),
      [breakpoints.up('sm')]: {
        marginLeft: spacing.unit * (direction === 'rtl' ? 3 : -1.5),
        marginRight: spacing.unit * (direction === 'rtl' ? -1.5 : 3),
      },
      */
    },
  },
  typography: {
    useNextVariants: true,
  },
})

export default theme