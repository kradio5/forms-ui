import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import {withRouter} from 'react-router'
import Authenticator from 'components/Authenticator'
import withClient from 'components/withClient'
import withUser from 'components/withUser'
import Localizator from 'components/Localizator'
import AppLayout from 'components/AppLayout'
import CssBaseline from '@material-ui/core/CssBaseline'
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider'
import MomentUtils from 'material-ui-pickers/utils/moment-utils'
import {MuiPickersUtilsProvider} from 'material-ui-pickers'
import theme from './MuiTheme'
import moment from 'util/moment'
import {hot} from 'react-hot-loader'

const styles = {
  root: {
    height: '100vh',
  },
}

@Localizator
@withRouter
@Authenticator
@withUser
@withClient
@withStyles(styles)
@hot(module)
class App extends React.Component {
  static displayName = 'App'

  static propTypes = {
    children: PropTypes.node,
  }

  render() {
    /*
    if (module.hot) {
      module.hot.accept()
    }
    */
    const {classes} = this.props
    return (
      <div className={classes.root}>
        <CssBaseline />
        <MuiThemeProvider theme={theme}>
          <MuiPickersUtilsProvider
            moment={moment}
            utils={MomentUtils}
          >
            <AppLayout>
              {this.props.children}
            </AppLayout>
          </MuiPickersUtilsProvider>
        </MuiThemeProvider>
      </div>
    )
  }
}

export default App