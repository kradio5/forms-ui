import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import {fade} from '@material-ui/core/styles/colorManipulator'
import RCSlider from 'rc-slider'
import 'rc-slider/assets/index.css'
import classNames from 'classnames'

const styles = theme => ({
  root: {
    fontFamily: 'Roboto, sans-serif',
    margin: [[41, 4, 0]],
  },
  handle: {
    borderColor: [theme.palette.secondary.main, '!important'],
    '&:active': {
      boxShadow: [[0, 0, 5, theme.palette.secondary.main], '!important'],
    },
    '&:focus': {
      boxShadow: [[0, 0, 0, 5, theme.palette.secondary.main], '!important'],
    },
  },
})

class Slider extends React.Component {
  static displayName = 'Slider'

  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    dots: PropTypes.bool,
    marks: PropTypes.shape({}),
    max: PropTypes.number,
    min: PropTypes.number,
    onChange: PropTypes.func,
    step: PropTypes.number,
    value: PropTypes.number,
  }

  static defaultProps = {
    dots: true,
    onChange: () => { },
  }

  render() {
    const {className, classes, theme, ...restProps} = this.props
    const Handle = RCSlider.Handle
    const handle = props => {
      const {className, dragging, index, ...restProps} = props
      return (
        <Handle
          {...restProps}
          className={classNames(className, classes.handle)}
        />
      )
    }
    return (
      <RCSlider
        {...restProps}
        activeDotStyle={{borderColor: theme.palette.secondary.main}}
        className={classNames(className, classes.root)}
        handle={handle}
        trackStyle={{backgroundColor: theme.palette.secondary.main}}
      />
    )
  }
}

export default withStyles(styles, {withTheme: true})(Slider)
