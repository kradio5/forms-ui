import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import {fade} from '@material-ui/core/styles/colorManipulator'
import CardMedia from '@material-ui/core/CardMedia'
import Icon from '@material-ui/core/Icon'
import ReactPlayer from 'react-player'
import classNames from 'classnames'

const styles = theme => ({
  root: {
    backgroundColor: fade(theme.palette.primary.main, 0.2),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  fixed: {
    flex: 'none',
  },
  media: {
    maxWidth: 640,
    flex: 1,
    position: 'relative',
    '&::before': {
      display: 'block',
      content: '""',
    },
  },
  media_16_9: {
    '&::before': {
      marginTop: `${9 / 16 * 100}%`,
    },
  },
  media_4_3: {
    '&::before': {
      marginTop: `${3 / 4 * 100}%`,
    },
  },
  mediaContent: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  mediaContentWithIcon: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mediaIcon: {
    /*
    fontSize: 200,
    [theme.breakpoints.up('sm')]: {
      fontSize: 300,
    },
    */
    fontSize: 'calc(10em + 10vmin)',
    color: fade(theme.palette.primary.main, 0.9),
    fill: fade(theme.palette.primary.main, 0.9),
  },
  videoThumbnailIcon: {
    position: 'absolute',
    color: theme.palette.common.white,
    bottom: theme.spacing.unit / 2,
    right: theme.spacing.unit / 2,
  },
})

class ActivityMedia extends React.Component {
  static displayName = 'ActivityMedia'

  static propTypes = {
    aspectRatio: PropTypes.oneOf(['16:9', '4:3']),
    className: PropTypes.string,
    fixed: PropTypes.bool,
    icon: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.element,
    ]),
    image: PropTypes.string,
    video: PropTypes.string,
    videoThumbnail: PropTypes.bool,
  }

  static defaultProps = {
    aspectRatio: '16:9',
    fixed: false,
    icon: 'landscape',
    videoThumbnail: false,
  }

  _getYoutubeThumbnail(video = '') {
    const regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/
    const match = video.match(regExp)
    const id = match && match[2].length === 11 && match[2]
    return id && `//img.youtube.com/vi/${id}/mqdefault.jpg`
  }

  render() {
    const {aspectRatio, classes, icon, image, fixed, video, videoThumbnail} = this.props
    const className = classNames(classes.media, {
      [classes.media_16_9]: aspectRatio === '16:9',
      [classes.media_4_3]: aspectRatio === '4:3',
    })
    return (
      <div className={classNames(classes.root, {
        [classes.fixed]: fixed,
      })}
      >
        {(video && videoThumbnail) || (!video && image) ? (
          <CardMedia
            className={className}
            image={videoThumbnail && this._getYoutubeThumbnail(video) || image}
          >
            {video && videoThumbnail ? (
              <div className={classes.mediaContent}>
                <Icon className={classes.videoThumbnailIcon}>videocam</Icon>
              </div>
            ) : null}
          </CardMedia>
        ) : (
          <div className={className}>
            <div className={classNames(classes.mediaContent, {
              [classes.mediaContentWithIcon]: !video,
            })}
            >
              {video ? (
                <ReactPlayer
                  controls
                  height={'100%'}
                  url={video}
                  width={'100%'}
                />
              ) : React.isValidElement(icon) ? React.cloneElement(icon, {
                className: classNames(classes.mediaIcon, icon.props.className),
              }) : (
                <Icon className={classes.mediaIcon}>
                  {icon}
                </Icon>
              )}
            </div>
          </div>
        )}
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(ActivityMedia)