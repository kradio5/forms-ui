import React from 'react'
import PropTypes from 'prop-types'
import MaterialDatePicker from './MaterialDatePicker'
import NativeDatePicker from './NativeDatePicker'
import ydwUtil from 'util/ydw'
import i18n from 'util/i18n'

class DatePicker extends React.Component {
  static displayName = 'DatePicker'

  static propTypes = {
    disabled: PropTypes.bool,
    fullWidth: PropTypes.bool,
    id: PropTypes.string,
    label: PropTypes.string,
    margin: PropTypes.oneOf(['none', 'dense', 'normal']),
    maxDate: PropTypes.instanceOf(Date),
    minDate: PropTypes.instanceOf(Date),
    onChange: PropTypes.func,
    openToYearSelection: PropTypes.bool,
    value: PropTypes.instanceOf(Date),
  }

  static defaultProps = {
    disabled: false,
    fullWidth: true,
    id: 'date-picker',
    label: i18n.t('ui.titles.date'),
    margin: 'normal',
    onChange: () => { },
    openToYearSelection: false,
    value: new Date(),
  }

  /**
   * @see https://stackoverflow.com/questions/10193294/how-can-i-tell-if-a-browser-supports-input-type-date
   */
  _isDateInputSupported() {
    var illegalValue = 'not-a-date'
    var input = document.createElement('input')
    input.setAttribute('type', 'date')
    input.setAttribute('value', illegalValue)

    return input.type === 'date' && !(input.value === illegalValue)
  }

  render() {
    const {openToYearSelection, ...restProps} = this.props
    if (ydwUtil.isMobile() && this._isDateInputSupported()) {
      return (
        <NativeDatePicker
          {...restProps}
        />
      )
    }
    return (
      <MaterialDatePicker
        {...restProps}
        openToYearSelection={openToYearSelection}
      />
    )
  }
}

export default DatePicker