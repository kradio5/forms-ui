import React from 'react'
import PropTypes from 'prop-types'
import {DatePicker} from 'material-ui-pickers'
import ydwUtil from 'util/ydw'
import moment from 'util/moment'
import i18n from 'util/i18n'

class MaterialDatePicker extends React.Component {
  static displayName = 'MaterialDatePicker'

  static propTypes = {
    animateYearScrolling: PropTypes.bool,
    disabled: PropTypes.bool,
    fullWidth: PropTypes.bool,
    id: PropTypes.string,
    label: PropTypes.string,
    labelFunc: PropTypes.func,
    maxDate: PropTypes.instanceOf(Date),
    minDate: PropTypes.instanceOf(Date),
    onChange: PropTypes.func,
    showTodayButton: PropTypes.bool,
    todayLabel: PropTypes.string,
    value: PropTypes.instanceOf(Date),
  }

  static defaultProps = {
    animateYearScrolling: false,
    labelFunc: (date, invalidLabel) => {
      if (date === null) {
        return ''
      }

      if (moment.isMoment(date) || moment.isDate(date)) {
        return ydwUtil.formatStartDay(date)
      }

      return invalidLabel
    },
    showTodayButton: true,
    todayLabel: i18n.t('ui.titles.sameDay'),
  }

  constructor(props) {
    super(props)

    this.state = {
      value: props.value,
    }
  }

  handleChange = value => {
    this.setState({
      value: moment.isMoment(value) ? value.toDate() : value,
    }, () => {
      this.props.onChange(this.state.value)
    })
  }

  render() {
    const {onChange, value, ...restProps} = this.props
    return (
      <DatePicker
        InputLabelProps={{shrink: true}}
        {...restProps}
        onChange={this.handleChange}
        value={this.state.value}
      />
    )
  }
}

export default MaterialDatePicker