import React from 'react'
import PropTypes from 'prop-types'
import TextField from '@material-ui/core/TextField'
import moment from 'util/moment'

class NativeDatePicker extends React.Component {
  static displayName = 'NativeDatePicker'

  static propTypes = {
    disabled: PropTypes.bool,
    fullWidth: PropTypes.bool,
    id: PropTypes.string,
    label: PropTypes.string,
    maxDate: PropTypes.instanceOf(Date),
    minDate: PropTypes.instanceOf(Date),
    onChange: PropTypes.func,
    value: PropTypes.instanceOf(Date),
  }

  constructor(props) {
    super(props)

    this.state = {
      value: moment(props.value).format('YYYY-MM-DD'),
    }
  }

  handleChange = e => {
    const {value} = e.target
    const m = moment(value, 'YYYY-MM-DD')
    this.setState({value}, () => {
      if (m.isValid()) {
        this.props.onChange(m.toDate())
      }
    })
  }

  render() {
    const {maxDate, minDate, onChange, value, ...restProps} = this.props
    return (
      <TextField
        InputLabelProps={{shrink: true}}
        {...restProps}
        max={maxDate && moment(maxDate).format('YYYY-MM-DD')}
        min={minDate && moment(minDate).format('YYYY-MM-DD')}
        onChange={this.handleChange}
        type="date"
        value={this.state.value}
      />
    )
  }
}

export default NativeDatePicker
