import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import {fade} from '@material-ui/core/styles/colorManipulator'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import Zoom from '@material-ui/core/Zoom'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Tooltip from '@material-ui/core/Tooltip'
import Icon from '@material-ui/core/Icon'
import Button from '@material-ui/core/Button'
import classNames from 'classnames'

const supportsTouch = 'ontouchstart' in window

const styles = theme => ({
  root: {},
  backdrop: {
    backgroundColor: supportsTouch ? fade(theme.palette.common.black, .5) : 'transparent',
  },
  labelPaper: {
    position: 'absolute',
    right: 64,
    padding: [[theme.spacing.unit / 2, theme.spacing.unit]],
  },
  label: {
    color: theme.palette.common.black,
    fontWeight: 500,
  },
  buttons: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'visible',
  },
  zoomContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
  },
  icon: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    '&$icon_open': {
      transform: `rotate(${90 + 45}deg)`,
    },
  },
  icon_open: {},
})

class FabGroup extends React.Component {
  static displayName = 'FabGroup'

  static propTypes = {
    children: PropTypes.node,
    icon: PropTypes.string,
  }

  static defaultProps = {
    icon: 'add',
  }

  state = {
    open: false,
  }

  handleToggleOpen = () => {
    this.setState(({open}) => ({
      open: !open,
    }))
  }

  handleClose = () => {
    this.setState({open: false})
  }

  render() {
    const {children, classes, className, icon} = this.props
    const {open} = this.state
    return (
      <div className={classNames(classes.root, className)}>
        <div className={classes.buttons}>
          {React.Children.map(children, (child, index) => {
            const handler = child.props.onClick
            const handleClick = (...args) => {
              this.handleClose()
              if (typeof handler === 'function') {
                handler(...args)
              }
            }
            const button = (
              <Zoom
                in={open}
                style={{transitionDelay: open ? index * 50 + 50 : children.length * 50 - index * 50}}
                unmountOnExit
              >
                {React.cloneElement(child, {onClick: handleClick})}
              </Zoom>
            )
            return (
              <div
                className={classes.zoomContainer}
                style={{top: `-${index * 64 + 64}px`}}
              >
                {supportsTouch && (
                  <Fade
                    in={open}
                    style={{transitionDelay: open ? index * 50 + 50 : children.length * 50 - index * 50}}
                    unmountOnExit
                  >
                    <Paper
                      className={classes.labelPaper}
                      onClick={handleClick}
                    >
                      <Typography
                        className={classes.label}
                        noWrap
                        variant="caption"
                      >
                        {child.props['aria-label']}
                      </Typography>
                    </Paper>
                  </Fade>
                )}
                {supportsTouch ? button : open ? (
                  <Tooltip
                    disableTouchListener
                    placement="left"
                    title={child.props['aria-label']}
                  >
                    {button}
                  </Tooltip>
                ) : null}
              </div>
            )
          })}
        </div>
        <Button
          color={open ? 'default' : 'secondary'}
          onClick={this.handleToggleOpen}
          variant="fab"
        >
          <Icon className={classNames(classes.icon, {[classes.icon_open]: open})}>
            {icon}
          </Icon>
        </Button>
        <Backdrop
          className={classes.backdrop}
          onClick={this.handleClose}
          open={open}
          unmountOnExit
        />
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(FabGroup)
