import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import Preloader from 'components/Preloader'

const styles = {
  /*
  root: {
    position: 'relative',
    width: '100%',
    height: '100%',
  },
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    overflowX: 'hidden',
    overflowY: 'scroll',
  },
  */
}

class ScrollPager extends React.Component {
  static displayName = 'ScrollPager'

  static propTypes = {
    children: PropTypes.node,
    onNext: PropTypes.func,
    resource: PropTypes.shape({
      link: PropTypes.func,
      links: PropTypes.object,
    }),
    scrollContainer: PropTypes.string,
    scrollThreshold: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.func,
    ]),
  }

  static defaultProps = {
    onNext: () => { },
    scrollThreshold: container => container.clientHeight / 2,
  }

  constructor(props) {
    super(props)

    this.state = {
      isLoading: false,
    }
  }

  componentDidMount() {
    this.isMounted_ = true
    this.isTicking_ = false
    const {scrollContainer} = this.props
    this.container_ = scrollContainer ?
      document.querySelector(scrollContainer) : ReactDOM.findDOMNode(this).offsetParent
    this.container_.addEventListener('scroll', this.handleUpdate)
    this.container_.addEventListener('resize', this.handleUpdate)
    // this.handleUpdate_()
  }

  componentWillReceiveProps({resource}) {
    if (resource.links !== this.props.resource.links && this.state.isLoading) {
      this.setState({isLoading: false})
    }
  }

  componentWillUnmount() {
    if (this.requestId_) {
      window.cancelAnimationFrame(this.requestId_)
      this.requestId_ = null
    }
    if (this.container_) {
      this.container_.removeEventListener('scroll', this.handleUpdate)
      this.container_.removeEventListener('resize', this.handleUpdate)
    }
    this.isMounted_ = false
    this.isTicking_ = false
  }

  handleUpdate = () => {
    if (!this.isTicking_) {
      this.requestId_ = window.requestAnimationFrame(() => {
        this.handleUpdate_()
        this.isTicking_ = false
      })
    }
    this.isTicking_ = true
  }

  handleUpdate_ = () => {
    if (!this.isMounted_ || this.state.isLoading) {
      return
    }
    const {clientHeight, scrollHeight, scrollTop} = this.container_
    let {resource, scrollThreshold} = this.props
    if (typeof scrollThreshold === 'function') {
      scrollThreshold = scrollThreshold(this.container_)
    }
    const scrollBalance = scrollHeight - scrollTop - clientHeight
    if (scrollHeight > 0 && (scrollBalance < scrollThreshold || scrollThreshold > scrollHeight)) {
      if (resource.links && resource.links.next) {
        this.setState({isLoading: true}, () => {
          this.props.onNext()
        })
      }
    }
  }

  render() {
    const {classes, children} = this.props
    return (
      <div className={classes.root}>
        {children}
        {this.state.isLoading && (
          <div className={classes.preloader}>
            <Preloader />
          </div>
        )}
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(ScrollPager)
