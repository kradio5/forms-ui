import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import Slider from 'components/Slider'
import math from 'util/math'
import i18n from 'util/i18n'

const RE_FIXED_3 = /(?:0(?!0|[1-9])|[1-9])\d*(?:\.\d{0,3})?/
const RE_FIXED_2 = /(?:0(?!0|[1-9])|[1-9])\d*(?:\.\d{0,2})?/
const RE_FIXED_1 = /(?:0(?!0|[1-9])|[1-9])\d*(?:\.\d{0,1})?/
const RE_INT = /(?:0(?!0|[1-9])|[1-9])\d{0,3}/

const MET_NAMES = ['VERY_LIGHT', 'LIGHT', 'LIGHT_MODERATE', 'MODERATE', 'MODERATE_HARD', 'HARD', 'VERY_HARD']
const MET_VALUES = [1.5, 2.5, 3.5, 4.5, 6, 7.5, 10]

const styles = theme => ({
  root: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'flex-start',
    marginBottom: theme.spacing.unit * 1.5,
  },
  metSlider: {
    position: 'relative',
    // width: '100%',
    flexGrow: 1,
    overflow: 'visible',
    marginRight: theme.spacing.unit * 2,
    [theme.breakpoints.up('sm')]: {
      marginRight: theme.spacing.unit * 2.5,
    },
  },
  helperText: {
    width: '100%',
    position: 'absolute',
    top: theme.spacing.unit * 2,
    fontSize: theme.typography.pxToRem(16),
    color: 'rgba(0, 0, 0, 0.87)',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
  },
  metValue: {
    width: 40,
  },
  metValueInput: {
    // textAlign: 'center',
  },
})

class MetSlider extends React.Component {
  static displayName = 'MetSlider'

  static propTypes = {
    name: PropTypes.string,
    onChange: PropTypes.func,
    onRef: PropTypes.func,
    precision: PropTypes.number,
    unit: PropTypes.string,
    validator: PropTypes.func,
    value: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]),
  }

  static defaultProps = {
    onChange: () => { },
    onRef: () => { },
    precision: 2,
    unit: 'met',
    validator: function (value) {
      return value.toNumber() > 0
    },
    value: 4.5,
  }

  constructor(props) {
    super(props)

    const state = this._createState(props)
    this.state = {
      ...state,
      isDirty: false,
      rawValue: Number(state.rawValue) ? state.rawValue : '',
    }
  }

  componentDidMount() {
    this.props.onRef(this)
  }

  componentWillReceiveProps(nextProps) {
    const {isDirty, value} = this.state
    if (!isDirty && value.toNumber() !== Number(nextProps.value)) {
      this.setState(this._createState(nextProps))
    }
  }

  _createState(props) {
    const value = math.unit(props.value, props.unit)
    return {
      invalid: false,
      sliderIndex: MET_VALUES.indexOf(this._getClosestSliderValue(value.toNumber())),
      value: value,
      unit: props.unit,
      rawValue: props.value && this._normalizeOut(value).toString(),
    }
  }

  isValid() {
    return this.props.validator(this.state.value)
  }

  validate() {
    const isValid = this.isValid()

    this.setState({
      invalid: !isValid,
    })

    return isValid
  }

  handleChange = e => {
    let rawValue = this._normalizeIn(e.target.value)
    let val = !rawValue ? '0' : rawValue

    if (Number.isNaN(val)) {
      return
    }

    const sliderValue = this._getClosestSliderValue(val)
    this.setState({
      isDirty: true,
      invalid: false,
      value: math.unit(val, this.props.unit),
      sliderIndex: MET_VALUES.indexOf(sliderValue),
      rawValue: rawValue,
    }, () => {
      this.props.onChange({
        name: this.props.name,
        value: this._normalizeOut(this.state.value),
        unit: this.props.unit,
      })
    })
  }

  handleSliderChange = val => {
    const value = MET_VALUES[val]
    this.setState({
      invalid: false,
      sliderIndex: val,
      value: math.unit(value, this.props.unit),
      rawValue: value.toString(),
    }, () => {
      this.props.onChange({
        name: this.props.name,
        value: this._normalizeOut(this.state.value),
        unit: this.props.unit,
      })
    })
  }

  _getClosestSliderValue(val) {
    return MET_VALUES
      .reduce((v, c) => {
        if (Math.abs(v - val) > Math.abs(c - val)) {
          return c
        }
        return v
      }, MET_VALUES[0])
  }

  _normalizeIn(val) {
    let s = (val || '').toString()
      .replace(/,+/g, '.')

    switch (this.props.precision) {
      case 3:
        return (s.match(RE_FIXED_3) || [''])[0]
      case 2:
        return (s.match(RE_FIXED_2) || [''])[0]
      case 1:
        return (s.match(RE_FIXED_1) || [''])[0]
      default:
        return (s.match(RE_INT) || [''])[0]
    }
  }

  _normalizeOut(val) {
    return Number(math.format(val.toNumber(), {
      notation: 'fixed',
      precision: this.props.precision,
    }))
  }

  render() {
    const {classes, theme} = this.props
    const {
      invalid,
      sliderIndex,
      rawValue,
    } = this.state
    const metName = MET_NAMES[sliderIndex]
    const marks = {
      '0': '',
      '1': i18n.t(`model.Intensity.LIGHT.shortTitle`),
      '2': '',
      '3': i18n.t(`model.Intensity.MODERATE.shortTitle`),
      '4': '',
      '5': i18n.t(`model.Intensity.HARD.shortTitle`),
      '6': '',
    }
    /*
    const marks = MET_VALUES.reduce((marks, value, index) => {
      marks[index] = value.toString()
      return marks
    }, {})
    */
    return (
      <div className={classes.root}>
        <FormControl
          aria-describedby="intensity-description"
          className={classes.metSlider}
          error={invalid}
          margin="normal"
        >
          <InputLabel
            htmlFor="met-slider"
            shrink
          >
            {i18n.t('model.Intensity.title')}
          </InputLabel>
          <Slider
            id="met-slider"
            marks={marks}
            max={MET_VALUES.length - 1}
            onChange={this.handleSliderChange}
            value={sliderIndex}
          />
          <FormHelperText
            className={classes.helperText}
            id="intensity-description"
          >
            {i18n.t(`model.Intensity.${metName}.description`)}
          </FormHelperText>
        </FormControl>
        <FormControl
          className={classes.metValue}
          error={invalid}
          margin="normal"
        >
          <InputLabel
            htmlFor="met-control"
            shrink
          >
            {'MET'}
          </InputLabel>
          <Input
            classes={{input: classes.metValueInput}}
            fullWidth
            id="met-control"
            inputProps={{maxLength: 4}}
            onChange={this.handleChange}
            value={rawValue}
          />
        </FormControl>
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(MetSlider)
