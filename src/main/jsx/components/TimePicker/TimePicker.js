import React from 'react'
import PropTypes from 'prop-types'
import MaterialTimePicker from './MaterialTimePicker'
import NativeTimePicker from './NativeTimePicker'
import ydwUtil from 'util/ydw'
import i18n from 'util/i18n'

class TimePicker extends React.Component {
  static displayName = 'TimePicker'

  static propTypes = {
    disabled: PropTypes.bool,
    fullWidth: PropTypes.bool,
    id: PropTypes.string,
    label: PropTypes.string,
    margin: PropTypes.oneOf(['none', 'dense', 'normal']),
    onChange: PropTypes.func,
    value: PropTypes.instanceOf(Date),
  }

  static defaultProps = {
    disabled: false,
    fullWidth: true,
    id: 'time-picker',
    label: i18n.t('ui.titles.time'),
    margin: 'normal',
    onChange: () => { },
    value: new Date(),
  }

  /**
   * @see https://stackoverflow.com/questions/10193294/how-can-i-tell-if-a-browser-supports-input-type-date
   */
  _isTimeInputSupported() {
    var illegalValue = 'not-a-date'
    var input = document.createElement('input')
    input.setAttribute('type', 'time')
    input.setAttribute('value', illegalValue)

    return input.type === 'time' && !(input.value === illegalValue)
  }

  render() {
    if (ydwUtil.isMobile() && this._isTimeInputSupported()) {
      return (
        <NativeTimePicker
          {...this.props}
        />
      )
    }
    return (
      <MaterialTimePicker
        {...this.props}
      />
    )
  }
}

export default TimePicker