import React from 'react'
import PropTypes from 'prop-types'
import TextField from '@material-ui/core/TextField'
import moment from 'util/moment'

class NativeTimePicker extends React.Component {
  static displayName = 'NativeTimePicker'

  static propTypes = {
    disabled: PropTypes.bool,
    fullWidth: PropTypes.bool,
    id: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func,
    value: PropTypes.instanceOf(Date),
  }

  constructor(props) {
    super(props)

    this.state = {
      value: moment(props.value).format('HH:mm'),
    }
  }

  handleChange = e => {
    const {value} = e.target
    const m = moment(value, 'HH:mm')
    this.setState({value}, () => {
      if (m.isValid()) {
        this.props.onChange(m.toDate())
      }
    })
  }

  render() {
    const {onChange, value, ...restProps} = this.props
    return (
      <TextField
        InputLabelProps={{shrink: true}}
        {...restProps}
        onChange={this.handleChange}
        type="time"
        value={this.state.value}
      />
    )
  }
}

export default NativeTimePicker
