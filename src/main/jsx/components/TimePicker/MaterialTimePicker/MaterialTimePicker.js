import React from 'react'
import PropTypes from 'prop-types'
import {TimePicker} from 'material-ui-pickers'
import moment from 'util/moment'

class MaterialTimePicker extends React.Component {
  static displayName = 'MaterialTimePicker'

  static propTypes = {
    disabled: PropTypes.bool,
    fullWidth: PropTypes.bool,
    id: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func,
    value: PropTypes.instanceOf(Date),
  }

  constructor(props) {
    super(props)

    this.state = {
      value: props.value,
    }
  }

  handleChange = value => {
    this.setState({
      value: moment.isMoment(value) ? value.toDate() : value,
    }, () => {
      this.props.onChange(this.state.value)
    })
  }

  render() {
    const {onChange, value, ...restProps} = this.props
    return (
      <TimePicker
        InputLabelProps={{shrink: true}}
        {...restProps}
        onChange={this.handleChange}
        value={this.state.value}
      />
    )
  }
}

export default MaterialTimePicker