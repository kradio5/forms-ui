import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import Input from '@material-ui/core/Input'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormGroup from '@material-ui/core/FormGroup'
import FormLabel from '@material-ui/core/FormLabel'
import FormControl from '@material-ui/core/FormControl'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import {DatePicker} from 'material-ui-pickers'
import AppToolbar, {ToolbarRow, ToolbarSection, ToolbarTitle} from 'components/AppToolbar'
import {RRule} from 'rrule'
import classNames from 'classnames'
import moment from 'util/moment'
import i18n from 'util/i18n'

const FREQ_VALUES = [
  'DAILY',
  'WEEKLY',
  'MONTHLY',
  'YEARLY',
]

const WEEKDAY_VALUES = [0, 1, 2, 3, 4, 5, 6]
  .map(i => moment(new Date, null, 'en').weekday(i).format('dd').toUpperCase())

const MAX_COUNT = 730
const MAX_INTERVAL = 99

const styles = theme => ({
  root: {
    width: '100%',
    height: '100%',
  },
  appBar: {
    [theme.breakpoints.up('md')]: {
      width: '100%',
    },
  },
  toolbar: {
    flexDirection: theme.direction === 'rtl' ? 'row-reverse' : 'row',
    flexWrap: 'nowrap',
  },
  content: {
    position: 'relative',
    width: '100%',
    height: 'calc(100% - 56px)',
    padding: theme.spacing.unit,
    marginTop: 56,
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 64px)',
      padding: theme.spacing.unit * 2,
      marginTop: 64,
    },
    overflowX: 'hidden',
    overflowY: 'scroll',
  },
  freqSelect: {
    marginRight: theme.spacing.unit,
  },
  inputRoot: {
    margin: [[0, theme.spacing.unit]],
  },
  input: {
    textAlign: 'center',
  },
  fieldset: {
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit,
    width: '100%',
    display: 'flex',
    flexWrap: 'nowrap',
    position: 'relative',
  },
  fieldsetInterval: {
    flexDirection: theme.direction === 'rtl' ? 'row-reverse' : 'row',
  },
  fieldsetMode: {
    flexDirection: 'column',
  },
  intervalControlLabel: {
    fontFamily: theme.typography.fontFamily,
    color: theme.palette.primary.main,
    fontSize: theme.typography.pxToRem(16),
    lineHeight: 1,
    padding: 0,
  },
  weekdayButton: {
    fontSize: theme.typography.pxToRem(16),
    fontWeight: 400,
    marginRight: theme.spacing.unit * 1.5,
    width: theme.spacing.unit * 4,
    height: theme.spacing.unit * 4,
    minWidth: theme.spacing.unit * 4,
    boxShadow: theme.shadows[2],
    '&:active': {
      boxShadow: theme.shadows[4],
    },
  },
  weekdayButtons: {
    margin: [[theme.spacing.unit, 0]],
  },
})

class CustomRecurrenceControl extends React.Component {
  static displayName = 'CustomRecurrenceControl'

  static propTypes = {
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    start: PropTypes.instanceOf(Date),
    value: PropTypes.string,
  }

  constructor(props) {
    super(props)

    const value = new RRule({
      freq: RRule.WEEKLY,
      // wkst: RRule[WEEKDAY_VALUES[0]],
      ...RRule.fromString(props.value).origOptions,
    })
    const {
      byweekday,
      bymonthday,
      count,
      interval,
      freq,
      until,
    } = value.origOptions

    const m = moment(this.props.start, null, 'en')

    this.state = {
      byweekday: (byweekday || (freq === RRule.WEEKLY ? [RRule[m.format('dd').toUpperCase()]] : null)),
      bymonthday: (bymonthday || (freq === RRule.MONTHLY && !byweekday ? [m.date()] : null)),
      interval: interval > 0 ? interval : 1,
      intervalRaw: interval > 0 ? interval.toString() : '1',
      freq,
      until: until || m.clone().add(1, 'month').toDate(),
      count: count > 0 ? count : 1,
      countRaw: count > 0 ? count.toString() : '1',
      mode: count > 0 ? 'COUNT' : until ? 'UNTIL' : 'NEVER',
    }
  }

  handleCancel = () => {
    this.props.onCancel()
  }

  handleDone = () => {
    const {bymonthday, byweekday, count, interval, freq, mode, until} = this.state
    const value = new RRule({
      freq,
      bymonthday,
      byweekday,
      count: mode === 'COUNT' ? count : null,
      interval,
      until: mode === 'UNTIL' ? until : null,
    })

    this.props.onSubmit(value.toString())
  }

  handleFreqChange = e => {
    const {value: freq} = e.target
    const m = moment(this.props.start, null, 'en')
    this.setState({
      freq,
      byweekday: freq === RRule.WEEKLY ? [RRule[m.format('dd').toUpperCase()]] : null,
      bymonthday: freq === RRule.MONTHLY ? [m.date()] : null,
    })
  }

  handleIntervalChange = e => {
    let {value: val} = e.target
    val = val.replace(/[.,-]/g, '')
    const n = Number(val)
    const interval = n > 0 ? Math.min(n, MAX_INTERVAL) : 1
    const intervalRaw = val === '' ? val : interval.toString()

    this.setState({interval, intervalRaw})
  }

  handleIntervalBlur = e => {
    const {value} = e.target
    if (value === '') {
      this.setState({intervalRaw: '1'})
    }
  }

  handleModeChange = (e, val) => {
    this.setState({mode: val})
  }

  handleCountChange = e => {
    let {value: val} = e.target
    val = val.replace(/[.,-]/g, '')
    const n = Number(val)
    const count = n > 0 ? Math.min(n, MAX_COUNT) : 1
    const countRaw = val === '' ? val : count.toString()
    this.setState({count, countRaw, mode: 'COUNT'})
  }

  handleCountBlur = e => {
    const {value} = e.target
    if (value === '') {
      this.setState({countRaw: '1'})
    }
  }

  handleUntilChange = date => {
    this.setState({until: date, mode: 'UNTIL'})
  }

  handleWeekdayClick = value => {
    let byweekday = this.state.byweekday || []
    const weekdayValue = RRule[value]
    const weekdayIndex = byweekday.findIndex(v => v.weekday === weekdayValue.weekday)

    if (weekdayIndex > -1) {
      byweekday.splice(weekdayIndex, 1)
    } else {
      byweekday = byweekday.concat(weekdayValue)
    }
    byweekday.sort((a, b) => a.weekday > b.weekday)

    this.setState({byweekday})
  }

  handleMonthlyRepeatChange = e => {
    const {value} = e.target
    this.setState({
      bymonthday: value === 'bymonthday' ? [moment(this.props.start).date()] : null,
      byweekday: value === 'byweekday' ? [this._getWeekOfMonthPos()] : null,
    })
  }

  _getInputWidth(value) {
    const size = Math.max(value.length, 1)
    const factor = size === 1 ? 12 : 10

    return `${size * factor - size + 1}px`
  }

  _getWeekOfMonthPos() {
    const weekday = moment(this.props.start, null, 'en').format('dd').toUpperCase()
    const monthweek = this._getWeekOfMonth()

    return RRule[weekday].nth(monthweek === 5 ? -1 : monthweek)
  }

  _getWeekOfMonth() {
    return Math.ceil(moment(this.props.start).date() / 7)
  }

  _renderWeekday(weekday, i) {
    const {classes} = this.props
    const byweekday = this.state.byweekday || []
    const index = RRule[weekday].weekday
    return (
      <Button
        className={classes.weekdayButton}
        color={byweekday.some(v => v.weekday === index) ? 'secondary' : 'default'}
        key={index}
        mini
        onClick={() => this.handleWeekdayClick(weekday)}
        size="small"
        variant="fab"
      >
        {moment().weekday(i).format('dd')[0]}
      </Button>
    )
  }

  _renderMonthlyRepeatSelect() {
    const m = moment(this.props.start)
    const monthday = m.format('D')
    const weekday = m.format('dddd')
    const monthweek = i18n.t(`ui.controls.Repeat.RRule.Frequency.MONTHLY.byweekday.monthweek.${this._getWeekOfMonth()}`)
    return (
      <FormControl margin="normal">
        <Select
          onChange={this.handleMonthlyRepeatChange}
          value={this.state.bymonthday ? 'bymonthday' : 'byweekday'}
        >
          <MenuItem value="bymonthday">
            {i18n.t('ui.controls.Repeat.RRule.Frequency.MONTHLY.bymonthday.label', {monthday})}
          </MenuItem>
          <MenuItem value="byweekday">
            {i18n.t('ui.controls.Repeat.RRule.Frequency.MONTHLY.byweekday.label', {weekday, monthweek})}
          </MenuItem>
        </Select>
      </FormControl>
    )
  }

  render() {
    const {classes} = this.props
    const {count, countRaw, interval, intervalRaw, mode, freq, until} = this.state
    const freqValue = FREQ_VALUES.find(v => RRule[v] === freq)
    const UntilInput = props => {
      const m = moment(until)
      const value = m.format(
        moment().isSame(m, 'year') ? 'M/D' : 'M/D/Y'
      )
      return (
        <Input
          classes={{input: classes.input, root: classes.inputRoot}}
          onBlur={props.onBlur}
          onChange={props.onChange}
          onClick={e => {
            this.handleModeChange(null, 'UNTIL')
            props.onClick(e)
          }}
          onKeyPress={props.onKeyPress}
          readOnly
          style={{width: this._getInputWidth(value)}}
          value={value}
        />
      )
    }
    const untilLabel = (
      <span>
        {i18n.t('ui.controls.Repeat.RRule.Mode.UNTIL.label')}
        <DatePicker
          TextFieldComponent={UntilInput}
          animateYearScrolling={false}
          onChange={this.handleUntilChange}
          value={until}
        />
      </span>
    )
    const countLabel = (
      <span>
        {i18n.t('ui.controls.Repeat.RRule.Mode.COUNT.preLabel')}
        <Input
          classes={{input: classes.input, root: classes.inputRoot}}
          onBlur={this.handleCountBlur}
          onChange={this.handleCountChange}
          onFocus={() => {
            this.handleModeChange(null, 'COUNT')
          }}
          style={{width: this._getInputWidth(countRaw)}}
          value={countRaw}
        />
        {i18n.t('ui.controls.Repeat.RRule.Mode.COUNT.postLabel', count)}
      </span>
    )
    return (
      <div className={classes.root}>
        <AppToolbar className={classes.appBar}>
          <ToolbarRow>
            <ToolbarSection variant="start">
              <IconButton
                color="secondary"
                onClick={this.handleCancel}
              >
                <Icon>{'arrow_back'}</Icon>
              </IconButton>
              <ToolbarTitle>
                {i18n.t('ui.controls.Repeat.RRule.Frequency.CUSTOM.title')}
              </ToolbarTitle>
            </ToolbarSection>
            <ToolbarSection variant="end">
              <IconButton
                color="secondary"
                onClick={this.handleDone}
              >
                <Icon>{'done'}</Icon>
              </IconButton>
            </ToolbarSection>
          </ToolbarRow>
        </AppToolbar>
        <div className={classes.content}>
          <div className={classNames(classes.fieldset, classes.fieldsetInterval)}>
            <FormLabel>
              {i18n.t(`ui.controls.Repeat.RRule.Interval.${freqValue}.preLabel`, interval)}
              <Input
                classes={{input: classes.input, root: classes.inputRoot}}
                maxLength="2"
                name="interval"
                onBlur={this.handleIntervalBlur}
                onChange={this.handleIntervalChange}
                style={{width: this._getInputWidth(intervalRaw)}}
                value={intervalRaw}
              />
              <Select
                className={classes.freqSelect}
                onChange={this.handleFreqChange}
                value={freq}
              >
                {FREQ_VALUES.map((value, index) => (
                  <MenuItem
                    key={index}
                    value={RRule[value]}
                  >
                    {i18n.t(`ui.controls.Repeat.RRule.Interval.${value}.label`, interval)}
                  </MenuItem>
                ))}
              </Select>
              {i18n.t(`ui.controls.Repeat.RRule.Interval.${freqValue}.postLabel`, interval)}
            </FormLabel>
          </div>
          {freq === RRule.WEEKLY && (
            <FormControl component="fieldset"
              margin="normal"
            >
              <FormLabel component="legend">
                {i18n.t('ui.controls.Repeat.RRule.Frequency.WEEKLY.byweekday.label')}
              </FormLabel>
              <FormGroup className={classes.weekdayButtons}
                row
              >
                {WEEKDAY_VALUES.map(this._renderWeekday, this)}
              </FormGroup>
            </FormControl>
          )}
          {freq === RRule.MONTHLY && this._renderMonthlyRepeatSelect()}
          <div className={classNames(classes.fieldset, classes.fieldsetMode)}>
            <FormLabel component="legend">
              {i18n.t('ui.controls.Repeat.RRule.Mode.label')}
            </FormLabel>
            <RadioGroup
              name="mode"
              onChange={this.handleModeChange}
              value={mode}
            >
              <FormControlLabel
                control={<Radio />}
                label={i18n.t('ui.controls.Repeat.RRule.Mode.NEVER.label')}
                value="NEVER"
              />
              <FormControlLabel
                control={<Radio />}
                label={untilLabel}
                value="UNTIL"
              />
              <FormControlLabel
                control={<Radio />}
                label={countLabel}
                value="COUNT"
              />
            </RadioGroup>
          </div>
        </div>
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(CustomRecurrenceControl)
