import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import Dialog from '@material-ui/core/Dialog'
import CustomRecurrenceControl from './CustomRecurrenceControl'
import RRuleTranslator from './RRuleTranslator'
import {RRule} from 'rrule'
import i18n from 'util/i18n'

const FREQ_VALUES = [
  'DAILY',
  'WEEKLY',
  'MONTHLY',
  'YEARLY',
]

const styles = theme => ({
})

class RecurrenceControl extends React.Component {
  static displayName = 'RecurrenceControl'

  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func,
    start: PropTypes.instanceOf(Date),
    value: PropTypes.string,
  }

  static defaultProps = {
    disabled: false,
    label: i18n.t('ui.controls.Repeat.label'),
    onChange: () => { },
    start: new Date,
    value: '',
  }

  constructor(props) {
    super(props)

    const value = props.value ? RRule.fromString(props.value) : new RRule()
    let freq_ = props.value && FREQ_VALUES.find(v => value.options.freq === RRule[v]) || ''
    // Check if it's a custom rrule
    if (freq_ && ['interval', 'count', 'until', 'byweekday', 'bymonthday'].some(v => v in value.origOptions)) {
      freq_ = '_'
    }
    this.state = {
      open: false,
      freq_,
      value: value.toString(),
    }
  }

  handleFreqChange = e => {
    const {value: val} = e.target
    const isCustom = val === 'CUSTOM'
    const newValue = new RRule({
      freq: FREQ_VALUES.includes(val) ? RRule[val] : null,
    })
    this.setState(({freq_, value}) => ({
      value: isCustom ? value : newValue.toString(),
      open: isCustom,
      freq_: isCustom ? freq_ : val,
    }), () => {
      if (!isCustom) {
        this.props.onChange(val && this.state.value)
      }
    })
  }

  handleSubmit = value => {
    this.setState({freq_: '_', open: false, value}, () => {
      this.props.onChange(this.state.value)
    })
  }

  handleClose = () => {
    this.setState({open: false})
  }

  render() {
    const {classes, label} = this.props
    const {freq_, open, value} = this.state
    return (
      <div className={classes.root}>
        <TextField
          InputLabelProps={{shrink: true}}
          SelectProps={{displayEmpty: true}}
          fullWidth
          label={label}
          margin="normal"
          onChange={this.handleFreqChange}
          select
          value={freq_}
        >
          {freq_ === '_' && (
            <MenuItem value="_">
              {RRuleTranslator.translate(value)}
            </MenuItem>
          )}
          <MenuItem value="">
            {i18n.t('ui.controls.Repeat.RRule.Frequency.ONCE.label')}
          </MenuItem>
          {FREQ_VALUES.map((value, index) => (
            <MenuItem
              key={index}
              value={value}
            >
              {i18n.t(`ui.controls.Repeat.RRule.Frequency.${value}.label`)}
            </MenuItem>
          ))}
          <MenuItem value="CUSTOM">
            {i18n.t('ui.controls.Repeat.RRule.Frequency.CUSTOM.label')}
          </MenuItem>
        </TextField>
        <Dialog
          fullScreen
          onClose={this.handleClose}
          open={open}
        >
          <CustomRecurrenceControl
            onCancel={this.handleClose}
            onSubmit={this.handleSubmit}
            start={this.props.start}
            value={value}
          />
        </Dialog>
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(RecurrenceControl)
