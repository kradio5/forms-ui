import {RRule} from 'rrule'
import moment from 'util/moment'
import i18n from 'util/i18n'

const FREQ_VALUES = [
  'YEARLY',
  'MONTHLY',
  'WEEKLY',
  'DAILY',
]

class RRuleTranslator {
  _getWeekdayNames(weekdays) {
    return [].concat(weekdays || [])
      .map(weekday => moment().isoWeekday(weekday + 1).format('ddd'))
  }

  _getDelim() {
    return '; '
  }

  _getExtra(value) {
    const {
      count,
      until,
    } = RRule.fromString(value).origOptions
    let delim = this._getDelim()

    if (count) {
      return delim + i18n.t('ui.controls.Repeat.RRule.Mode.COUNT.text', count)
    } else if (until) {
      const m = moment(until)
      return delim + i18n.t('ui.controls.Repeat.RRule.Mode.UNTIL.text', {
        date: m.format(moment().isSame(m, 'year') ? 'M/D' : 'M/D/Y'),
      })
    }
    return ''
  }

  _getFreq(value) {
    const {
      freq,
      interval,
    } = RRule.fromString(value).options

    return i18n.t(`ui.controls.Repeat.RRule.Frequency.${FREQ_VALUES[freq]}.text`, interval)
  }

  translate(value) {
    if (!value) {
      return i18n.t('ui.controls.Repeat.RRule.Frequency.ONCE.text')
    }
    const {
      options: {
        byweekday,
        bynweekday,
        dtstart,
        freq,
      },
      origOptions,
    } = RRule.fromString(value)

    // Check if it's a simple rrule
    if (!['interval', 'count', 'until', 'byweekday', 'bymonthday'].some(v => v in origOptions)) {
      const freqValue = FREQ_VALUES.find(v => RRule[v] === freq)
      return i18n.t(`ui.controls.Repeat.RRule.Frequency.${freqValue}.label`)
    }

    let result = this._getFreq(value)
    const extra = this._getExtra(value)
    const postText = i18n.t('ui.controls.Repeat.RRule.Frequency.postText')

    switch (freq) {
      case RRule.WEEKLY:
        result += ' ' + i18n.t('ui.controls.Repeat.RRule.Frequency.WEEKLY.byweekday.text', {
          weekdays: this._getWeekdayNames(byweekday).join(', '),
        }) + extra
        break
      case RRule.MONTHLY:
        result += (bynweekday && bynweekday[0] ?
          ' (' + i18n.t('ui.controls.Repeat.RRule.Frequency.MONTHLY.byweekday.text', {
            weekday: moment(dtstart).format('dddd'),
            monthweek: i18n.t(`ui.controls.Repeat.RRule.Frequency.MONTHLY.byweekday.monthweek.${bynweekday[0][1] === -1 ? 5 : bynweekday[0][1]}`),
          }) + ')' : '') + extra
        break
      default:
        result += extra
    }

    return result + (postText && ' ' + postText)
  }
}

export default new RRuleTranslator()