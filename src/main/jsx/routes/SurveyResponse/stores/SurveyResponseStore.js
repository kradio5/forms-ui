import AppDispatcher from 'dispatcher/AppDispatcher'
import {ReduceStore} from 'flux/utils'
import {ActionTypes} from 'constants/AppConstants'

class SurveyResponseStore extends ReduceStore {

  getInitialState() {
    return { 
      response: {},
    }
  }

  getResponse() {
    return this.getState().response
  }

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.SURVEY_RESPONSE_LOAD: {
        const {response={}} = action 
        return {...state, response}
      }
      case ActionTypes.SURVEY_RESPONSE_CLEAR:
        return {...state, answers: [], pages: []}
      default:
        return state
    }
  }
}

export default new SurveyResponseStore(AppDispatcher)
