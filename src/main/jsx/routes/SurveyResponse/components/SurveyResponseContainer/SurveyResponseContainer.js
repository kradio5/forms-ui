import React from 'react'
import PropTypes from 'prop-types'
import {Container} from 'flux/utils'
import SurveyResponseStore from 'routes/SurveyResponse/stores/SurveyResponseStore'
import SurveyResponseActions from 'routes/SurveyResponse/actions/SurveyResponseActions'
import SurveyResponse from '../SurveyResponse'

class SurveyResponseContainer extends React.Component {

  static getStores() {
    return [SurveyResponseStore]
  }

  static calculateState() {
    return {
      surveyResponse: SurveyResponseStore.getResponse(),
    }
  }

  static displayName = 'SurveyResponseContainer'

  static propTypes = {
    match: PropTypes.shape({
      params: PropTypes.shape({
        surveyResponseId: PropTypes.string,
      }),
    }),
  }

  async componentDidMount() {
    const {surveyResponseId} = this.props.match.params
    this.setState({isLoading: true})
    await SurveyResponseActions.findById(surveyResponseId)
    this.setState({isLoading: false})
  }

  render() {
    return (
      <SurveyResponse
        {...this.props}
        {...this.state}
      />
    )
  }
}

export default Container.create(SurveyResponseContainer)
