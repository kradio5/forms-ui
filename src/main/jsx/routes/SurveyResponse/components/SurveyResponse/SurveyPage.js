import React from 'react'
import PropTypes from 'prop-types'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'

const Page = ({children, title, ...rest}) => {
  return (
    <div 
      {...rest}
    >
      <Typography color="textSecondary">
        {title}
      </Typography>
      <Card>
        <CardContent>
          {children}
        </CardContent>
      </Card>
    </div>
  )
}

Page.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
}

export default Page
