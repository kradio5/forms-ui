import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import AppToolbar, {ToolbarTitle, ToolbarRow, ToolbarSection} from 'components/AppToolbar'
import BackButton from 'components/BackButton'
import Preloader from 'components/Preloader'
import i18n from 'util/i18n'
import Question from './SurveyQuestion'
import Answer from './SurveyAnswer'
import Page from './SurveyPage'

const styles = theme => ({
  root: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    display: 'flex',
    flexDirection: theme.direction === 'rtl' ? 'row-reverse' : 'row',
    zIndex: 1,
    overflow: 'hidden',
  },
  content: {
    position: 'relative',
    width: '100%',
    height: 'calc(100% - 56px)',
    padding: theme.spacing.unit,
    marginTop: 56,
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 64px)',
      padding: theme.spacing.unit * 2,
      marginTop: 64,
    },
    overflowX: 'hidden',
    overflowY: 'scroll',
  },
  page : {
    padding: 16,
  },
})


class SurveyResponse extends React.PureComponent {
  static displayName = 'SurveyResponse'

  static propTypes = {
    classes: PropTypes.object,// eslint-disable-line react/forbid-prop-types
    isLoading: PropTypes.bool,
    surveyResponse: PropTypes.object,// eslint-disable-line react/forbid-prop-types
  }

  _renderAnswers(answer, index) {
    const {text, choice} = answer
    return (
      <Answer
        choice={choice}
        key={`answer-${index}`}
        text={text}
      />
    )

  }

  _renderQuestion(question, index){
    const {title, answers} = question
    return (
      <Question 
        key={index}
        title={title}
      >
        <List>
          {answers.map(this._renderAnswers, this)}
        </List>
      </Question>
    )
  }

  _renderPage(page) {
    const {classes} = this.props
    const {position, title, questions} = page
    return (
      <Page
        className={classes.page}
        key={`page-${position}`}
        title={i18n.t('ui.pages.SurveyResponse.page', {page: position, title})}
      >
        <List>
          {questions.map(this._renderQuestion, this)}
        </List>
      </Page>
    )
  }

  render() {
    const {
      classes,
      isLoading,
      surveyResponse,
    } = this.props

    const {pages: surveyPages=[]} = surveyResponse

    const pages = surveyPages.map(this._renderPage, this) 
    const toolbar = (
      <AppToolbar>
        <ToolbarRow>
          <ToolbarSection variant="start">
            <BackButton/>
            <ToolbarTitle>
              {i18n.t('ui.pages.SurveyResponse.title')}
            </ToolbarTitle>
          </ToolbarSection>
          <ToolbarSection variant="end" />
        </ToolbarRow>
      </AppToolbar>
    )

    return (
      <div className={classes.root}>
        {toolbar}
        <main className={classes.content}>
          <List>
            {pages}
          </List>
          <Preloader
            isLoading={isLoading}
          />
        </main>
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(SurveyResponse)
