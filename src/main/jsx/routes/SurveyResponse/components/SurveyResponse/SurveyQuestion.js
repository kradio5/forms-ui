import React from 'react'
import PropTypes from 'prop-types'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'

const Question = ({title, children}) => {
  return (
    <ListItem alignItems="flex-start"
      divider
    >
      <ListItemText 
        disableTypography 
        primary={title}
        secondary={children}
      />
    </ListItem>
  )
}

Question.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
}

export default Question
