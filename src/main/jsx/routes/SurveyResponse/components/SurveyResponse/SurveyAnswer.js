import React from 'react'
import PropTypes from 'prop-types'
import ListItem from '@material-ui/core/ListItem'
import Typography from '@material-ui/core/Typography'

const Answer = ({text, choice}) => {
  return (
    <ListItem alignItems="flex-start">
      <Typography
        color="textSecondary"
        display={choice ? 'block' : 'none'}
      >
        {choice}
      </Typography>
      <Typography
        color="textSecondary"
        display={text ? 'block' : 'none'}
      >
        {text}
      </Typography>
    </ListItem>
  )
}

Answer.propTypes = {
  choice: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  text: PropTypes.string,
}

export default Answer
