import React from 'react'
import PropTypes from 'prop-types'
import {Route} from 'react-router-dom'
import SurveyResponseContainer from './components/SurveyResponseContainer'

const route = ({match}) => (
  <Route
    component={SurveyResponseContainer}
    exact
    path={`${match.path}/:surveyResponseId`}
  />
)

route.displayName="SurveyResponseRoute"
route.propTypes ={
  match: PropTypes.object,// eslint-disable-line react/forbid-prop-types
}

export default route
