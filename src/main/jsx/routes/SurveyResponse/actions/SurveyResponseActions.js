import AppDispatcher from 'dispatcher/AppDispatcher'
import {ActionTypes} from 'constants/AppConstants'
import {StubApi} from 'api'

class SurveyResponseActions {

  async findById(id) {
    const response = await StubApi.getResponseById(id)
    AppDispatcher.dispatch({
      actionType: ActionTypes.SURVEY_RESPONSE_LOAD,
      response,
    })
  }

}

export default new SurveyResponseActions()
