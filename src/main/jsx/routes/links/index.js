import {template} from 'lodash'
import i18n from 'util/i18n'

export default class RouteLinks {
  linkTo(link, data = {}) {
    return template(this.links[link])({
      links: window.ydw.config._links,
      locale: i18n.locale(),
      ...data,
    })
  }
}