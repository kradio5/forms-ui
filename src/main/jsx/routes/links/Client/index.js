import RouteLinks from 'routes/links'

class ClientLinks extends RouteLinks {
  links = {
    'CLIENTS': '/clients',
    'CLIENT_SURVEY_RESPONSES': '/clients/${id}',
    'CLIENT_SURVEY_FORMS': '/clients/${client}/forms',
    'CLIENT_SURVEY_FORMS_PROCESS': '/clients/${client}/forms/${form}',
    'SURVEY_RESPONSE': '/response/${id}',
  }
}

export default new ClientLinks()
