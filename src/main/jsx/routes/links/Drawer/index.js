import {template} from 'lodash'
import ClientStore from 'stores/ClientStore'
import i18n from 'util/i18n'

class DrawerLinks {
  linkTo(link, data = {}) {
    return template(link)({
      user: ClientStore.getUrlFragment(),
      links: window.ydw.config._links,
      locale: i18n.locale(),
      ...data,
    })
  }
}

export default new DrawerLinks()