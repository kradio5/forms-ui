import AppDispatcher from 'dispatcher/AppDispatcher'
import {ReduceStore} from 'flux/utils'
import {ActionTypes} from 'constants/AppConstants'

class ClientsStore extends ReduceStore {
  getInitialState() {
    return {
      clients: [],
    }
  }

  getClients() {
    return this.getState().clients
  }

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.CLIENTS_LOAD: {
        const {clients=[]} = action 
        return {...state, clients}
      }
      case ActionTypes.CLIENTS_CLEAR:
        return this.getInitialState()
      default:
        return state
    }
  }
}

export default new ClientsStore(AppDispatcher)
