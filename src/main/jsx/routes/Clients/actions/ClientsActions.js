import AppDispatcher from 'dispatcher/AppDispatcher'
import {ActionTypes} from 'constants/AppConstants'
import ClientsStore from 'routes/Clients/stores/ClientsStore'
import {StubApi} from 'api'

class ClientsActions {

  async list() {
    const clients = await StubApi.listClientProfiles()
    AppDispatcher.dispatch({
      actionType: ActionTypes.CLIENTS_LOAD,
      clients,
    })
    return clients
  }

  fetchNext(params = {}) {
    return ClientsStore.getResource()
      .link('next').fetch(params)
      .then(data => {
        AppDispatcher.dispatch({
          actionType: ActionTypes.CLIENTS_LOAD_NEXT,
          data: {
            resource: data,
            clients: data.props.clientProfiles || [],
          },
        })

        return data
      })
  }

  clear() {
    AppDispatcher.dispatch({
      actionType: ActionTypes.CLIENTS_CLEAR,
    })
  }
}

export default new ClientsActions()
