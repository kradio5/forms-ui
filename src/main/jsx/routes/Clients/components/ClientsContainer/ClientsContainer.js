import React from 'react'
import {Container} from 'flux/utils'
import ClientsStore from 'routes/Clients/stores/ClientsStore'
import ClientsActions from 'routes/Clients/actions/ClientsActions'
import Clients from '../Clients'

class ClientsContainer extends React.Component {

  static getStores() {
    return [ClientsStore]
  }

  static calculateState() {
    return {
      clients: ClientsStore.getClients(),
    }
  }

  static displayName = 'ClientsContainer'

  componentDidMount() {
    ClientsActions.list()
  }

  render() {
    return (
      <Clients
        {...this.props}
        {...this.state}
      />
    )
  }
}

export default Container.create(ClientsContainer)
