import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import {fade} from '@material-ui/core/styles/colorManipulator'
import Avatar from '@material-ui/core/Avatar'
import Icon from '@material-ui/core/Icon'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader' 
import ListItem from '@material-ui/core/ListItem'

const styles = theme => ({
  avatar: {
    backgroundColor: fade(theme.palette.primary.main, .8),
    color: theme.palette.secondary.main,
    fill: theme.palette.secondary.main,
  },
  card: {
    width: '100%',
  },
  listItem: {
    padding: 0,
    marginBottom: theme.spacing.unit,
    '&:last-child': {
      marginBottom: 0,
    },
  },
})

const ClientCard = ({classes, children, ...rest}) => {
  const avatar = (
    <Avatar className={classes.avatar}>
      <Icon>{'person'}</Icon>
    </Avatar>
  )
  return (
    <ListItem
      button
      className={classes.listItem}
      disableRipple
      {...rest}
    >
      <Card className={classes.card}>
        <CardHeader
          avatar={avatar}
          className={classes.cardHeader}
          title={children}
        />
      </Card>
    </ListItem>
  )
}

ClientCard.propTypes = {
  children: PropTypes.string,
  classes: PropTypes.object,// eslint-disable-line react/forbid-prop-types
}

export default withStyles(styles, {withTheme: true})(ClientCard)
