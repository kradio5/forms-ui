import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import NavigationActions from 'actions/NavigationActions'
import ClientLinks from 'routes/links/Client'
import AppToolbar, {ToolbarTitle, ToolbarRow, ToolbarSection} from 'components/AppToolbar'
import BackButton from 'components/BackButton'
import i18n from 'util/i18n'
import ClientCard from './ClientCard'
import ClientList from './ClientList'

const styles = theme => ({
  root: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    display: 'flex',
    flexDirection: theme.direction === 'rtl' ? 'row-reverse' : 'row',
    zIndex: 1,
    overflow: 'hidden',
  },
  content: {
    position: 'relative',
    width: '100%',
    height: 'calc(100% - 56px)',
    padding: theme.spacing.unit,
    marginTop: 56,
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 64px)',
      padding: theme.spacing.unit * 2,
      marginTop: 64,
    },
    overflowX: 'hidden',
    overflowY: 'scroll',
  },
  list: {
    padding: 0,
  },
})


class Clients extends React.PureComponent {
  static displayName = 'Clients'

  static propTypes = {
    classes: PropTypes.object,// eslint-disable-line react/forbid-prop-types
    clients: PropTypes.arrayOf(PropTypes.object),
  }

  handleSelectClient = id => {
    NavigationActions.push(
      ClientLinks.linkTo('CLIENT_SURVEY_RESPONSES', {id})
    )
  }

  render() {
    const {classes, clients=[]} = this.props
    const toolbar = (
      <AppToolbar>
        <ToolbarRow>
          <ToolbarSection variant="start">
            <BackButton/>
            <ToolbarTitle>
              {i18n.t('ui.titles.clients', {_: 'Clients'})}
            </ToolbarTitle>
          </ToolbarSection>
          <ToolbarSection variant="end" />
        </ToolbarRow>
      </AppToolbar>
    )

    const clientCards = clients.map(({first_name, last_name, id}) => (
      <ClientCard
        key={id}
        onClick={() => {
          this.handleSelectClient(id)
        }}
      >
        {`${first_name} ${last_name}`}
      </ClientCard>
    ))
    return (
      <div className={classes.root}>
        {toolbar}
        <main className={classes.content}>
          <ClientList
            noItems={i18n.t('ui.titles.noClients')}
          >
            {clientCards}
          </ClientList>
        </main>
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(Clients)
