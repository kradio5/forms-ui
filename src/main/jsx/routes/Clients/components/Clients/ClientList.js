import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import NoItems from 'components/NoItems'
import List from '@material-ui/core/List'

const styles = () => ({
  list: {
    padding: 0,
  },
})

const ClientList = ({classes, children, noItems}) => {
  return children.length > 0 ? (
    <List className={classes.list}>
      {children}
    </List>
  ) : (
    <NoItems
      icon="person"
      title={noItems}
    />
  )
}

ClientList.propTypes = {
  children: PropTypes.arrayOf(PropTypes.node),
  classes: PropTypes.object,// eslint-disable-line react/forbid-prop-types
  noItems: PropTypes.string,
}

export default withStyles(styles, {withTheme: true})(ClientList)
