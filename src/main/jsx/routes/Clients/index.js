import React from 'react'
import PropTypes from 'prop-types'
import {Route, Switch} from 'react-router-dom'
import ImportLoader from 'components/ImportLoader'

const Clients = ImportLoader(() => import('./components/ClientsContainer'))
const SurveyResponsesRoute = ImportLoader(() => import('./routes/SurveyResponses'))

const route = ({match}) => (
  <Switch>
    <Route
      component={Clients}
      exact
      path={match.path}
    />
    <Route
      component={SurveyResponsesRoute}
      path={`${match.path}/:client`}
    />
  </Switch>
)

route.displayName="ClientsRoute"
route.propTypes ={
  match: PropTypes.object,// eslint-disable-line react/forbid-prop-types
}

export default route
