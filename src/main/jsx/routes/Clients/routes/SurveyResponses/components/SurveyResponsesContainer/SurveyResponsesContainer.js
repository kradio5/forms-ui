import React from 'react'
import PropTypes from 'prop-types'
import {Container} from 'flux/utils'
import SurveyResponsesActions from 'routes/Clients/routes/SurveyResponses/actions/SurveyResponsesActions'
import SurveyResponsesStore from 'routes/Clients/routes/SurveyResponses/stores/SurveyResponsesStore'
import SurveyResponses from '../SurveyResponses'

class SurveyResponsesContainer extends React.Component {

  static getStores() {
    return [SurveyResponsesStore]
  }

  static calculateState(prevState) {
    return {
      ...prevState,
      responses: SurveyResponsesStore.getResponses(),
    }
  }

  static displayName = 'SurveyResponsesContainer'

  static propTypes = {
    match: PropTypes.shape({
      params: PropTypes.shape({
        client: PropTypes.string,
      }),
    }),
  }


  async componentDidMount(){
    const {client} = this.props.match.params
    this.setState({isLoading: true})
    await SurveyResponsesActions.findByUid(client)
    this.setState({isLoading: false})

  }

  render() {
    return (
      <SurveyResponses
        {...this.props}
        {...this.state}
      />
    )
  }
}

export default Container.create(SurveyResponsesContainer)
