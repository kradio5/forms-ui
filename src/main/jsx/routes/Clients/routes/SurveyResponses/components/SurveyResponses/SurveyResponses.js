import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import NavigationActions from 'actions/NavigationActions'
import ClientLinks from 'routes/links/Client'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Fab from '@material-ui/core/Fab'
import Icon from '@material-ui/core/Icon'
import AppToolbar, {ToolbarTitle, ToolbarRow, ToolbarSection} from 'components/AppToolbar'
import BackButton from 'components/BackButton'
import Preloader from 'components/Preloader'
import NoItems from 'components/NoItems'
import i18n from 'util/i18n'

const styles = theme => ({
  root: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    display: 'flex',
    flexDirection: theme.direction === 'rtl' ? 'row-reverse' : 'row',
    zIndex: 1,
    overflow: 'hidden',
  },
  content: {
    position: 'relative',
    width: '100%',
    height: 'calc(100% - 56px)',
    padding: theme.spacing.unit,
    marginTop: 56,
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 64px)',
      padding: theme.spacing.unit * 2,
      marginTop: 64,
    },
    overflowX: 'hidden',
    overflowY: 'scroll',
  },
  card: {
    width: '100%',
  },
  add: {
    position: 'absolute',
    bottom: '16px',
    right: '16px',
  },
})

class SurveyResponses extends React.Component {
  static displayName = 'SurveyResponses'

  static propTypes = {
    classes: PropTypes.object,// eslint-disable-line react/forbid-prop-types
    isLoading: PropTypes.bool,
    match: PropTypes.shape({
      params: PropTypes.shape({
        client: PropTypes.oneOfType([
          PropTypes.string,
          PropTypes.number,
        ]),
      }),
    }),
    responses: PropTypes.arrayOf(
      PropTypes.object,
    ),
  }

  handleSelectResponse = id => {
    NavigationActions.push(
      ClientLinks.linkTo('SURVEY_RESPONSE', {id})
    )
  }

  handleCreateResponse = () => {
    const {client} = this.props.match.params
    NavigationActions.push(
      ClientLinks.linkTo('CLIENT_SURVEY_FORMS', {client})
    )
  }

  render() {
    const {classes, responses=[], isLoading} = this.props
    const toolbar = (
      <AppToolbar>
        <ToolbarRow>
          <ToolbarSection variant="start">
            <BackButton />
            <ToolbarTitle>
              {i18n.t('ui.pages.SurveyResponses.title')}
            </ToolbarTitle>
          </ToolbarSection>
          <ToolbarSection variant="end" />
        </ToolbarRow>
      </AppToolbar>
    )

    const responseItems = responses.map((response, index) => (
      <ListItem
        button
        key={index}
        onClick={() => {
          this.handleSelectResponse(response.id)
        }}
      >
        <ListItemIcon>
          <Icon 
            color="action"
          >
            {'assignment'}
          </Icon>
        </ListItemIcon>
        <ListItemText 
          primary={response.title}
          secondary={response.phase.title}
        />
      </ListItem>
    ))

    const responseList = (<List>{responseItems}</List>)
    const noItems = (<NoItems/>)
    const isItemsLoaded = !!responses.length || isLoading
    return (
      <div className={classes.root}>
        {toolbar}
        <main className={classes.content}>
          { isItemsLoaded ? responseList : noItems }
          <Preloader
            isLoading={isLoading}
          />
          <Fab 
            aria-label="Add" 
            className={classes.add}
            color="primary" 
            onClick={() => {
              this.handleCreateResponse()
            }}
          >
            <Icon>
              {'add'}
            </Icon>
          </Fab>
        </main>
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(SurveyResponses)
