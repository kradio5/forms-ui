import AppDispatcher from 'dispatcher/AppDispatcher'
import {ReduceStore} from 'flux/utils'
import {ActionTypes} from 'constants/AppConstants'

class SurveyResponsesStore extends ReduceStore {
  getInitialState() {
    return {responses: []}
  }

  getResponses() {
    const {responses} = this.getState()
    return responses
  }

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.SURVEY_RESPONSES_LOAD:
        return {
          ...state, 
          responses: action.responses, 
          resource: action.resource,
        }
      case ActionTypes.SURVEY_RESPONSES_CLEAR:
        return {...state, responses: []}
      default:
        return state
    }
  }
}

export default new SurveyResponsesStore(AppDispatcher)
