import AppDispatcher from 'dispatcher/AppDispatcher'
import {ActionTypes} from 'constants/AppConstants'
import {StubApi} from 'api'

class SurveyResponsesActions {

  async findByUid(uid) {
    const resource = await StubApi.listResponsesByUserId(uid)
    AppDispatcher.dispatch({
      actionType: ActionTypes.SURVEY_RESPONSES_LOAD,
      responses: resource._embedded.responses || [],
      resource,
    })
  }

}

export default new SurveyResponsesActions()
