import React from 'react'
import PropTypes from 'prop-types'
import {Route, Switch} from 'react-router-dom'
import ImportLoader from 'components/ImportLoader'

const SurveyForms = ImportLoader(() => import('./components/SurveyFormsContainer'))
const SurveyProcessRoute = ImportLoader(() => import('./routes/SurveyProcess'))

const route = ({match}) => (
  <Switch>
    <Route
      component={SurveyForms}
      exact
      path={match.path}
    />
    <Route
      component={SurveyProcessRoute}
      path={`${match.path}/:form`}
    />
  </Switch>
)

route.displayName="SurveyFormRoute"
route.propTypes ={
  match: PropTypes.object,// eslint-disable-line react/forbid-prop-types
}

export default route
