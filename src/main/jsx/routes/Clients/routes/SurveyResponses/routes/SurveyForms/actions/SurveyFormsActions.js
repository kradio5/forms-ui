import AppDispatcher from 'dispatcher/AppDispatcher'
import {ActionTypes} from 'constants/AppConstants'
import {StubApi} from 'api'

class SurveyFormsActions {

  async findAll() {
    const resource = await StubApi.listForms()
    AppDispatcher.dispatch({
      actionType: ActionTypes.SURVEY_FORMS_LOAD,
      forms: resource._embedded.forms || [],
      resource,
    })
  }

}

export default new SurveyFormsActions()
