import AppDispatcher from 'dispatcher/AppDispatcher'
import {ReduceStore} from 'flux/utils'
import {ActionTypes} from 'constants/AppConstants'

class SurveyFormsStore extends ReduceStore {
  getInitialState() {
    return {forms: [], resource: {}}
  }

  getResource() {
    return this.getState().resource
  }
  getSurveyForms() {
    return this.getState().forms
  }

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.SURVEY_FORMS_LOAD:
        return {forms: action.forms, resource: action.resource}
      case ActionTypes.SURVEY_FORMS_LOAD_NEXT: {
        const newForms = [...state.forms, ...action.forms]
        return {forms: newForms, resource: action.resource}
      }
      default:
        return state
    }
  }
}

export default new SurveyFormsStore(AppDispatcher)
