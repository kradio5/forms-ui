import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import classnames from 'classnames'
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'
import NavigationActions from 'actions/NavigationActions'
import AppToolbar, {ToolbarTitle, ToolbarRow, ToolbarSection} from 'components/AppToolbar'
import BackButton from 'components/BackButton'
import Preloader from 'components/Preloader'
import i18n from 'util/i18n'
import ClientLinks from 'routes/links/Client'

const styles = theme => ({
  root: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    display: 'flex',
    flexDirection: theme.direction === 'rtl' ? 'row-reverse' : 'row',
    zIndex: 1,
    overflow: 'hidden',
  },
  content: {
    position: 'relative',
    width: '100%',
    height: 'calc(100% - 56px)',
    padding: theme.spacing.unit,
    marginTop: 56,
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 64px)',
      padding: theme.spacing.unit * 2,
      marginTop: 64,
    },
    overflowX: 'hidden',
    overflowY: 'scroll',
  },
  page : {
    padding: 16,
  },

  cardTitle: {
    margin: '16px 0 16px 0',
  },

  hidden: {
    display: 'none',
  },
})


class SurveyProcess extends React.PureComponent {
  static displayName = 'SurveyProcess'

  static propTypes = {
    classes: PropTypes.object, // eslint-disable-line react/forbid-prop-types
    isLoading: PropTypes.bool, // eslint-disable-line react/forbid-prop-types
    respondingSurveyForm: PropTypes.object, // eslint-disable-line react/forbid-prop-types
  }
  
  handleFinish = () => {
    const {match} = this.props
    const {params} = match
    NavigationActions.replace(
      ClientLinks.linkTo('CLIENT_SURVEY_RESPONSES', {id: params.client})
    )
  }

  render() {
    const {
      classes,
      isLoading,
      respondingSurveyForm,
    } = this.props

    const {title} = respondingSurveyForm

    const toolbar = (
      <AppToolbar>
        <ToolbarRow>
          <ToolbarSection variant="start">
            <BackButton/>
            <ToolbarTitle>
              {i18n.t('ui.pages.SurveyProcess.title', {title})}
            </ToolbarTitle>
          </ToolbarSection>
          <ToolbarSection variant="end" />
        </ToolbarRow>
      </AppToolbar>
    )

    return (
      <div className={classes.root}>
        {toolbar}
        <main className={classes.content}>
          <Card
            className={classnames({[classes.hidden]:isLoading})}
          >
            <CardContent>
              <Typography
                className={classes.cardTitle}
                component="h3"
                variant="h6"
              >
                {title}
              </Typography>
              <Typography>
                {i18n.t('ui.pages.SurveyProcess.description')}
              </Typography>
            </CardContent>
            <CardActions>
              <Button
                color="primary"
                onClick={() => {this.handleFinish()}}
              >
                Finish
              </Button>
            </CardActions>
          </Card>
          <Preloader
            isLoading={isLoading}
          />
        </main>
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(SurveyProcess)
