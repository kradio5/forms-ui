import AppDispatcher from 'dispatcher/AppDispatcher'
import {ActionTypes} from 'constants/AppConstants'
import {StubApi} from 'api'

class SurveyProcessActions {

  async loadRespondingDetails() {
    const forms = await StubApi.listForms()
    const resource = await StubApi.fetchLinkToResponse()
    window.open(resource.editUrl, '_blank')
    AppDispatcher.dispatch({
      actionType: ActionTypes.SURVEY_PROCESS_LOAD,
      resource,
      surveyForm: {
        editUrl: resource.editUrl,
        phase: resource.formPhase,
        title: resource.formTitle, 
      },
    })
  }

}

export default new SurveyProcessActions()
