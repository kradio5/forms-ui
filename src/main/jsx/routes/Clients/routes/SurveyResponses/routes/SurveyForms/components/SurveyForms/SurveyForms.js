import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import Icon from '@material-ui/core/Icon'
import NavigationActions from 'actions/NavigationActions'
import AppToolbar, {ToolbarTitle, ToolbarRow, ToolbarSection} from 'components/AppToolbar'
import BackButton from 'components/BackButton'
import Preloader from 'components/Preloader'
import i18n from 'util/i18n'
import ClientLinks from 'routes/links/Client'

const styles = theme => ({
  root: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    display: 'flex',
    flexDirection: theme.direction === 'rtl' ? 'row-reverse' : 'row',
    zIndex: 1,
    overflow: 'hidden',
  },
  content: {
    position: 'relative',
    width: '100%',
    height: 'calc(100% - 56px)',
    padding: theme.spacing.unit,
    marginTop: 56,
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 64px)',
      padding: theme.spacing.unit * 2,
      marginTop: 64,
    },
    overflowX: 'hidden',
    overflowY: 'scroll',
  },
  page : {
    padding: 16,
  },
})


class SurveyForms extends React.PureComponent {
  static displayName = 'SurveyForms'

  static propTypes = {
    classes: PropTypes.object,// eslint-disable-line react/forbid-prop-types
    forms: PropTypes.arrayOf(PropTypes.object),
    isLoading: PropTypes.bool,// eslint-disable-line react/forbid-prop-types
  }

  handleSelectForm = (form) => {
    const {match} = this.props
    const {params} = match
    NavigationActions.push(
      ClientLinks.linkTo('CLIENT_SURVEY_FORMS_PROCESS', {
        client: params.client,
        form,
      })
    )
  }

  _renderForm(form, index){
    return (
      <ListItem
        button
        key={index}
        onClick={() => {this.handleSelectForm(index)}}
      >
        <ListItemIcon>
          <Icon 
            color="action"
          >
            {'assignment'}
          </Icon>
        </ListItemIcon>
        <ListItemText
          primary={form.title}
          secondary={form.phase.title}
        />
      </ListItem>
    )

  }

  render() {
    const {
      classes,
      isLoading,
      forms,
    } = this.props

    const toolbar = (
      <AppToolbar>
        <ToolbarRow>
          <ToolbarSection variant="start">
            <BackButton/>
            <ToolbarTitle>
              {i18n.t('ui.pages.SurveyForms.title')}
            </ToolbarTitle>
          </ToolbarSection>
          <ToolbarSection variant="end" />
        </ToolbarRow>
      </AppToolbar>
    )

    const formItems = forms.map(this._renderForm, this)
    return (
      <div className={classes.root}>
        {toolbar}
        <main className={classes.content}>
          <List>
            {formItems}
          </List>
          <Preloader
            isLoading={isLoading}
          />
        </main>
      </div>
    )
  }
}

export default withStyles(styles, {withTheme: true})(SurveyForms)
