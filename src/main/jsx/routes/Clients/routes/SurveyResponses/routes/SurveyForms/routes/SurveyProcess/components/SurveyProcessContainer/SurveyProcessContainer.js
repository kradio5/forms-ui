import React from 'react'
import PropTypes from 'prop-types'
import {Container} from 'flux/utils'
import SurveyProcessStore from '../../stores/SurveyProcessStore'
import SurveyFormsActions from '../../actions/SurveyProcessActions'
import SurveyProcess from '../SurveyProcess'

class SurveyProcessContainer extends React.Component {

  static getStores() {
    return [SurveyProcessStore]
  }

  static calculateState() {
    return {
      respondingSurveyForm: SurveyProcessStore.getRespondingForm(),
    }
  }

  static displayName = 'SurveyProcessContainer'

  static propTypes = {
    match: PropTypes.object,
  }

  async componentWillMount() {
    this.setState({isLoading: true})
    await SurveyFormsActions.loadRespondingDetails()
    this.setState({isLoading: false})
  }

  render() {
    return (
      <SurveyProcess
        {...this.props}
        {...this.state}
      />
    )
  }
}

export default Container.create(SurveyProcessContainer)
