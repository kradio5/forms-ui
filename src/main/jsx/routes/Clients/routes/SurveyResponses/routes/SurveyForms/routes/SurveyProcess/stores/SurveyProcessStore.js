import AppDispatcher from 'dispatcher/AppDispatcher'
import {ReduceStore} from 'flux/utils'
import {ActionTypes} from 'constants/AppConstants'

class SurveyProcess extends ReduceStore {
  getInitialState() {
    return {resource: {}, surveyForm: {}}
  }

  getResource() {
    return this.getState().resource
  }

  getRespondingForm() {
    return this.getState().surveyForm
  }

  reduce(state, action) {
    switch (action.actionType) {
      case ActionTypes.SURVEY_PROCESS_LOAD:
        return {surveyForm: action.surveyForm, resource: action.resource}
      default:
        return state
    }
  }
}

export default new SurveyProcess(AppDispatcher)
