import React from 'react'
import PropTypes from 'prop-types'
import {Container} from 'flux/utils'
import SurveyFormsStore from '../../stores/SurveyFormsStore'
import SurveyFormsActions from '../../actions/SurveyFormsActions'
import SurveyForms from '../SurveyForms'

class SurveyFormsContainer extends React.Component {

  static getStores() {
    return [SurveyFormsStore]
  }

  static calculateState() {
    return {
      forms: SurveyFormsStore.getSurveyForms(),
    }
  }

  static displayName = 'SurveyFormsContainer'

  static propTypes = {
    match: PropTypes.object,
  }

  async componentWillMount() {
    this.setState({isLoading: true})
    await SurveyFormsActions.findAll()
    this.setState({isLoading: false})
  }

  render() {
    return (
      <SurveyForms
        {...this.props}
        {...this.state}
      />
    )
  }
}

export default Container.create(SurveyFormsContainer)
