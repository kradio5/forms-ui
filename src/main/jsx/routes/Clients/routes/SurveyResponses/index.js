import React from 'react'
import PropTypes from 'prop-types'
import {Route, Switch} from 'react-router-dom'
import ImportLoader from 'components/ImportLoader'

const SurveyResponses = ImportLoader(() => import('./components/SurveyResponsesContainer'))
const SurveyFormsRoute = ImportLoader(() => import('./routes/SurveyForms'))

const route = ({match}) => (
  <Switch>
    <Route
      component={SurveyResponses}
      exact
      path={match.path}
    />
    <Route
      component={SurveyFormsRoute}
      path={`${match.path}/forms`}
    />
  </Switch>
)

route.displayName="SurveyResponsesRoute"
route.propTypes ={
  match: PropTypes.object,// eslint-disable-line react/forbid-prop-types
}

export default route
