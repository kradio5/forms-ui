import RouteSecurity from 'components/RouteSecurity'

export default new RouteSecurity([
  ['/login', () => false],
  ['/clients*', () => true],
])
