import React from 'react'
import {Router} from 'react-router'
import {Redirect, Route, Switch} from 'react-router-dom'
import history from 'util/history'
import ImportLoader from 'components/ImportLoader'
import App from 'components/App'

const LoginRoute = ImportLoader(() => import('./Login'))
const ClientsRoute = ImportLoader(() => import('./Clients'))
const SurveyResponseRoute = ImportLoader(() => import('./SurveyResponse'))

const route = () => (
  <Router history={history}>
    <Switch>
      <Route
        component={LoginRoute}
        path="/login"
      />
      <App>
        <Switch>
          <Route
            component={ClientsRoute}
            path="/clients"
          />
          <Route
            component={SurveyResponseRoute}
            path="/response"
          />
          <Redirect to="/clients" />
        </Switch>
      </App>
    </Switch>
  </Router>
)

route.displayName = "RootRoute"

export default route
