/**
 * Example application config.
 */

window.ydw = {
  config: {
    "sourceUri": "http://localhost:3000",
    "oauth2": {
      "domain": "localhost:8081",
      "clientId": "example",
      "_secure": false,
    },
    navigation: {
      sections: {
        user: [{
          name: "profile",
          icon: "person",
          href: "${links.home.href}/profile",
        }, {
          name: "logout",
          icon: "exit_to_app",
          href: "${links.home.href}/logout",
        }],
        main: [{
          name: "summary",
          icon: "dashboard",
          href: "${links.coachUi.href}/users/${user}/summary?locale=${locale}",
        }, {
          name: "plan",
          icon: "event",
          href: "${links.coachUi.href}/users/${user}/events?locale=${locale}",
        }, {
          name: "notes",
          icon: "markunread",
          href: "${links.coachUi.href}/users/${user}/notes?locale=${locale}",
        }, {
          name: "trackers",
          icon: "accessibility",
          href: "${links.coachUi.href}/users/${user}/trackers?locale=${locale}",
        }],
        my: [{
          name: "exercises",
          icon: "directions_run",
          href: "${links.exerciseUi.href}/exercises?locale=${locale}",
        }, {
          name: "foods",
          icon: "restaurant",
          href: "${links.foodUi.href}/foods?locale=${locale}",
        }, {
          name: "experts",
          icon: "group",
          href: "${links.expertUi.href}/clients?locale=${locale}",
        }],
        contacts: [{
          name: "settings",
          icon: "settings",
          href: "${links.coachUi.href}/users/${user}/settings?locale=${locale}",
        }, {
          name: "contacts",
          icon: "help",
          href: "https://youdowell.com",
        }],
      },
    },
    _links: {
      "home": {
        href: "http://localhost:8081"
      },
      "logout": {
        href: "http://localhost:8081/logout"
      },
      "authApi": {
        href: "http://localhost:8081/api/auth/v1"
      },
      "coachApi": {
        href: "http://localhost:8082/api/coach/v1"
      },
      "nutritionApi": {
        href: "http://localhost:8083/api/nutrition/v1"
      },
      "surveyApi": {
        href: "http://localhost:8086/api/survey/v1"
      },
      "coachUi": {
        href: "http://localhost:3000"
      },
      "exerciseUi": {
        href: "http://localhost:3001"
      },
      "foodUi": {
        href: "http://localhost:3002"
      },
      "expertUi": {
        href: "http://localhost:3003"
      },
      "surveyUi": {
        href: "http://localhost:3004"
      }
    },
    theme: {
      palette: {
        primary: {
          main: '#37474f',
        },
        secondary: {
          main: '#cddc39',
        },
      },
    }
  }
}
