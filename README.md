# Youdowell Forms UI

Web back-end for Youdowell Forms project.

## Requirements

* npm
* gulp

## Getting started

Run using `npm start`.

This will build and start a webpack hot reload server in development mode on port 3000.

### Building

* `npm install` to install dependencies
* `npm run build` to build a project package

### Config
* `window.ydw.config.api.root` - url to API
