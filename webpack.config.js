const webpack = require('webpack')
const path = require('path')

const PROJECT_ROOT = path.resolve('src', 'main', 'jsx')
const PROJECT_SCSS_ROOT = path.resolve('src', 'main', 'scss')
const OUTPUT = path.resolve('build', 'bundle')

const NODE_ENV = process.env.NODE_ENV || 'development'
const {getIfUtils, removeEmpty} = require('webpack-config-utils')
const {ifProduction, ifNotProduction, ifDevelopment} = getIfUtils(NODE_ENV)
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

module.exports = {
  mode: NODE_ENV,
  entry: {
    index: [
      'babel-polyfill',
      'index.js',
    ],
  },
  output: {
    path: OUTPUT,
    publicPath: '',
    filename: ifProduction('[name].nocache.js', 'index.nocache.js'),
    chunkFilename: ifProduction('[id].build.[chunkhash].js', '[id].build.js'),
  },
  context: PROJECT_ROOT,
  resolve: {
    modules: [
      PROJECT_ROOT,
      'node_modules',
    ],
  },
  module: {
    rules: [{
      test: /\.js$/,
      include: [PROJECT_ROOT],
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader',
        options: {
          cacheDirectory: ifDevelopment(),
          plugins: ['react-hot-loader/babel'],
        },
      }],
    }, {
      test: /\.(sa|sc|c)ss$/,
      use: [
        ifDevelopment('style-loader', MiniCssExtractPlugin.loader),
        {
          loader: 'css-loader',
          options: {
            sourceMap: true,
            importLoaders: 2,
          },
        },
        {
          loader: 'postcss-loader',
          options: {
            sourceMap: true,
            plugins: () => [require('autoprefixer')({grid: false})],
          },
        },
        {
          loader: 'sass-loader',
          options: {
            outputStyle: 'expanded',
            sourceMap: true,
            includePaths: [
              path.resolve('node_modules'),
              PROJECT_SCSS_ROOT,
            ],
          },
        },
      ],
    }, {
      test: /\.(png|woff|woff2|eot|ttf|svg)$/,
      use: ['url-loader?limit=100000'],
    }],
  },
  plugins: removeEmpty([
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: ifDevelopment('[name].css', '[name].[hash].css'),
      chunkFilename: ifDevelopment('[id].css', '[id].[hash].css'),
    }),
    ifProduction(new webpack.SourceMapDevToolPlugin({
      filename: '[file].map',
    })),
    ifDevelopment(new webpack.HotModuleReplacementPlugin()),
  ]),
}
