const webpack = require('webpack')
const webpackDevServer = require('webpack-dev-server')

const config = require('./webpack.config.js')
const options = {
  contentBase: 'build/bundle',
  historyApiFallback: true,
  host: '172.16.83.1',
  hot: true,
  inline: true,
  progress: true,
  port: 3008,
  publicPath: '/',
  stats: { colors: true },
  headers: { 'Access-Control-Allow-Origin': '*' },
}

webpackDevServer.addDevServerEntrypoints(config, options)
const compiler = webpack(config)
const server = new webpackDevServer(compiler, options)

server.listen(options.port, '172.16.83.1', () => {
  console.log(`dev server listening on port ${options.port}`);
})
